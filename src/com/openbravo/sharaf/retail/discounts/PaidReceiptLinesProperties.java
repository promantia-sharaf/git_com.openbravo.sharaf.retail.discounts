/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.discounts;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

/**
 * Defines hql properties for the PaidReceipsLines Order header
 * 
 * @author ebe
 * 
 */

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class PaidReceiptLinesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
      private static final long serialVersionUID = 1L;
      {
        add(new HQLProperty("ordLine.custdisOffer.id", "custdisOffer"));
        add(new HQLProperty("ordLine.custdisOrderline.id", "custdisOrderline"));
        add(new HQLProperty("ordLine.custdisRedemptionAmount", "custdisRedemptionAmount"));
        add(new HQLProperty("ordLine.custdisIrType", "custdisIrType"));
      }
    };

    return list;
  }
}

/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.discounts.service;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class OfferBrandConfigModel extends ProcessHQLQuery {

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    List<String> hqlQueries = new ArrayList<String>();
    String hqlQry = "select ob.id as id, ob.client.id as clientId, ob.organization.id as orgId, ob.active as active,"
        + " ob.brand.id as brandId, ob.promotionDiscount.id as promotionDiscountId from custdis_m_offer_brand ob where ob.active = 'Y'";
    hqlQueries.add(hqlQry);
    return hqlQueries;
  }

}

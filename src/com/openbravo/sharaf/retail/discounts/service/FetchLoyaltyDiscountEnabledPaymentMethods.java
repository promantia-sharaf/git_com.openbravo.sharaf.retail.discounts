package com.openbravo.sharaf.retail.discounts.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.retail.posterminal.TerminalType;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;

public class FetchLoyaltyDiscountEnabledPaymentMethods extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(FetchLoyaltyDiscountEnabledPaymentMethods.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    try {
      OBContext.setAdminMode(true);

      HashMap<String, String> map = new HashMap<String, String>();
      OBCriteria<FIN_PaymentMethod> obCriteriaPM = OBDal.getInstance()
          .createCriteria(FIN_PaymentMethod.class);
      obCriteriaPM.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_ORGANIZATION,
          OBDal.getInstance().get(Organization.class, "0")));
      List<FIN_PaymentMethod> paymentMethodList = obCriteriaPM.list();
      if (!paymentMethodList.isEmpty()) {
        for (int i = 0; i < paymentMethodList.size(); i++) {

          if (paymentMethodList.get(i).isCustdisEnableloyalty() != null
              && paymentMethodList.get(i).isCustdisEnableloyalty()) {

            FIN_PaymentMethod finPaymentMethod = OBDal.getInstance().get(FIN_PaymentMethod.class,
                paymentMethodList.get(i).getId());

            TerminalType terminalType = OBDal.getInstance().get(TerminalType.class,
                jsonsent.get("terminalType"));

            OBCriteria<TerminalTypePaymentMethod> obCriteria = OBDal.getInstance()
                .createCriteria(TerminalTypePaymentMethod.class);

            obCriteria.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_PAYMENTMETHOD,
                finPaymentMethod));

            obCriteria.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_OBPOSTERMINALTYPE,
                terminalType));

            List<TerminalTypePaymentMethod> posTerminalTypePaymentMethodList = obCriteria.list();
            if (posTerminalTypePaymentMethodList.size() > 0) {
              for (int k = 0; k < posTerminalTypePaymentMethodList.size(); k++) {
                map.put(posTerminalTypePaymentMethodList.get(k).getSearchKey(),
                    posTerminalTypePaymentMethodList.get(k).getId());
              }
            }
          }
        }
      }
      data.put("enabledLoyaltyDiscountPayMethod", map);
      result.put("status", 0);
      result.put("data", data);
    } catch (Exception e) {
      log.error("Error getting Loyalty Enabled Payment Methods: " + e.getMessage());
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

}
/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.discounts.service;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class BigWinSlotTimeModel extends ProcessHQLQuery {

	@Override
	protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
		List<String> hqlQueries = new ArrayList<String>();
		String hqlQry = "select st.id as id, st.startTime as StartTime, st.client.id as client, st.eNDTime as EndTime,"
				+ " st.offer.id as m_offer_id, st.active as active from custdis_slottime st where st.active = 'Y'"
				+ "and st.client='" + OBContext.getOBContext().getCurrentClient().getId() + "'";
		hqlQueries.add(hqlQry);
		return hqlQueries;
	}

}
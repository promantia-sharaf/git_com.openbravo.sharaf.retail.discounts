package com.openbravo.sharaf.retail.discounts.service;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;

import com.openbravo.sharaf.retail.discounts.OfferLimit;

public class PromotionLimitServicePaymentCheck extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(PromotionLimitServicePaymentCheck.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    // String offerid = null;
    String clientid = null;
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    try {
      OBContext.setAdminMode(true);

      JSONArray jsondataArray = new JSONArray(jsonsent.getString("jsondata"));
      JSONArray mainArray = new JSONArray();
      JSONObject mainObj = new JSONObject();
      for (int i = 0; i < jsondataArray.length(); i++) {
        JSONObject eachObj = new JSONObject();
        JSONObject jsonobj = (JSONObject) jsondataArray.get(i);
        String offerid = jsonobj.getString("id");
        int qtyFromPos = Integer.parseInt(jsonobj.getString("qty"));

        PriceAdjustment moffer = OBDal.getInstance().get(PriceAdjustment.class, offerid);

        if (!moffer.isActive()) {
          log.error("mOffer is not active for moffer id:" + offerid);
          throw new Exception("mOffer is not active for moffer id:" + offerid);
        }

        OBCriteria<OfferLimit> offerLimitList = OBDal.getInstance()
            .createCriteria(OfferLimit.class);
        offerLimitList.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, moffer));

        if (offerLimitList.list().size() > 0) {
          OfferLimit offerLimit = offerLimitList.list().get(0);
          int qtyRemaining = offerLimit.getPromotionDiscount().getCustdisSellableLimit().intValue()
              - offerLimit.getConsumedCount().intValue();
          if (qtyFromPos > qtyRemaining) {
            eachObj.put("offerid", offerLimit.getPromotionDiscount().getId());
            eachObj.put("name", offerLimit.getPromotionDiscount().getName());
            eachObj.put("qty_avail", qtyRemaining);
            mainArray.put(eachObj);
          }
        }

      }

      if (mainArray.length() > 0) {
        mainObj.put("OfferList", mainArray);
      }

      int count = 0; // = getRemainingLimit(offerid, clientid);
      data.put("remainingqty", count);
      result.put("status", 0);
      if (mainArray.length() > 0) {
        result.put("data", mainObj);
      } else {
        result.put("data", "success");
      }

    } catch (Exception e) {
      log.error("Error in fetching promotion count:", e);
      result.put("status", 1);
      result.put("message", e);
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

}
package com.openbravo.sharaf.retail.discounts.service;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;

import com.openbravo.sharaf.retail.discounts.LoyaltyDiscountUsage;

public class LoyaltyDiscountFairLimitService extends JSONProcessSimple {

  private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
      .getLogger(LoyaltyDiscountFairLimitService.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    String offerid = null;
    String clientid = null;
    String cardNumber = null;
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    if (jsonsent.has("mofferid") && jsonsent.has("cardNumber")) {
      offerid = jsonsent.getString("mofferid");
      clientid = jsonsent.getString("client");
      cardNumber = jsonsent.getString("cardNumber");
    }

    try {
      OBContext.setAdminMode(true);
      int count = getRemainingLimit(offerid, cardNumber, clientid);
      data.put("remainingqty", count);
      result.put("status", 0);
      result.put("data", data);

    } catch (Exception e) {
      log.error("Error in fetching promotion count:", e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

  private int getRemainingLimit(String offerid, String cardNumber, String clientid)
      throws Exception {
    int count = 0;
    PriceAdjustment moffer = OBDal.getInstance().get(PriceAdjustment.class, offerid);

    if (moffer != null && (!moffer.isActive())) {
      log.error("Offer is not active for offer:" + moffer.getName());
      throw new Exception("Offer is not active for offer:" + moffer.getName());
    }

    OBQuery<LoyaltyDiscountUsage> offerLimitList = OBDal.getInstance().createQuery(
        LoyaltyDiscountUsage.class,
        " loyaltyCardNumber = '" + cardNumber + "' and offer.id = '" + offerid + "'");

    if (offerLimitList.list().size() == 0 && moffer != null) {
      log.error("Record not found in promotion limit table for Offer:" + moffer.getName());
      return moffer.getCustdisFairUsageLimit().intValue();
    }

    LoyaltyDiscountUsage offerLimit = offerLimitList.list().get(0);
    if (offerLimit.getConsumedCount() == null) {
      return offerLimit.getOffer().getCustdisFairUsageLimit().intValue();
    }

    if (offerLimit != null && offerLimit.getConsumedCount() != null) {
      count = offerLimit.getOffer().getCustdisFairUsageLimit().intValue()
          - offerLimit.getConsumedCount().intValue();
    }
    return count;
  }
}
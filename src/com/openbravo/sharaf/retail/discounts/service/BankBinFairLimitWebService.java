package com.openbravo.sharaf.retail.discounts.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.openbravo.service.web.WebService;

import com.openbravo.sharaf.retail.discounts.utils.WebServiceUtils;

public class BankBinFairLimitWebService implements WebService {

  private static final Logger log = Logger.getLogger(BankBinFairLimitWebService.class);
  private static String offerid = "mofferid";
  private static String client = "client";
  private static String cardnumber = "creditCardNumber";

  @Override
  public void doGet(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    String mOfferId = null;
    String clientstr = null;
    String cardNumber = null;
    log.info("BankBinFairLimitWebService request"+request.toString());
    try {
      mOfferId = request.getParameter(offerid);
      clientstr = request.getParameter(client);
      cardNumber = request.getParameter(cardnumber);
      if (mOfferId == null) {
        throw new Exception("Blank value set for parameter mOfferId in the request");
      }
      if (clientstr == null) {
        throw new Exception("Blank value set for parameter client in the request");
      }
      if (cardNumber == null) {
        throw new Exception("Blank value set for parameter card number in the request");
      }
    } catch (Exception e) {
      log.error("Fair Usage limit: Error in Print Web Service", e);
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

    try {

      int count = WebServiceUtils.getFairUsageLimitStatus(mOfferId, clientstr, cardNumber);
      WebServiceUtils.writeResults(response, count);

    } catch (Exception e) {
      log.error(e);
      response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

  }

  @Override
  public void doPost(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {

    response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);

  }

  @Override
  public void doDelete(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);

  }

  @Override
  public void doPut(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);

  }

}

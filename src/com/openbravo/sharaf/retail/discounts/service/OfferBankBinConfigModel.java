/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.discounts.service;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class OfferBankBinConfigModel extends ProcessHQLQuery {

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    List<String> hqlQueries = new ArrayList<String>();
    String hqlQry = "select obb.id as id, obb.client.id as clientId, obb.organization.id as orgId, obb.active as active,"
        + "obb.bankBIN.id as bankBinId, obb.promotionDiscount.id as promotionDiscountId from custdis_m_offer_bank_bins obb where "
        + "active = 'Y'";
    hqlQueries.add(hqlQry);
    return hqlQueries;
  }
}

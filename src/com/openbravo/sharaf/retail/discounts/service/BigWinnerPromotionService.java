package com.openbravo.sharaf.retail.discounts.service;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.model.pricing.priceadjustment.Product;

import com.openbravo.sharaf.retail.discounts.CustdisBigWinnerTransUsage;

import com.openbravo.sharaf.retail.discounts.OfferLimit;
import com.promantia.sharaf.epay.CpseEpayConfig;
import com.promantia.sharaf.tft.CpstTftTrans;
import com.openbravo.sharaf.integration.retail.qwikcilver.master.CustqcPendingOp;
import com.openbravo.sharaf.retail.discounts.CustdisBigWinSequenceTrack;
import com.openbravo.sharaf.retail.discounts.CustdisSlotTime;
import java.util.Random;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.TimeZone;

public class BigWinnerPromotionService extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(BigWinnerPromotionService.class);
  private final Lock seqObjLock = new ReentrantLock();
  private final Lock seqCheckLock = new ReentrantLock();

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    if (!seqCheckLock.tryLock()) {
      JSONObject result = new JSONObject();
      return result;
    }
    try {
    log.info("BigWinnerPromotionService" + jsonsent.toString());
    String offerid = null;
    String documentno = null;
    String slotid = null;
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    Boolean winnerSelected = false;
    String focProduct = null;
    Boolean isVoid = false;
    Boolean seqRecordUpdated = false;
    String phoneNumber = null;
    Boolean validateFairUsageLimit = false;
    Boolean offerActive = false;
    Boolean slotValid = false;

    
    if (jsonsent.has("mofferid")) {
      offerid = jsonsent.getString("mofferid");
    }
    if (jsonsent.has("documentNo")) {
      documentno = jsonsent.getString("documentNo");
    }
    if (jsonsent.has("slottimeid")) {
      slotid = jsonsent.getString("slottimeid");
    }
    if(jsonsent.has("isVoid")) {
      isVoid = jsonsent.getBoolean("isVoid");
    }
    if(jsonsent.has("phoneNumber")) {
      phoneNumber = jsonsent.getString("phoneNumber");
      log.info("Phone number"+phoneNumber);  
      }
    // Checking Winner already declared for that slot
    // Inserting eligible bill into sequence track table
    // Compare sequence number with winning sequence Number
    try {
      OBContext.setAdminMode(true);
      if(isVoid == false) {
        PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
        CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotid);
        if(offer != null && offer.isActive()) {
          offerActive = true;
          log.info("Offer active");
        }
        if (slotTime != null && slotTime.isActive()) {
            slotValid = true;
            log.info("Slot Active"); 
        }
       validateFairUsageLimit = validatePhoneNumber(offerid, phoneNumber);
       if(!validateFairUsageLimit) {
        if(offerActive && slotValid) {
          winnerSelected = bigWinnerSelection(offerid, slotid, documentno);
        } else {
          data.put("promotionNotAvailable",offerActive);
          data.put("slotNotAvailable",slotValid);
        }
       } else {
         data.put("promotionAlreadyUtilized",validateFairUsageLimit);
       }
      } else {
        seqRecordUpdated = updateVoidSeqTrack(offerid, slotid, documentno, isVoid);
      }
      log.info("Winner result"+winnerSelected); 
      if(winnerSelected) {
        focProduct = selectFOCProduct(offerid);
      }
      data.put("winnerselected", winnerSelected);
      data.put("focProduct", focProduct);
      data.put("seqRecordUpdated", seqRecordUpdated);
      result.put("status", 0);
      result.put("data", data);

    } catch (Exception e) {
      log.error("Error in selecting winner:", e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  } finally {
    seqCheckLock.unlock();
    }
  }
  
  
  private synchronized boolean bigWinnerSelection(String offerid, String slotid, String documentno) throws Exception {
    Boolean winnerSelected = false;
    Long sequenceNumber = 0L;  
    Boolean winnerDeclaredStatus = winnerDeclaredStatus(offerid, slotid);
    CustdisBigWinSequenceTrack seqTrackObj = null;
    Long winSequence  = null;
    if(winnerDeclaredStatus) {
      winnerSelected = false;
    } else {
      //Check whether document number already added for that offerid and slotid combination
      Boolean docAlreadyAdded = docAlreadyAdded(offerid, slotid, documentno);
      PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
      CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotid);
      if (slotTime.getWINSequence() != null) {
         winSequence = slotTime.getWINSequence();
         log.info("Winning sequencce number"+winSequence.toString());
      }
      if(!docAlreadyAdded) {
        //Insert new document in sequence track window
        //Check whether winner sequence number already matched with the sequence
        sequenceNumber = getSequenceNumber(offerid, slotid);

        if(seqTrackObj == null && (sequenceNumber < winSequence)) {
        seqTrackObj = saveSequenceTrack(slotTime, documentno, offer, false, false);
        if(seqTrackObj != null) {
          sequenceNumber = sequenceNumber + 1L;
        }
        log.info("Document sequence number"+sequenceNumber.toString());
        if (winSequence != null) {
          if (sequenceNumber == winSequence) {
            winnerSelected = true;
            log.info("Winning selected");
          } else {
            winnerSelected = false;
        }
        }
        }
      } else {
        winnerSelected = false;
      }
    }
    return winnerSelected;
  }
  
  private synchronized boolean winnerDeclaredStatus(String offerid, String slotid) throws Exception {
    Boolean winnerAlreadyDeclared = false;
    //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY,0);
    cal.set(Calendar.MINUTE,0);
    cal.set(Calendar.SECOND,0);
    cal.set(Calendar.MILLISECOND,0);
    Date currentDate = cal.getTime();
   // String currentDate = formatter.format(new Date());
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
      CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotid);
      OBCriteria<CustdisBigWinSequenceTrack> seqTrackCriteria = OBDal.getInstance()
          .createCriteria(CustdisBigWinSequenceTrack.class);
      seqTrackCriteria.setFilterOnReadableOrganization(false);
      seqTrackCriteria
          .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_CUSTDISSLOTTIME, slotTime));
      seqTrackCriteria.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
      seqTrackCriteria.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DECLARED, true));
      seqTrackCriteria.add(Restrictions.ge(CustdisBigWinSequenceTrack.PROPERTY_CREATIONDATE, currentDate));
      log.info("Created Time one: " + currentDate);
      if(seqTrackCriteria.list().size() > 0) {
    	  winnerAlreadyDeclared = true;
        }
      return winnerAlreadyDeclared;
  }
  
  private synchronized boolean validatePhoneNumber(String offerid, String phoneNumber) throws Exception {
    Boolean promotionAlreadyUtilized = false;
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
    Long fairUsageLimit = offer.getCustdisFairUsageLimit();
    OBCriteria<CustdisBigWinnerTransUsage> prodUsageCriteria = OBDal.getInstance()
    .createCriteria(CustdisBigWinnerTransUsage.class);
    prodUsageCriteria.setFilterOnReadableOrganization(false);
    prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_OFFER, offer));
    prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_BPARTNERPHONE, phoneNumber));
      if(prodUsageCriteria.list().size() >= fairUsageLimit.intValue() && fairUsageLimit != 0L) {
        log.info("Promotion already utilized for the provided mobile number");
        promotionAlreadyUtilized = true;
      }
      return promotionAlreadyUtilized;
  }
  private synchronized boolean docAlreadyAdded(String offerid, String slotid, String documentno) throws Exception {
    Boolean docAlreadyAdded = false;
    int count = 0;
    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
    String currentDate = formatter.format(new Date());
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
    CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotid);
    OBCriteria<CustdisBigWinSequenceTrack> sequenceTrackCriteria = OBDal.getInstance()
        .createCriteria(CustdisBigWinSequenceTrack.class);
    sequenceTrackCriteria.setFilterOnReadableOrganization(false);
    sequenceTrackCriteria
    .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
    sequenceTrackCriteria
        .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_CUSTDISSLOTTIME, slotTime));
    sequenceTrackCriteria
    .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DOCUMENTNO, documentno));
    if(sequenceTrackCriteria.list().size() > 0) {
      docAlreadyAdded = true;
    }
      return docAlreadyAdded;
  }
  
  private synchronized Long getSequenceNumber(String offerid, String slotid) throws Exception {
   // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");    
    //String currentDate = formatter.format(new Date());
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY,0);
    cal.set(Calendar.MINUTE,0);
    cal.set(Calendar.SECOND,0);
    cal.set(Calendar.MILLISECOND,0);
    Date currentDate = cal.getTime();
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
    CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotid);
    OBCriteria<CustdisBigWinSequenceTrack> sequenceTrackCriteria = OBDal.getInstance()
        .createCriteria(CustdisBigWinSequenceTrack.class);
    sequenceTrackCriteria.setFilterOnReadableOrganization(false);
    sequenceTrackCriteria
    .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
    sequenceTrackCriteria
        .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_CUSTDISSLOTTIME, slotTime));
    sequenceTrackCriteria.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_ISVOID,false));
    sequenceTrackCriteria.add(Restrictions.ge(CustdisBigWinSequenceTrack.PROPERTY_CREATIONDATE, currentDate));
    log.info("Created Time: " + currentDate + "------ " +Long.valueOf(sequenceTrackCriteria.list().size()));
    
    return Long.valueOf(sequenceTrackCriteria.list().size());

  }
  
  private synchronized CustdisBigWinSequenceTrack saveSequenceTrack(CustdisSlotTime slotTime, String documentno, 
      PriceAdjustment offer, Boolean isVoid, Boolean declared) {
    CustdisBigWinSequenceTrack seqTrackObj = null;
    if (!seqObjLock.tryLock()) {
    	log.info("inside save seq lock");
      return seqTrackObj;
    }
    try {
      log.info("Record creation for document"+documentno);      
      seqTrackObj = OBProvider.getInstance().get(CustdisBigWinSequenceTrack.class);
      seqTrackObj.setCustdisSlottime(slotTime);
      seqTrackObj.setDocumentno(documentno);
      seqTrackObj.setOffer(offer);
      seqTrackObj.setVoid(isVoid);
      seqTrackObj.setDeclared(declared);
      OBDal.getInstance().save(seqTrackObj);
      OBDal.getInstance().flush();
      return seqTrackObj;
    }finally {
      seqObjLock.unlock();
    }

  }
    
  private String selectFOCProduct(String offerid) throws Exception {
    String selectedFOCProductID = null; 
    int focProductCount = 0;
    Long sellableLimit = 0L;
    String productID = null;
    Long consumedCount = 0L;
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
    OBCriteria<Product> prodCriteria = OBDal.getInstance()
    .createCriteria(Product.class);
    prodCriteria.setFilterOnReadableOrganization(false);
    prodCriteria.add(Restrictions.eq(Product.PROPERTY_PRICEADJUSTMENT, offer));
    prodCriteria.add(Restrictions.eq(Product.PROPERTY_ACTIVE, true));
    for(int i =0; i < prodCriteria.list().size(); i++) {
      log.info("Prod criteria after removing Org filter");
    }
    focProductCount = prodCriteria.list().size();
      if (focProductCount == 0) {
        selectedFOCProductID = null;
      } else {
        if(focProductCount == 1) {
          if(prodCriteria.list().get(0) != null && prodCriteria.list().get(0).getCustdisSellableLimit() != null) {
            sellableLimit = prodCriteria.list().get(0).getCustdisSellableLimit();
            if(prodCriteria.list().get(0).getProduct().getId() != null) {
              productID = prodCriteria.list().get(0).getProduct().getId();
            }
            org.openbravo.model.common.plm.Product prod = OBDal.getInstance().get(org.openbravo.model.common.plm.Product.class, productID);
            OBCriteria<CustdisBigWinnerTransUsage> prodUsageCriteria = OBDal.getInstance()
            .createCriteria(CustdisBigWinnerTransUsage.class);
            prodUsageCriteria.setFilterOnReadableOrganization(false);
            prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_OFFER, offer));
            prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_PRODUCT, prod));
            if(prodUsageCriteria.list().size() > 0) {
              for(int i =0; i < prodUsageCriteria.list().size(); i++) {
                consumedCount += prodUsageCriteria.list().get(i).getCount(); 
                log.info("Consumed count"+consumedCount);
              }
              } 
            //focProductCount = prodCriteria.list().size();
            if(sellableLimit > 0 && (sellableLimit == consumedCount)) {
              log.info("sellableLimit > 0 && (sellableLimit == consumedCount)");
              selectedFOCProductID = null;
            } else if(sellableLimit == 0 || sellableLimit == null ||sellableLimit > consumedCount) {
              log.info("sellableLimit == 0 || sellableLimit > consumedCount");
              selectedFOCProductID = productID;
            }
          } else {
            productID = prodCriteria.list().get(0).getProduct().getId();
            selectedFOCProductID = productID;
          }
        } else {
          log.info("FOC Product More than 1");
          int focCheckCount = 5;
          //Select a random number between 0 to (Total FOC Product Count - 1)
          for(int i = 0; i < focCheckCount; i++) {  // checking available FOC products for 5 times
            int randomNum = randInt(0,focProductCount - 1);
            if(prodCriteria.list().get(randomNum) != null && prodCriteria.list().get(randomNum).getCustdisSellableLimit() != null) {
             sellableLimit = prodCriteria.list().get(randomNum).getCustdisSellableLimit();
            }
            if(sellableLimit > 0 ) {
              if(prodCriteria.list().get(randomNum).getProduct().getId() != null) {
                productID = prodCriteria.list().get(randomNum).getProduct().getId();
              }
              org.openbravo.model.common.plm.Product prod = OBDal.getInstance().get( org.openbravo.model.common.plm.Product.class, productID);
              OBCriteria<CustdisBigWinnerTransUsage> prodUsageCriteria = OBDal.getInstance()
              .createCriteria(CustdisBigWinnerTransUsage.class);
              prodUsageCriteria.setFilterOnReadableOrganization(false);
              prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_OFFER, offer));
              prodUsageCriteria.add(Restrictions.eq(CustdisBigWinnerTransUsage.PROPERTY_PRODUCT, prod));
              if(prodUsageCriteria.list().size() > 0) {
                for(int j =0; j < prodUsageCriteria.list().size(); j++) {
                  consumedCount += prodUsageCriteria.list().get(j).getCount(); 
                  log.info("Consumed count"+consumedCount);
                }
                } 
              if(sellableLimit > 0 && (sellableLimit == consumedCount)) {
                selectedFOCProductID = null;
              } else if(sellableLimit == 0  || sellableLimit == null || sellableLimit > consumedCount){
                selectedFOCProductID = productID;
                log.info("selectedFOCProductID"+selectedFOCProductID);
                break;
              }
            } else if (sellableLimit == 0 || sellableLimit == null){
              if(prodCriteria.list().get(randomNum).getProduct().getId() != null) {
                selectedFOCProductID = prodCriteria.list().get(randomNum).getProduct().getId();
                break;
              }
            }
          }
        }
  } 
      return selectedFOCProductID;
  }
  
  //Update isVoid as true in sequence track table
  private Boolean updateVoidSeqTrack(String offerid, String slotId, String documentNo, Boolean isVoid) throws Exception {
    Boolean seqRecordUpdated = false;
    PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, offerid);
    CustdisSlotTime slotTime = OBDal.getInstance().get(CustdisSlotTime.class, slotId);
    OBCriteria<CustdisBigWinSequenceTrack> sequenceTrackCriteria = OBDal.getInstance()
        .createCriteria(CustdisBigWinSequenceTrack.class);
    sequenceTrackCriteria.setFilterOnReadableOrganization(false);
    sequenceTrackCriteria
        .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
    sequenceTrackCriteria
    .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DOCUMENTNO, documentNo));
    sequenceTrackCriteria
    .add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_CUSTDISSLOTTIME, slotTime));
    for(int i=0; i < sequenceTrackCriteria.list().size(); i++) {
        sequenceTrackCriteria.list().get(i).setVoid(isVoid);
        OBDal.getInstance().save(sequenceTrackCriteria.list().get(0));
        OBDal.getInstance().flush();
        seqRecordUpdated = true;
    }
    return seqRecordUpdated;
  }
  private int randInt(int min, int max) {
    Random random = new Random();
    int randomNum = random.nextInt((max - min) + 1) + min;
    return randomNum;
}

}
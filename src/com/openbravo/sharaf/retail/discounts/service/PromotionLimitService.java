package com.openbravo.sharaf.retail.discounts.service;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;

import com.openbravo.sharaf.retail.discounts.OfferLimit;

public class PromotionLimitService extends JSONProcessSimple {

  private static Logger log = Logger.getLogger(PromotionLimitService.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    String offerid = null;
    String clientid = null;
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();

    if (jsonsent.has("mofferid")) {
      offerid = jsonsent.getString("mofferid");
      clientid = jsonsent.getString("client");
    }

    try {
      OBContext.setAdminMode(true);
      int count = getRemainingLimit(offerid, clientid);
      data.put("remainingqty", count);
      result.put("status", 0);
      result.put("data", data);

    } catch (Exception e) {
      log.error("Error in fetching promotion count:", e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }

    return result;
  }

  private int getRemainingLimit(String offerid, String clientid) throws Exception {
    int count = 0;
    PriceAdjustment moffer = OBDal.getInstance().get(PriceAdjustment.class, offerid);

    if (moffer != null && (!moffer.isActive())) {
      log.error("Offer is not active for offer:" + moffer.getName());
      throw new Exception("Offer is not active for offer:" + moffer.getName());
    }

    OBCriteria<OfferLimit> offerLimitList = OBDal.getInstance().createCriteria(OfferLimit.class);
    offerLimitList.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, moffer));

    if (offerLimitList.list().size() == 0) {
      log.error("Record not found in promotion limit table for Offer:" + moffer.getName());
      throw new Exception(
          "Record not found in promotion limit table for Offer:" + moffer.getName());
    }

    OfferLimit offerLimit = offerLimitList.list().get(0);

    if (offerLimit.getConsumedCount() == null) {
      return offerLimit.getPromotionDiscount().getCustdisSellableLimit().intValue();
    }

    if (offerLimit != null && offerLimit.getConsumedCount() != null) {
      count = offerLimit.getPromotionDiscount().getCustdisSellableLimit().intValue()
          - offerLimit.getConsumedCount().intValue();
    }

    return count;
  }
}
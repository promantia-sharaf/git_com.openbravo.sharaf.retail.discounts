package com.openbravo.sharaf.retail.discounts;

import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.retail.posterminal.OrderLoaderHook;

import com.promantia.sharaf.tft.CpstTftTrans;

public class OrderLoaderHookForBigWin implements OrderLoaderHook {

	private static final Logger log = Logger.getLogger(OrderLoaderHookForBigWin.class);

	@Override
	public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice) throws Exception {
		try {
			log.info("inside order loader of jsonorder" + jsonorder.toString());
			String bigWinId = null;
			String productId = null;
			long qty = 0L;
			String originalDocNo = null;
			String slotId = null;
			String documentNo = order.getDocumentNo();
			boolean isVerifiedReturn = jsonorder.has("isVerifiedReturn") ? jsonorder.getBoolean("isVerifiedReturn")
					: false;
			JSONArray lines = jsonorder.getJSONArray("lines");
			for (int i = 0; i < lines.length(); i++) {
				JSONObject line = lines.getJSONObject(i);
				qty = Integer.parseInt(line.getString("qty"));
				if (isVerifiedReturn && originalDocNo == null) {
					originalDocNo = line.getString("originalDocumentNo");
					log.info("original doc no" + originalDocNo);
				}
				if (bigWinId == null && line.has("custDisBigWinnerPromotion")
						&& line.getString("custDisBigWinnerPromotion") != null) {
					bigWinId = line.getString("custDisBigWinnerPromotion");
					log.info("bigwinid" + bigWinId);

					PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, bigWinId);
					for (OrderLine orderLine : order.getOrderLineList()) {
						if (orderLine.getId().equals(line.get("id"))) {
							orderLine.setCustdisBigWin(offer);

						}
					}
				}
				if (line.has("custDisBigWinnerPromotion") && bigWinId != null) {
					productId = line.getJSONObject("product").getString("id");
					log.info("product id" + productId);
				}
				if (slotId == null && line.has("custDisBigWinnerPromotionSlotID")
						&& line.getString("custDisBigWinnerPromotionSlotID") != null) {
					slotId = line.getString("custDisBigWinnerPromotionSlotID");
					log.info("slot time id" + slotId);

				}

			}

			// if in case of successful winner declared after order completed updating
			// declared as true in sequence tracking table
			if (bigWinId != null && documentNo != null && slotId != null) {

				OBCriteria<CustdisBigWinSequenceTrack> bigWinSequence = OBDal.getInstance()
						.createCriteria(CustdisBigWinSequenceTrack.class);
				PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, bigWinId);
				CustdisSlotTime slottime = OBDal.getInstance().get(CustdisSlotTime.class, slotId);
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DOCUMENTNO, documentNo));
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_CUSTDISSLOTTIME, slottime));

				List<CustdisBigWinSequenceTrack> sequence = bigWinSequence.list();
				if (sequence.size() > 0) {
					CustdisBigWinSequenceTrack seqTrack = bigWinSequence.list().get(0);
					seqTrack.setDeclared(true);
					OBDal.getInstance().save(seqTrack);
					OBDal.getInstance().flush();

				}
			}

			// For sales and return: big win txn usage table record insertion
			if (bigWinId != null && productId != null) {
				PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, bigWinId);
				Product product = OBDal.getInstance().get(Product.class, productId);
				CustdisBigWinnerTransUsage usage = OBProvider.getInstance().get(CustdisBigWinnerTransUsage.class);
				if (order != null) {
					if (jsonorder.has("custshaCustomerPhone")
							&& !jsonorder.getString("custshaCustomerPhone").isEmpty()) {
						usage.setBpartnerPhone(jsonorder.getString("custshaCustomerPhone"));
					} else if (jsonorder.getJSONObject("bp").has("phone")
							&& !jsonorder.getJSONObject("bp").getString("phone").isEmpty()) {
						usage.setBpartnerPhone(jsonorder.getJSONObject("bp").getString("phone"));
					}
				}
				usage.setOrder(order);
				usage.setOffer(offer);
				usage.setProduct(product);
				usage.setCount(qty);
				OBDal.getInstance().save(usage);
				OBDal.getInstance().flush();

			}
			// if it is verified return updating isVoid as true in sequence tracking table
			if (bigWinId != null && isVerifiedReturn && originalDocNo != null) {

				OBCriteria<CustdisBigWinSequenceTrack> bigWinSequence = OBDal.getInstance()
						.createCriteria(CustdisBigWinSequenceTrack.class);
				PriceAdjustment offer = OBDal.getInstance().get(PriceAdjustment.class, bigWinId);
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_OFFER, offer));
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DOCUMENTNO, originalDocNo));
				bigWinSequence.add(Restrictions.eq(CustdisBigWinSequenceTrack.PROPERTY_DECLARED, true));
				List<CustdisBigWinSequenceTrack> sequence = bigWinSequence.list();
				if (sequence.size() > 0) {
					CustdisBigWinSequenceTrack seqTrack = bigWinSequence.list().get(0);
					seqTrack.setVoid(true);
					OBDal.getInstance().save(seqTrack);
					OBDal.getInstance().flush();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}

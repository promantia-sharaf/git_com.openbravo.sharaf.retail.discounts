package com.openbravo.sharaf.retail.discounts.term;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class CUSTDISPaidReceiptLinesProperties extends ModelExtension {
  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
      private static final long serialVersionUID = 1L;
      {
        add(new HQLProperty("ordLine.custdisBankBin.id", "custdisBankBinOffer"));
        add(new HQLProperty("ordLine.custdisBigWin.id", "custDisBigWinnerPromotion"));
        add(new HQLProperty("ordLine.custdisLoyaldisprmo.id", "custdisLoyaltyDiscountOffer"));
        add(new HQLProperty("ordLine.custdisLinedisamt", "custdisLinedisamt"));
        add(new HQLProperty("ordLine.custdisLineloyldisamt", "custdisLineloyldisamt"));
      }
    };
    return list;

  }
}

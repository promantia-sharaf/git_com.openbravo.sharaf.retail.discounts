/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.discounts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

/**
 * @author ebe
 * 
 */
@ApplicationScoped
@ComponentProvider.Qualifier(CUSTDISComponentProvider.QUALIFIER)
public class CUSTDISComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CUSTDIS_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.retail.discounts";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {

    final GlobalResourcesHelper grhelper = new GlobalResourcesHelper();

    grhelper.add("promotions.js");
    grhelper.add("utils.js");
    grhelper.add("components/modalPromotionSelector.js");
    grhelper.add("hooks/OBPOS_PostAddProductToOrder.js");
    grhelper.add("hooks/OBPOS_PreAddPaymentButton.js");
    grhelper.add("hooks/OBPOS_preCheckDiscount.js");
    grhelper.add("hooks/OBPOS_preApplyDiscountsHook.js");
    grhelper.add("hooks/OBPOS_PreDeleteLine.js");
    grhelper.add("hooks/OBPOS_PreRemovePaymentHook.js");
    grhelper.add("hooks/OBPOS_LoadPOSWindow.js");
    grhelper.add("hooks/prePaymentHook.js");
    grhelper.add("hooks/preChangeBusinessPartner.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApply.js");
    grhelper.add("hooks/OBRETUR_postAddVerifiedReturnLinesHook.js");
    grhelper.add("hooks/OBPOS_PreDiscountChangeHook.js");
    grhelper.add("hooks/prePaymentHookForDiscounts.js");
    grhelper.add("extend/extendEditOrderLine.js");
    grhelper.add("extend/extendDiscountListOptions.js");
    grhelper.add("hooks/OBPOS_AddProductToOrderHook.js");
    grhelper.add("hooks/OBPOS_LineSelectedHook.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApplyForPromotions.js");
    grhelper.add("models/BankBinConfigModel.js");
    grhelper.add("models/BankBinNumberConfigModel.js");
    grhelper.add("models/OfferBankBinConfigModel.js");
    grhelper.add("models/OfferBrandConfigModel.js");
    grhelper.add("models/OfferPaymentMethodConfigModel.js");
    grhelper.add("components/modalCardBinNumber.js");
    grhelper.add("hooks/OBPOS_PaymentSelected.js");
    grhelper.add("components/modalBankBinPromotionSelector.js");
    grhelper.add("components/extendModalPayment.js");
    grhelper.add("components/modalLoyalityCardNumber.js");
    grhelper.add("bankbinpromotion.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApplyForBankBinPromotion.js");
    grhelper.add("hooks/OBPOS_PreAddProductPreDeleteLineHook.js");
    grhelper.add("hooks/OBPOS_PrePaymentHookForBankBin.js");
    grhelper.add("promotionValidation.js");
    grhelper.add("models/BigWinSlotTimeModel.js");
    grhelper.add("hooks/OBPOS_PrePaymentHookForBigWinnerPromo.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApplyForBigWinPromotion.js");
    grhelper.add("hooks/OBPOS_PreDeleteLineForBigWinPromo.js");
    grhelper.add("hooks/OBPOS_PreDeleteCurrentOrderForBigWinPromo.js");
    grhelper.add("components/modalBigWinnerDeclaration.js");
    grhelper.add("hooks/OBPOS_PostAddProductBuyGetYwithDiscountPercentage.js");
    grhelper.add("hooks/preAddPaymentHookforLoyaltyDiscount.js");
    grhelper.add("hooks/OBPOS_TerminalLoadedFromBackendLoyaltyDiscount.js");
    grhelper.add("hooks/preDeleteLineForLoyaltyDiscount.js");
    grhelper.add("hooks/OBRETUR_ReturnReceiptApplyForLoyaltyDiscount.js");
    grhelper.add("hooks/OBPOS_PrePaymentHookForLoayltyDiscount.js");
    grhelper.add("hooks/OBPOS_PreAddProductToOrderLoyaltyValidation.js");
    grhelper.add("hooks/preAddPaymentHookforLoyaltyPartialReturn.js");
    
    return grhelper.getGlobalResources();
  }

  private class GlobalResourcesHelper {
    private final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    private final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";

    public void add(String file) {
      globalResources.add(
          createComponentResource(ComponentResourceType.Static, prefix + file, POSUtils.APP_NAME));
    }

    public List<ComponentResource> getGlobalResources() {
      return globalResources;
    }
  }
}

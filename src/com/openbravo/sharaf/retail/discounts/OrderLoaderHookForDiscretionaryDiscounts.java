package com.openbravo.sharaf.retail.discounts;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONPropertyToEntity;
import org.openbravo.mobile.core.utils.OBMOBCUtils;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.common.invoice.InvoiceLineOffer;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.common.order.OrderLineOffer;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class OrderLoaderHookForDiscretionaryDiscounts implements OrderLoaderHook {

  private static final Logger log = Logger
      .getLogger(OrderLoaderHookForDiscretionaryDiscounts.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {

    try {

      JSONArray lines = jsonorder.getJSONArray("lines");
      Entity promotionLineEntity = ModelProvider.getInstance().getEntity(OrderLineOffer.class);
      Entity invLinePromotionLineEntity = ModelProvider.getInstance().getEntity(
          InvoiceLineOffer.class);
      int pricePrecision = order.getCurrency().getObposPosprecision() == null ? order.getCurrency()
          .getPricePrecision().intValue() : order.getCurrency().getObposPosprecision().intValue();
      if (jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BS")) {
        log.info("Invoked orderloaderhook for discretionary discounts for Bulk Sales");
        for (int i = 0; i < lines.length(); i++) {
          try {
            String promoId = null;
            String promoName = null;
            String discretionaryDisc = null;
            JSONObject line = lines.getJSONObject(i);
            OrderLine orderline = OBDal.getInstance().get(OrderLine.class, line.getString("id"));
            InvoiceLine invoiceLine = OBDal.getInstance().get(InvoiceLine.class,
                invoice.getInvoiceLineList().get(i).getId());

            if (line.has("promotions")) {
              JSONArray promotions = line.getJSONArray("promotions");
              log.debug("Promotions length: " + promotions.length());

              for (int k = 0; k < promotions.length(); k++) {
                JSONObject clone = new JSONObject(promotions.getJSONObject(k).toString());
                promoId = clone.getString("discountType");
                promoName = clone.get("name").toString();
                log.debug("Promotion ID: " + promoId);
                if (promoId != null
                    && (new BigDecimal(clone.getString("amt")).compareTo(BigDecimal.ONE) > 0)) {
                  if (promoId.equalsIgnoreCase("5B59AAF5B6BF477C912A7DFD8A7EE7C7")
                      || promoId.equalsIgnoreCase("7B49D8CC4E084A75B7CB4D85A6A3A578")
                      || promoId.equalsIgnoreCase("8338556C0FBF45249512DB343FEFD280")
                      || promoId.equalsIgnoreCase("D1D193305A6443B09B299259493B272A")
                      || promoId.equalsIgnoreCase("20E4EC27397344309A2185097392D964")) {

                    log.info("OrderlineOffer record size is: "
                        + orderline.getOrderLineOfferList().size());
                    if (orderline.getOrderLineOfferList().size() > 0) {
                      for (int j = 0; j < orderline.getOrderLineOfferList().size(); j++) {
                        if (orderline.getOrderLineOfferList().get(j).getPriceAdjustment().getName()
                            .equalsIgnoreCase(promoName)) {
                          orderline.getOrderLineOfferList().remove(j);
                        }
                      }
                    }

                    log.info("InvoiceLineOffer record size is: "
                        + invoiceLine.getInvoiceLineOfferList().size());
                    if (invoiceLine.getInvoiceLineOfferList().size() > 0) {
                      for (int j = 0; j < invoiceLine.getInvoiceLineOfferList().size(); j++) {
                        if (invoiceLine.getInvoiceLineOfferList().get(j).getPriceAdjustment()
                            .getName().equalsIgnoreCase(promoName)) {
                          invoiceLine.getInvoiceLineOfferList().remove(j);
                        }
                      }
                    }

                  }
                }
              }

              for (int k = 0; k < promotions.length(); k++) {
                JSONObject clone = new JSONObject(promotions.getJSONObject(k).toString());
                promoId = clone.getString("discountType");
                log.debug("Promotion ID: " + promoId);
                if (promoId != null
                    && (new BigDecimal(clone.getString("amt")).compareTo(BigDecimal.ONE) > 0)) {
                  if (promoId.equalsIgnoreCase("5B59AAF5B6BF477C912A7DFD8A7EE7C7")
                      || promoId.equalsIgnoreCase("7B49D8CC4E084A75B7CB4D85A6A3A578")
                      || promoId.equalsIgnoreCase("8338556C0FBF45249512DB343FEFD280")
                      || promoId.equalsIgnoreCase("D1D193305A6443B09B299259493B272A")
                      || promoId.equalsIgnoreCase("20E4EC27397344309A2185097392D964")) {

                    discretionaryDisc = ((new BigDecimal(clone.getString("userAmt"))).divide(
                        new BigDecimal(line.getString("qty")), 2, RoundingMode.HALF_UP)).toString();
                    log.debug("Calculated discretionary discount amt" + discretionaryDisc);
                    clone.put("amt", discretionaryDisc);
                    clone.put("displayedTotalAmount", discretionaryDisc);
                    clone.put("userAmt", discretionaryDisc);
                    clone.put("qtyOffer", 1);

                    if (clone != null) {
                      int qty = Integer.parseInt(line.getString("qty"));
                      for (int j = 0; j < qty; j++) {
                        JSONObject jsonPromotion = new JSONObject(clone.toString());
                        boolean hasActualAmt = jsonPromotion.has("actualAmt");
                        if ((hasActualAmt && jsonPromotion.getDouble("actualAmt") == 0)
                            || (!hasActualAmt && jsonPromotion.getDouble("amt") == 0)) {
                          continue;
                        }
                        OrderLineOffer promotion = OBProvider.getInstance().get(
                            OrderLineOffer.class);
                        JSONPropertyToEntity.fillBobFromJSON(promotionLineEntity, promotion,
                            jsonPromotion, jsonorder.getLong("timezoneOffset"));
                        if (hasActualAmt) {
                          promotion.setTotalAmount(BigDecimal.valueOf(
                              jsonPromotion.getDouble("actualAmt")).setScale(pricePrecision,
                              RoundingMode.HALF_UP));
                        } else {
                          promotion.setTotalAmount(BigDecimal.valueOf(
                              jsonPromotion.getDouble("amt")).setScale(pricePrecision,
                              RoundingMode.HALF_UP));
                        }
                        promotion.setLineNo((long) ((j + 1) * 10));
                        promotion.setSalesOrderLine(orderline);
                        promotion.setObdiscQtyoffer(BigDecimal.ONE);
                        if (jsonPromotion.has("identifier") && !jsonPromotion.isNull("identifier")) {
                          String identifier = jsonPromotion.getString("identifier");
                          if (identifier.length() > 100) {
                            identifier = identifier.substring(identifier.length() - 100);
                          }
                          promotion.setObdiscIdentifier(identifier);
                        }
                        if (jsonPromotion.has("discountinstance")
                            && !jsonPromotion.isNull("discountinstance")) {
                          String discountinstance = jsonPromotion.getString("discountinstance");
                          promotion.setObposDiscountinstance(discountinstance);
                        }
                        promotion.setId(OBMOBCUtils.getUUIDbyString(orderline.getId() + (j + 100)));
                        promotion.setNewOBObject(true);
                        orderline.getOrderLineOfferList().add(promotion);

                        // ///

                        InvoiceLineOffer promo = OBProvider.getInstance().get(
                            InvoiceLineOffer.class);
                        JSONPropertyToEntity.fillBobFromJSON(invLinePromotionLineEntity, promo,
                            jsonPromotion, jsonorder.getLong("timezoneOffset"));
                        if (hasActualAmt) {
                          promo.setTotalAmount(BigDecimal.valueOf(
                              jsonPromotion.getDouble("actualAmt")).setScale(pricePrecision,
                              RoundingMode.HALF_UP));
                        } else {
                          promo.setTotalAmount(BigDecimal.valueOf(jsonPromotion.getDouble("amt"))
                              .setScale(pricePrecision, RoundingMode.HALF_UP));
                        }
                        promo.setLineNo((long) ((j + 1) * 10));
                        promo.setInvoiceLine(invoiceLine);
                        promo.setObdiscQtyoffer(BigDecimal.ONE);

                        promo.setId(OBMOBCUtils.getUUIDbyString(invoiceLine.getId() + (j + 100)));
                        promo.setNewOBObject(true);
                        invoiceLine.getInvoiceLineOfferList().add(promo);

                      }
                    }
                  }
                }
              }
            }
          } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
          }
        }
      }
    } catch (Exception e) {
      log.error(e.getMessage());
      e.printStackTrace();
    }
  }
}

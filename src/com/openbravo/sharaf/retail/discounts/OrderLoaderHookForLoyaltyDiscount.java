package com.openbravo.sharaf.retail.discounts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedOrdV;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class OrderLoaderHookForLoyaltyDiscount implements OrderLoaderHook {

  private static final org.apache.log4j.Logger log = org.apache.log4j.Logger
      .getLogger(OrderLoaderHookForLoyaltyDiscount.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {

    try {
      String clientId = jsonorder.getString("client");
      JSONArray lines = jsonorder.getJSONArray("lines");
      Map<String, String> promoMap = new HashMap<String, String>();

      boolean isLoyaltyDiscountPromotionQuantityUpdated = false;
      PriceAdjustment loyaltyDiscount = null;
      boolean isLoyaltyDisountProduct = false;

      boolean isVerifiedReturn = jsonorder.has("isVerifiedReturn")
          ? jsonorder.getBoolean("isVerifiedReturn")
          : false;
      boolean isLoyaltyDiscountLinesFullyReturned = jsonorder.has("isAllLinesFullReturn")
          ? jsonorder.getBoolean("isAllLinesFullReturn")
          : false;
      int qtyBB = 0;
      log.info("Invoked orderloaderhook for Loyalty discounts");

      for (int i = 0; i < lines.length(); i++) {
        try {
          String promoId = null;
          String custDisOrderLineId = null;
          PriceAdjustment mOffer = null;

          OfferLimit offerLimit = null;
          JSONObject line = lines.getJSONObject(i);

          if (line.has("custdisOffer")) {
            promoId = line.getString("custdisOffer");
          }
          if (line.has("custdisOrderline")) {
            custDisOrderLineId = line.getString("custdisOrderline");
          }

          if (promoId != null) {
            mOffer = OBDal.getInstance().get(PriceAdjustment.class, promoId);
          }
          if (line.has("custdisLoyaltyDiscountOffer")
              && line.getString("custdisLoyaltyDiscountOffer") != null) {
            isLoyaltyDisountProduct = true;
          }
          if (mOffer != null && mOffer.isActive() && isLoyaltyDisountProduct) {
            log.info("Offer is active " + promoId);
            Client client = OBDal.getInstance().get(Client.class, clientId);

            OBCriteria<OfferLimit> offerLimitCriteria = OBDal.getInstance()
                .createCriteria(OfferLimit.class);
            offerLimitCriteria.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, client));
            offerLimitCriteria.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, mOffer));
            if (offerLimitCriteria.list().size() > 0) {
              offerLimit = offerLimitCriteria.list().get(0);
            }

            if (offerLimitCriteria.list().size() != 0) {
              if (line.has("custdisOffer")
                  && (line.has("custdisOrderline") || (line.has("priceAdjPromo"))
                      || (line.has("promotions") && line.getJSONArray("promotions").length() > 0
                          && line.getJSONArray("promotions").getJSONObject(0)
                              .getString("discountType")
                              .equals("CA5491E6000647BD889B8D7CDF680795")))) {
                if (promoMap.containsKey(promoId)
                    && (jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BS")
                        || jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BR"))) {
                  continue;
                } else if (promoMap.containsKey(promoId)
                    && promoMap.containsValue(custDisOrderLineId)
                    && !(jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BS")
                        || jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BR"))) {
                  continue;
                } else {

                  if (line.has("custdisOrderline")) {
                    promoMap.put(promoId, line.getString("custdisOrderline"));
                  }
                  Long currentCountInDB = offerLimit.getConsumedCount();
                  if (currentCountInDB == null) {
                    currentCountInDB = 0L;
                  }
                  long qty = Integer.parseInt(line.getString("qty")) + currentCountInDB;
                  int qtyFromLine = Integer.parseInt(line.getString("qty"));

                  if (!isVerifiedReturn) {
                    if ((qtyFromLine < 0)
                        && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                      offerLimit.setConsumedCount((long) 0);
                    } else {
                      offerLimit.setConsumedCount(qty);
                    }
                  } else if (isLoyaltyDisountProduct) {
                    if (isLoyaltyDiscountLinesFullyReturned) {
                      if ((qtyFromLine < 0)
                          && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                        offerLimit.setConsumedCount((long) 0);
                      } else {
                        offerLimit.setConsumedCount(qty);
                      }
                    }
                  } else {
                    if ((qtyFromLine < 0)
                        && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                      offerLimit.setConsumedCount((long) 0);
                    } else {
                      offerLimit.setConsumedCount(qty);
                    }
                  }
                }
              }
            }
          }
          String loyaltyDiscountId = (line.has("custdisLoyaltyDiscountOffer")
              && line.getString("custdisLoyaltyDiscountOffer") != null)
                  ? line.getString("custdisLoyaltyDiscountOffer")
                  : null;
          if (isVerifiedReturn && loyaltyDiscountId == null) {
            String originalOderNumber = line.has("originalDocumentNo")
                ? line.getString("originalDocumentNo")
                : "";
            for (int k = 0; k < jsonorder.getJSONArray("lines").getJSONObject(0)
                .getJSONArray("originalPayments").length(); k++) {
              JSONObject pline = jsonorder.getJSONArray("lines").getJSONObject(0)
                  .getJSONArray("originalPayments").getJSONObject(k);

              if (pline.has("paymentData")) {
                if (pline.getJSONObject("paymentData") != null
                    && pline.getJSONObject("paymentData").has("data")) {
                  if (pline.getJSONObject("paymentData").getJSONObject("data") != null
                      && pline.getJSONObject("paymentData").getJSONObject("data")
                          .has("custdisLinkedPaymentMethod")
                      && pline.getJSONObject("paymentData").getJSONObject("data")
                          .getBoolean("custdisLinkedPaymentMethod")) {
                    OBQuery<Order> orderOriginal = OBDal.getInstance().createQuery(Order.class,
                        " documentNo = '" + originalOderNumber + "'");
                    OBCriteria<FIN_PaymentSchedOrdV> paySchList = OBDal.getInstance()
                        .createCriteria(FIN_PaymentSchedOrdV.class);
                    paySchList.add(Restrictions.eq(FIN_PaymentSchedOrdV.PROPERTY_SALESORDER,
                        orderOriginal.list().get(0)));
                    for (FIN_PaymentSchedOrdV x : paySchList.list()) {
                      List<FIN_PaymentDetailV> peyDetList = x.getFINPaymentDetailVList();
                      for (FIN_PaymentDetailV fin_PaymentDetailV : peyDetList) {
                        if (fin_PaymentDetailV.getPayment().getId()
                            .equalsIgnoreCase(pline.getString("paymentId"))
                            && fin_PaymentDetailV.getPayment().getCustdisMOffer() != null) {
                          loyaltyDiscountId = fin_PaymentDetailV.getPayment().getCustdisMOffer()
                              .getId();
                        }

                      }
                    }
                  }
                }
              }
            }
          }

          if (loyaltyDiscountId != null && isLoyaltyDisountProduct) {
            loyaltyDiscount = OBDal.getInstance().get(PriceAdjustment.class, loyaltyDiscountId);
            if (loyaltyDiscount != null && loyaltyDiscount.isActive()) {

              Client client = OBDal.getInstance().get(Client.class, clientId);
              OBCriteria<OfferLimit> offerLimitCriteriaLoyalty = OBDal.getInstance()
                  .createCriteria(OfferLimit.class);
              offerLimitCriteriaLoyalty.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, client));
              offerLimitCriteriaLoyalty
                  .add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, loyaltyDiscount));
              OfferLimit offerLimitLoyalty = null;
              if (offerLimitCriteriaLoyalty.list().size() > 0) {
                offerLimitLoyalty = offerLimitCriteriaLoyalty.list().get(0);
              }
              qtyBB = Integer.parseInt(line.getString("qty"));
              if (offerLimitCriteriaLoyalty.list().size() != 0 && offerLimitLoyalty != null) {
                if (!isLoyaltyDiscountPromotionQuantityUpdated) {
                  Long currentCountInDB = offerLimitLoyalty.getConsumedCount();
                  if (currentCountInDB == null) {
                    currentCountInDB = 0L;
                  }
                  long qty = Integer.parseInt(line.getString("qty")) + currentCountInDB;
                  int qtyFromLine = Integer.parseInt(line.getString("qty"));

                  if ((qtyFromLine < 0)
                      && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                    offerLimitLoyalty.setConsumedCount((long) 0);
                  } else {
                    offerLimitLoyalty.setConsumedCount(qty);
                  }

                  OBDal.getInstance().save(loyaltyDiscount);
                  OBDal.getInstance().save(offerLimitLoyalty);
                  isLoyaltyDiscountPromotionQuantityUpdated = true;
                }
              }
              if (line.has("custdisLoyaltyDiscountOffer")
                  && line.getString("custdisLoyaltyDiscountOffer") != null) {
                for (OrderLine orderLine : order.getOrderLineList()) {
                  if (orderLine.getId().equals(line.get("id"))) {
                    orderLine.setCustdisLoyaldisprmo(loyaltyDiscount);
                  }
                }
              }
            }
          }

        } catch (Exception e) {
          log.error(
              "Exception while updating consumedcount in orderLoaderHookForDiscounts.java (inside for loop) ",
              e);
        }
      }

      String linkedPaymentMethod = null;
      String documentNo = null;
      String cardNumber = null;
      String cardValidity = null;

      JSONArray paymentList = new JSONArray();
      if (isVerifiedReturn) {
        paymentList = jsonorder.getJSONArray("lines").getJSONObject(0)
            .getJSONArray("originalPayments");
      } else {
        paymentList = jsonorder.getJSONArray("payments");
      }

      for (int j = 0; j < paymentList.length(); j++) {
        JSONObject line = paymentList.getJSONObject(j);
        if (line.has("paymentData")) {
          if (line.getJSONObject("paymentData") != null
              && line.getJSONObject("paymentData").has("data")) {
            if (line.getJSONObject("paymentData").getJSONObject("data") != null
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .has("custdisLinkedPaymentMethod")
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .getBoolean("custdisLinkedPaymentMethod")) {
              if (line.getJSONObject("paymentData").getJSONObject("data")
                  .has("custdisAppliedPaymentMethod")) {
                linkedPaymentMethod = isVerifiedReturn ? "" : line.getString("id");
                cardNumber = line.getJSONObject("paymentData").getString("CardNumber");
                cardValidity = line.getJSONObject("paymentData").getString("CardValidity");
              }
            }
          }
        }
      }

      String loyaltyDiscountId = null;
      for (int i = 0; i < lines.length(); i++) {
        JSONObject line = lines.getJSONObject(i);
        if (loyaltyDiscountId == null) {
          loyaltyDiscountId = (line.has("custdisLoyaltyDiscountOffer")
              && line.getString("custdisLoyaltyDiscountOffer") != null
              && line.getString("custdisLoyaltyDiscountOffer") != "")
                  ? line.getString("custdisLoyaltyDiscountOffer")
                  : null;
        }
      }

      if (linkedPaymentMethod != null && loyaltyDiscount != null && loyaltyDiscountId != null) {
        if (!isVerifiedReturn) {
          FIN_Payment payment = new FIN_Payment();
          OBCriteria<FIN_PaymentSchedOrdV> paySchList = OBDal.getInstance()
              .createCriteria(FIN_PaymentSchedOrdV.class);
          paySchList.add(Restrictions.eq(FIN_PaymentSchedOrdV.PROPERTY_SALESORDER, order));
          for (FIN_PaymentSchedOrdV x : paySchList.list()) {
            List<FIN_PaymentDetailV> peyDetList = x.getFINPaymentDetailVList();
            for (FIN_PaymentDetailV fin_PaymentDetailV : peyDetList) {
              if (fin_PaymentDetailV.getPayment().getId().equalsIgnoreCase(linkedPaymentMethod)) {
                payment = fin_PaymentDetailV.getPayment();
              }
              if (fin_PaymentDetailV.getPayment().getId().equalsIgnoreCase(linkedPaymentMethod)) {

                documentNo = fin_PaymentDetailV.getPayment().getDocumentNo();
                fin_PaymentDetailV.getPayment().setCustdisLoyaltycardnumber(cardNumber);
                fin_PaymentDetailV.getPayment().setCustdisLoyaltyvalidity(cardValidity);

              }

            }
          }

          if (documentNo != null) {
            payment.setReferenceNo(documentNo);
          }
        }

        OBQuery<LoyaltyDiscountUsage> offerLimitList = OBDal.getInstance()
            .createQuery(LoyaltyDiscountUsage.class, " loyaltyCardNumber = '" + cardNumber
                + "' and offer.id = '" + loyaltyDiscount.getId() + "'");
        if (offerLimitList == null || offerLimitList.list().size() == 0) {
          LoyaltyDiscountUsage usage = new LoyaltyDiscountUsage();
          usage.setLoyaltyCardNumber(cardNumber);
          usage.setOffer(loyaltyDiscount);
          usage.setOrganization(loyaltyDiscount.getOrganization());
          usage.setConsumedCount(1L);
          usage.setOrder(order);
          OBDal.getInstance().save(usage);
        } else {
          LoyaltyDiscountUsage usage = offerLimitList.list().get(0);
          usage.setOrder(order);
          if (usage.getConsumedCount() == null) {
            usage.setConsumedCount(1L);
          } else {
            if (!isVerifiedReturn) {
              if (usage.getConsumedCount() + qtyBB < 0) {
                usage.setConsumedCount(0L);
              } else {
                usage.setConsumedCount(usage.getConsumedCount() + qtyBB);
              }
            } else {
              if (isVerifiedReturn && isLoyaltyDiscountLinesFullyReturned) {
                if (usage.getConsumedCount() + qtyBB < 0) {
                  usage.setConsumedCount(0L);
                } else {
                  usage.setConsumedCount(usage.getConsumedCount() + qtyBB);
                }
              }
            }
          }

          OBDal.getInstance().save(usage);
        }

      }
      log.info("completed updating consumed count for the current order");
    } catch (Exception e) {
      log.error("Exception while updating consumedcount in orderLoaderHookForDiscounts.java ", e);
    }

  }

}

package com.openbravo.sharaf.retail.discounts.utils;

import java.io.Writer;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import com.openbravo.sharaf.retail.discounts.BankBinPromoUsage;

import com.openbravo.sharaf.retail.discounts.OfferLimit;

public class WebServiceUtils {

  private static Logger log = Logger.getLogger(WebServiceUtils.class);

  public static int getLimitStatus(String mOfferId, String clientstr) throws Exception {

    int count = 0;

    try {
      Client client = OBDal.getInstance().get(Client.class, clientstr);
      PriceAdjustment moffer = OBDal.getInstance().get(PriceAdjustment.class, mOfferId);
      if (client == null) {
        throw new ClassNotFoundException("client object not found");
      }
      if (moffer == null) {
        throw new ClassNotFoundException("mOffer object not found");
      }

      OBCriteria<OfferLimit> offerLimitList = OBDal.getInstance().createCriteria(OfferLimit.class);
      offerLimitList.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, client));
      offerLimitList.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, moffer));

      if (offerLimitList.list().size() == 0) {
        // if no limit it is consider as -1 and promotion is applied successfully
        count = -1;
      }

      if (offerLimitList.list().size() > 0) {
        OfferLimit promotionLimit = offerLimitList.list().get(0);
        log.info("Promotion Limit: Long value received from db:"
            + promotionLimit.getConsumedCount());
        count = (promotionLimit.getConsumedCount()).intValue();

      }

    } catch (Exception e) {
      log.error("Promotion Limit: Error in print Web Service", e);
      throw new Exception("Promotion Limit: Error in print Web Service", e);
    }

    return count;

  }

  public static int getFairUsageLimitStatus(String mOfferId, String clientstr, String cardNumber) throws Exception {

    int promotionCount = 0;

    try {
      Client client = OBDal.getInstance().get(Client.class, clientstr);
      PriceAdjustment moffer = OBDal.getInstance().get(PriceAdjustment.class, mOfferId);
      if (client == null) {
        throw new ClassNotFoundException("client object not found");
      }
      if (moffer == null) {
        throw new ClassNotFoundException("mOffer object not found");
      }
      if (moffer != null && (!moffer.isActive())) {
        log.error("Offer is not active for offer:" + moffer.getName());
        throw new Exception("Offer is not active for offer:" + moffer.getName());
      }
      OBQuery<BankBinPromoUsage> offerLimitList = OBDal.getInstance().createQuery(
          BankBinPromoUsage.class,
          " creditCardNumber = '" + cardNumber + "' and offer.id = '" + mOfferId + "'");

      if (offerLimitList.list().size() == 0) {
        // if no limit it is consider as -1 and promotion is applied successfully
        promotionCount = -1;
      }

      if (offerLimitList.list().size() > 0) {
        BankBinPromoUsage promotionLimit = offerLimitList.list().get(0);
        log.info("Fair Usage Limit: Long value received from db:"
            + promotionLimit.getConsumedCount());
        promotionCount = (promotionLimit.getConsumedCount()).intValue();
      }
    } catch (Exception e) {
      log.error("Fair Usage Limit: Error in print Web Service", e);
      throw new Exception("Fair Usage Limit: Error in print Web Service", e);
    }

    return promotionCount;

  }

  public static void writeResults(HttpServletResponse response, int count) throws Exception {
    try {
      String result = "{\"count\":" + count + "}";
      response.setContentType("application/json);charset=UTF-8");
      final Writer w = response.getWriter();
      w.write(result);
      response.setStatus(HttpServletResponse.SC_OK);
      w.close();
    } catch (Exception e) {
      throw new Exception("Promotion Limit: Error in writing results to response", e);
    }

  }

}

package com.openbravo.sharaf.retail.discounts;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetailV;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedOrdV;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.pricing.priceadjustment.PriceAdjustment;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class OrderLoaderHookForDiscounts implements OrderLoaderHook {

  private static final Logger log = Logger.getLogger(OrderLoaderHookForDiscounts.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {

    try {
      String clientId = jsonorder.getString("client");
      JSONArray lines = jsonorder.getJSONArray("lines");
      Map<String, String> promoMap = new HashMap<String, String>();
      boolean isBankBinPromotionQuantityUpdated = false;
      PriceAdjustment mOfferBankBin = null;
      boolean isBankBinProduct = false;
      boolean isVerifiedReturn = jsonorder.has("isVerifiedReturn")
          ? jsonorder.getBoolean("isVerifiedReturn")
          : false;
      boolean isBankBinLinesFullyReturned = jsonorder.has("isAllLinesFullReturn")
        		  ? jsonorder.getBoolean("isAllLinesFullReturn")
        		  : false;   
      int qtyBB = 0;

      log.info("Invoked orderloaderhook for discounts");
      for (int i = 0; i < lines.length(); i++) {
        try {
          String promoId = null;
          String custDisOrderLineId = null;
          PriceAdjustment mOffer = null;

          OfferLimit offerLimit = null;
          JSONObject line = lines.getJSONObject(i);

          if (line.has("custdisOffer")) {
            promoId = line.getString("custdisOffer");
          }
          if (line.has("custdisOrderline")) {
            custDisOrderLineId = line.getString("custdisOrderline");
          }

          if (promoId != null) {
            mOffer = OBDal.getInstance().get(PriceAdjustment.class, promoId);
          }
          if (line.has("custdisBankBinOffer") && line.getString("custdisBankBinOffer") != null) {
        	  isBankBinProduct = true;
        	  }
          if (mOffer != null && mOffer.isActive()) {
            log.info("Offer is active " + promoId);
            Client client = OBDal.getInstance().get(Client.class, clientId);

            OBCriteria<OfferLimit> offerLimitCriteria = OBDal.getInstance()
                .createCriteria(OfferLimit.class);
            offerLimitCriteria.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, client));
            offerLimitCriteria.add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, mOffer));
            if (offerLimitCriteria.list().size() > 0) {
              offerLimit = offerLimitCriteria.list().get(0);
            }

            if (offerLimitCriteria.list().size() != 0) {
              if (line.has("custdisOffer")
                  && (line.has("custdisOrderline") || (line.has("priceAdjPromo"))
                      || (line.has("promotions") && line.getJSONArray("promotions").length() > 0
                          && line.getJSONArray("promotions").getJSONObject(0)
                              .getString("discountType")
                              .equals("CA5491E6000647BD889B8D7CDF680795")))) {
                if (promoMap.containsKey(promoId)
                    && (jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BS")
                        || jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BR"))) {
                  continue;
                } else if (promoMap.containsKey(promoId)
                    && promoMap.containsValue(custDisOrderLineId)
                    && !(jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BS")
                        || jsonorder.getString("custsdtDocumenttypeSearchKey").equals("BR"))) {
                  continue;
                } else {
                  if (line.has("custdisOrderline")) {
                    promoMap.put(promoId, line.getString("custdisOrderline"));
                  }
                  Long currentCountInDB = offerLimit.getConsumedCount();
                  if (currentCountInDB == null) {
                    currentCountInDB = 0L;
                  }
                  long qty = Integer.parseInt(line.getString("qty")) + currentCountInDB;
                  int qtyFromLine = Integer.parseInt(line.getString("qty"));

                  if (!isVerifiedReturn) {
                      if ((qtyFromLine < 0)
                          && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                        offerLimit.setConsumedCount((long) 0);
                      } else {
                        offerLimit.setConsumedCount(qty);
                      }
                  } else if (isBankBinProduct) {
                	  if (isBankBinLinesFullyReturned) {
                        	  if ((qtyFromLine < 0)
                        	  && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                        	  offerLimit.setConsumedCount((long) 0);
                        	  } else {
                        	  offerLimit.setConsumedCount(qty);
                        	  }
                	  }
                }else {
                      if ((qtyFromLine < 0)
                      && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                      offerLimit.setConsumedCount((long) 0);
                      } else {
                      offerLimit.setConsumedCount(qty);
                      }
                }

                  OBDal.getInstance().save(mOffer);
                  OBDal.getInstance().save(offerLimit);
                }
              }
            }
          }
          String bankBinId = (line.has("custdisBankBinOffer")
              && line.getString("custdisBankBinOffer") != null)
                  ? line.getString("custdisBankBinOffer")
                  : null;

          if (isVerifiedReturn && bankBinId == null) {
            String originalOderNumber = line.has("originalDocumentNo")
                ? line.getString("originalDocumentNo")
                : "";
            for (int k = 0; k < jsonorder.getJSONArray("lines").getJSONObject(0)
                .getJSONArray("originalPayments").length(); k++) {
              JSONObject pline = jsonorder.getJSONArray("lines").getJSONObject(0)
                  .getJSONArray("originalPayments").getJSONObject(k);

              if (pline.has("paymentData")) {
                if (pline.getJSONObject("paymentData") != null
                    && pline.getJSONObject("paymentData").has("data")) {
                  if (pline.getJSONObject("paymentData").getJSONObject("data") != null
                      && pline.getJSONObject("paymentData").getJSONObject("data")
                          .has("custdisLinkedPaymentMethod")
                      && pline.getJSONObject("paymentData").getJSONObject("data")
                          .getBoolean("custdisLinkedPaymentMethod")) {
                    OBQuery<Order> orderOriginal = OBDal.getInstance().createQuery(Order.class,
                        " documentNo = '" + originalOderNumber + "'");
                    OBCriteria<FIN_PaymentSchedOrdV> paySchList = OBDal.getInstance()
                        .createCriteria(FIN_PaymentSchedOrdV.class);
                    paySchList.add(Restrictions.eq(FIN_PaymentSchedOrdV.PROPERTY_SALESORDER,
                        orderOriginal.list().get(0)));
                    for (FIN_PaymentSchedOrdV x : paySchList.list()) {
                      List<FIN_PaymentDetailV> peyDetList = x.getFINPaymentDetailVList();
                      for (FIN_PaymentDetailV fin_PaymentDetailV : peyDetList) {
                        if (fin_PaymentDetailV.getPayment().getId()
                            .equalsIgnoreCase(pline.getString("paymentId"))
                            && fin_PaymentDetailV.getPayment().getCustdisMOffer() != null) {
                          bankBinId = fin_PaymentDetailV.getPayment().getCustdisMOffer().getId();
                        }

                      }
                    }
                  }
                }
              }
            }
          }

          if (bankBinId != null) {
            mOfferBankBin = OBDal.getInstance().get(PriceAdjustment.class, bankBinId);
            if (mOfferBankBin != null && mOfferBankBin.isActive()) {

              Client client = OBDal.getInstance().get(Client.class, clientId);
              OBCriteria<OfferLimit> offerLimitCriteriaBankBin = OBDal.getInstance()
                  .createCriteria(OfferLimit.class);
              offerLimitCriteriaBankBin.add(Restrictions.eq(OfferLimit.PROPERTY_CLIENT, client));
              offerLimitCriteriaBankBin
                  .add(Restrictions.eq(OfferLimit.PROPERTY_PROMOTIONDISCOUNT, mOfferBankBin));
              OfferLimit offerLimitBankBin = null;
              if (offerLimitCriteriaBankBin.list().size() > 0) {
                offerLimitBankBin = offerLimitCriteriaBankBin.list().get(0);
              }
              qtyBB = Integer.parseInt(line.getString("qty"));
              if (offerLimitCriteriaBankBin.list().size() != 0 && offerLimitBankBin != null) {
                if (!isBankBinPromotionQuantityUpdated) {
                  Long currentCountInDB = offerLimitBankBin.getConsumedCount();
                  if (currentCountInDB == null) {
                    currentCountInDB = 0L;
                  }
                  long qty = Integer.parseInt(line.getString("qty")) + currentCountInDB;
                  int qtyFromLine = Integer.parseInt(line.getString("qty"));

                  if ((qtyFromLine < 0)
                      && (currentCountInDB == null || currentCountInDB == 0 || qty < 0)) {
                    offerLimitBankBin.setConsumedCount((long) 0);
                  } else {
                    offerLimitBankBin.setConsumedCount(qty);
                  }

                  OBDal.getInstance().save(mOfferBankBin);
                  OBDal.getInstance().save(offerLimitBankBin);
                  isBankBinPromotionQuantityUpdated = true;
                }
              }
                if (line.has("custdisBankBinOffer")
                    && line.getString("custdisBankBinOffer") != null) {
                  for (OrderLine orderLine : order.getOrderLineList()) {
                    if (orderLine.getId().equals(line.get("id"))) {
                      orderLine.setCustdisBankBin(mOfferBankBin);
                    }
                  }
                }
            }
          }

        } catch (Exception e) {
          log.error(
              "Exception while updating consumedcount in orderLoaderHookForDiscounts.java (inside for loop) ",
              e);
        }
      }

      // Payment's In for Bank Bin Promotion
      String mainPaymentMethod = null;
      String linkedPaymentMethod = null;
      String documentNo = null;
      String cardNumber = null;
      boolean isCustdisMainPaymentMethod = false;

      JSONArray paymentList = new JSONArray();
      if (isVerifiedReturn) {
        paymentList = jsonorder.getJSONArray("lines").getJSONObject(0)
            .getJSONArray("originalPayments");
      } else {
        paymentList = jsonorder.getJSONArray("payments");
      }

      for (int j = 0; j < paymentList.length(); j++) {
        JSONObject line = paymentList.getJSONObject(j);

        if (line.has("paymentData")) {
          if (line.getJSONObject("paymentData") != null
              && line.getJSONObject("paymentData").has("data")) {
            if (line.getJSONObject("paymentData").getJSONObject("data") != null
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .has("custdisLinkedPaymentMethod")
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .getBoolean("custdisLinkedPaymentMethod")) {
              if (line.getJSONObject("paymentData").getJSONObject("data")
                  .has("custdisAppliedPaymentMethod")) {
                linkedPaymentMethod = isVerifiedReturn ? "" : line.getString("id");
              }
            } else if (line.getJSONObject("paymentData").getJSONObject("data") != null
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .has("custdisMainPaymentMethod")
                && line.getJSONObject("paymentData").getJSONObject("data")
                    .getBoolean("custdisMainPaymentMethod")) {
              isCustdisMainPaymentMethod = true;
              mainPaymentMethod = isVerifiedReturn ? "" : line.getString("id");
              if (line.has("paymentData") && line.getJSONObject("paymentData").has("provider")
                  && line.getJSONObject("paymentData").getJSONObject("provider")
                      .has("cpseftProviderCode")
                  && line.getJSONObject("paymentData").getJSONObject("provider")
                      .getString("cpseftProviderCode").equals("integrated.mashreq")) {
                cardNumber = line.getJSONObject("paymentData").getJSONObject("properties")
                    .getString("cardNumber");
              } else {
                cardNumber = line.has("eftCardDetails") ? line.getString("eftCardDetails")
                    : line.getJSONObject("paymentData").getJSONObject("data")
                        .getString("SHARCCCardNumber");
              }

            }
          }
        }
      }
      String bankBinId = null;
      for (int i = 0; i < lines.length(); i++) {
        JSONObject line = lines.getJSONObject(i);
        if(bankBinId == null) {
          bankBinId = (line.has("custdisBankBinOffer")
            && line.getString("custdisBankBinOffer") != null)
                ? line.getString("custdisBankBinOffer")
                : null;
        }
        if (line.has("custdisBankBinOffer") && line.getString("custdisBankBinOffer") != null) {
        	isBankBinProduct = true;
        	}
        }
      if (mainPaymentMethod != null && linkedPaymentMethod != null && mOfferBankBin != null
          && isCustdisMainPaymentMethod && bankBinId != null) {
        if (!isVerifiedReturn) {
          FIN_Payment payment = new FIN_Payment();
          OBCriteria<FIN_PaymentSchedOrdV> paySchList = OBDal.getInstance()
              .createCriteria(FIN_PaymentSchedOrdV.class);
          paySchList.add(Restrictions.eq(FIN_PaymentSchedOrdV.PROPERTY_SALESORDER, order));
          for (FIN_PaymentSchedOrdV x : paySchList.list()) {
            List<FIN_PaymentDetailV> peyDetList = x.getFINPaymentDetailVList();
            for (FIN_PaymentDetailV fin_PaymentDetailV : peyDetList) {
              if (fin_PaymentDetailV.getPayment().getId().equalsIgnoreCase(mainPaymentMethod)
                  && isCustdisMainPaymentMethod) {
                payment = fin_PaymentDetailV.getPayment();
              }
              if (fin_PaymentDetailV.getPayment().getId().equalsIgnoreCase(linkedPaymentMethod)) {
                documentNo = fin_PaymentDetailV.getPayment().getDocumentNo();
                fin_PaymentDetailV.getPayment().setCustdisMOffer(mOfferBankBin);
              }

            }
          }

          if (documentNo != null) {
            payment.setReferenceNo(documentNo);
          }
        }

        cardNumber = cardNumber.contains("******") ? cardNumber
            : cardNumber.replace("*****", "******");
        OBQuery<BankBinPromoUsage> offerLimitList = OBDal.getInstance()
            .createQuery(BankBinPromoUsage.class, " creditCardNumber = '" + cardNumber
                + "' and offer.id = '" + mOfferBankBin.getId() + "'");
        if (offerLimitList != null && offerLimitList.list().size() == 0) {
          BankBinPromoUsage usage = new BankBinPromoUsage();
          usage.setCreditCardNumber(cardNumber);
          usage.setOffer(mOfferBankBin);
          usage.setOrganization(mOfferBankBin.getOrganization());
          usage.setConsumedCount(1L);
          OBDal.getInstance().save(usage);
        } else {
          BankBinPromoUsage usage = offerLimitList.list().get(0);
          if (usage.getConsumedCount() == null) {
            usage.setConsumedCount(1L);
          } else {
        	  if (!isVerifiedReturn && isBankBinProduct) {
            if (usage.getConsumedCount() + qtyBB < 0) {
              usage.setConsumedCount(0L);
            } else {
              usage.setConsumedCount(usage.getConsumedCount() + qtyBB);
              }
        	  } else {
        	  if (isVerifiedReturn && isBankBinProduct && isBankBinLinesFullyReturned) {
        	  if (usage.getConsumedCount() + qtyBB < 0) {
        	  usage.setConsumedCount(0L);
        	  } else {
        	  usage.setConsumedCount(usage.getConsumedCount() + qtyBB);
        	  }
        	  }
            }
          }

          OBDal.getInstance().save(usage);
        }

      }
      log.info("completed updating consumed count for the current order");
    } catch (Exception e) {
      log.error("Exception while updating consumedcount in orderLoaderHookForDiscounts.java ", e);
      e.printStackTrace();
    }

  }

}
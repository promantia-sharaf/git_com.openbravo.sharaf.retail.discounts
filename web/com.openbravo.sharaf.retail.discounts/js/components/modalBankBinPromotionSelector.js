/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTDIS.BankBinListPromotionsLine',
  kind: 'OB.UI.listItemButton',
  style: 'line-height: 23px; width: 100%',
  components: [{
    name: 'line',
    style: 'line-height: 23px; width: 100%',
    components: [{
      name: 'textInfo',
      style: 'float: left; ',
      components: [{
        style: 'display: table;',
        components: [{
          style: 'display: table-row;',
          components: [{
            name: 'identifier',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 14px; font-style: italic; color: #888888;',
          components: [{
            name: 'dateRange',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 12px; font-style: italic; color: #f8941d;',
          components: [{
            name: 'promoInfo',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 12px; font-style: italic; color: #6cb33f;',
          components: [{
            name: 'products',
            style: 'display: table-cell;'
          }]
        }]
      }]
    }, {
      style: 'display: table; float: right; text-align: right; vertical-align: middle ',
      components: [{
        style: 'display: table-row;',
        name: 'bPGroup'
      }]
    }]
  }],
  create: function () {
    this.inherited(arguments);
    this.$.identifier.setContent(this.model.get('_identifier') + (this.model.get('printName') ? ' / ' + this.model.get('printName') : ''));
    this.$.dateRange.setContent(this.model.get('startingDate') + (this.model.get('endingDate') ? ' - ' + this.model.get('endingDate') : ''));
    var promoInfo = this.model.get('obshintPromoDocno') ? this.model.get('obshintPromoDocno') : '';
    if (this.model.get('obshintPromoDesc')) {
      promoInfo += (promoInfo !== '' ? ' / ' : '') + this.model.get('obshintPromoDesc');
    }
    this.$.promoInfo.setContent(OB.I18N.getLabel('CUSTDIS_MinPurchaseAmount') + " " + OB.I18N.formatCurrency(this.model.get('custdisMinPurchaseAmount')) + " " + OB.I18N.getLabel('CUSTDIS_MaxDiscountAmount') + " " + OB.I18N.formatCurrency(this.model.get('custdisMaxDiscountAmt')));
    this.$.products.setContent(OB.I18N.getLabel('CUSTDIS_ApplicableDiscount') + " " + OB.I18N.formatCurrency(this.model.get('custdisApplicableDiscountAmount')));
    if (this.model.get('includedBPCategories') === 'N') {
      if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Gold') {
        this.$.bPGroup.setStyle('background-color: #FFDF00 !important; color: #000000 !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Silver') {
        this.$.bPGroup.setStyle('background-color: #D3D3D3 !important; color: #FFFFFF !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Platinum') {
        this.$.bPGroup.setStyle('background-color: #E5E4E2 !important; color: #000000 !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else {
        this.$.bPGroup.setStyle('background-color: #000000 !important; color: #FFFFFF !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      }
      this.$.bPGroup.setContent(OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name'));
    }
  }
});

/* Scrollable table (body of modal) */
enyo.kind({
  name: 'CUSTDIS.BankBinListPromotions',
  classes: 'row-fluid',
  promotionsList: null,
  events: {
    onSelectPromotion: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'bbstPromotionSelector',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '420px',
          renderLine: 'CUSTDIS.BankBinListPromotionsLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],

  init: function (model) {
    var me = this;
    this.promotionsList = new Backbone.Collection();
    this.$.bbstPromotionSelector.setCollection(this.promotionsList);
    this.promotionsList.on('click', function (item) {
      me.doSelectPromotion(item);
    });
  }
});

enyo.kind({
  kind: 'OB.UI.Modal',
  name: 'CUSTDIS.ModalBankBinPromotionSelector',
  i18nHeader: 'CUSTDIS_LblPromotionSelectorCaption',
  topPosition: '60px',
  events: {
    onHideThisPopup: ''
  },
  handlers: {
    onSelectPromotion: 'selectPromotion'
  },
  //body of the popup
  body: {
    kind: 'CUSTDIS.BankBinListPromotions'
  },
  selectPromotion: function (inSender, inEvent) {
    this.promotion = inEvent;
    
    var me = this;
    // validation of qtylimit check
    var qtyPerLimit = null;
    var productInfo = []; // Array to store descriptions of products that exceed the limit
    var isQtyCheck = false;

    if (me.args.order.attributes.lines.models.length > 0 && me.args.promotions.models.length > 0) {
        var linesModels = me.args.order.attributes.lines.models;
        var promotionsModels = me.args.promotions.models;

        var productCounts = {};

        for (var j = 0; j < promotionsModels.length; j++) {
            var models = promotionsModels[j];
            if(this.promotion.id === models.id){
                var custdisProductLines = models.get('custdisProductLines');
                qtyPerLimit = models.get('custdisPerqtyart');

                for (var k = 0; k < custdisProductLines.length; k++) {
                    var productId = custdisProductLines[k].id;
                    productCounts[productId] = 0; // Initialize counts for each product
                }
            }
        }

        for (var i = 0; i < linesModels.length; i++) {
            var lines = linesModels[i];
            var productId = lines.get('product').get('id');
            if(lines.get('product').get('productType') != 'S') {
               if (productCounts[productId] !== undefined) {
                if(OB.UTIL.isNullOrUndefined(lines.get('custdisAddByPromo')) && !lines.get('custdisAddByPromo')){
                   productCounts[productId]++;
                if (!OB.UTIL.isNullOrUndefined(qtyPerLimit) && qtyPerLimit != '' && productCounts[productId] > qtyPerLimit) {
                    if (!productInfo.includes(lines.get('product').get('description'))) {
                        productInfo.push(lines.get('product').get('description'));
                    }
                    isQtyCheck = true; // Flag to indicate at least one product exceeds the limit
                }
            }
            } 
            }    
        }
    }

    var proceedProcess = function() {
      me.args.order.set('custdisMaxDiscountAmt', me.promotion.get('custdisMaxDiscountAmt'));
      me.args.order.set('custdisDiscountPercentage', me.promotion.get('custdisDiscountPercentage'));
      me.args.order.set('custdisLinkedPaymentMethod', me.promotion.get('custdisLinkedPaymentMethod'));
      me.args.order.set('custdisMinPurchaseAmountBankBin', me.promotion.get('custdisMinPurchaseAmount')); 
      me.args.order.set('custdisSelectedLineAmount', me.promotion.get('custdisSelectedLineAmount'));  
      if(me.promotion.get('discountType')  === '25ECBE384EAB4C1EB69948FF99FC8456') {
          me.args.order.set('custDisLoyaltyDiscountPromotion', me.promotion);
          OB.MobileApp.model.receipt.set('custDisLoyaltyDiscountPromotion',me.promotion);
      }else {
          me.args.order.set('custDisBankBinPromotion', me.promotion); 
      }  
      me.promotion.unset('custdisSelectedLineAmount');
      me.doHideThisPopup();
      
      var loyaltyIssued = false;
      OB.MobileApp.model.receipt.get('lines').models.forEach(function(line) {
        if (!OB.UTIL.isNullOrUndefined(line.get('custdisLoyaltyDiscountOffer'))) {
          loyaltyIssued = true;  
        }
      }); 
            
      if(!OB.UTIL.isNullOrUndefined(me.promotion.get('discountType')) &&  me.promotion.get('discountType') === '25ECBE384EAB4C1EB69948FF99FC8456') {
           OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
             popup: 'OB_UI_ModalLoyalityCardNumber',
             args: {
                 promotions: me.promotion,
                 args: me.args.args,
                 order: me.args.order,
                 callbacks: me.args.callbacks 
          }});
      } else if(!loyaltyIssued){
         OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
         popup: 'OB_UI_ModalCardBinNumber',
         args: {
             promotions: me.promotion,
             args: me.args.args,
             order: me.args.order,
             callbacks: me.args.callbacks 
         }});
      } else if(loyaltyIssued){
         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Bank Bin Promotion not applicable as Loyalty Program discount has already been applied in the invoice");
      }
    }
    if (!OB.UTIL.isNullOrUndefined(this.promotion.get('custdisSellableLimit')) && this.promotion.get('custdisSellableLimit') > 0) {
         var sellableLimit =  this.promotion.get('custdisSellableLimit') ;
        new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.PromotionLimitService').exec({
               mofferid: me.promotion.get('id'),
               client: OB.MobileApp.model.attributes.terminal.client
           }, function(data) {
               if (data && data.exception) {
                   if (!OB.UTIL.isNullOrUndefined(data.exception.status) && !OB.UTIL.isNullOrUndefined(data.exception.status.message)) {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),data.exception.status.message);
                   } else {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),data.exception.message);
                   }
                   me.doHideThisPopup();
                   //-------
               } else {
                   if(!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.remainingqty)){
                      var percentage = 0;
                      if (data.remainingqty > 0) {
                           percentage = Math.round(((sellableLimit-data.remainingqty)/sellableLimit)*100);
                      }
                      if (percentage >= 90 && percentage < 100 ){
                          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_PermittedQtyLimitAlert', [percentage]));
                          me.doHideThisPopup();
                          proceedProcess();
                      }
                       if (data.remainingqty <= 0) {
                         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  "Promo limit exceeded");
                         me.doHideThisPopup();
                       } else if(isQtyCheck){
                    	   OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDIS_PermittedQtyLimit',[productInfo,qtyPerLimit]));
                           me.doHideThisPopup(); 
                       }else {
                         proceedProcess();
                       }                       
                   } else {
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  "Promo limit exceeded");
                      me.doHideThisPopup();
                   }
               }
          });
    }else if(isQtyCheck){
 	   OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDIS_PermittedQtyLimit',[productInfo,qtyPerLimit]));
       me.doHideThisPopup(); 
   }else {
      proceedProcess();
    }
  },
  loadPromoProducts: function (promos, callback) {
    var finishLoadProd = function () {
        var finish = true;
        _.each(promos, function (promo) {
          if (promo.get('productsLoaded') === false) {
            finish = false;
          }
        });
        if (finish) {
          callback();
        }
        };

    _.each(promos, function (promo) {
      promo.set('productsLoaded', false);
    });

    _.each(promos, function (promo) {
      var select = "select m.em_custdis_primary_newprice, m.em_custdis_secondary_newprice, op.m_product_id, p._identifier, p.standardPrice, op.em_custdis_giftqty, op.em_custdis_amount, op.em_custdis_discountamount, op.em_custdis_percentage, op.em_custdis_ir_type, op.em_custdis_is_gift " + //
      "from m_offer_product op join m_product p on op.m_product_id = p.m_product_id " + //
      "join m_offer m on m.m_offer_id = op.m_offer_id " +
      "where op.m_offer_id = '" + promo.id + "'";
      var discType = promo.get('discountType');
      OB.Dal.query(OB.Model.DiscountProductQuery, select, [], function (products) {
          var info = '',
          giftItemArr = [];
            products.models.forEach(function (prd){ // to add discount type to prd
            if(discType === '1424B47A341E4D38B0F61CCF26450AF3'){
                prd.set('discType','1424B47A341E4D38B0F61CCF26450AF3');
            }
          });
        
        _.each(products.models, function (prd) {
          if (!prd.get('isGift')) {
            var discountAmount = OB.CUSTDIS.Utils.calculateLineDiscount(prd.get('standardPrice'), prd);
            if (discountAmount) {
              info += OB.I18N.getLabel('CUSTDIS_lblPromoSelectorDiscount') + ': ' + OB.I18N.formatCurrency(discountAmount);
            }
          }
        });

        _.each(products.models, function (prd) {
            if (discType === '1424B47A341E4D38B0F61CCF26450AF3') {
                prd.set('isGift', true);
            }
            if (prd.get('isGift')) {
                var giftQty = (!OB.UTIL.isNullOrUndefined(prd.get('custdisGiftQty'))) ? prd.get('custdisGiftQty') : 1 ;
                if (info !== '') {
                    info += ', ';
                }
                info += prd.get('name');
                if (prd.get('irType')) {
                    info += ' (' + OB.I18N.formatCurrency(prd.get('discountAmount')) + ')';
                }
                for (i = 0; i < giftQty; i++) {
                    giftItemArr.push(prd.get('name'));
                }
            }
            if (discType === '1424B47A341E4D38B0F61CCF26450AF3') {
                prd.set('isGift', false);
            }
        });
        promo.set('productsLoaded', true);
        promo.set('products', giftItemArr);
        finishLoadProd();
      }, function () {
        promo.set('productsLoaded', true);
        finishLoadProd();
      });
    });
  },
  
  executeOnShow: function () {
   var me = this;
   me.args.order.set('custDisBankBinPromotionFlowInProgress', true);
   this.promotion = null;
   
     this.loadPromoProducts(this.args.promotions.models, function () {
     me.$.body.$.bankBinListPromotions.$.bbstPromotionSelector.collection.reset(me.args.promotions.models);
   });

 },
  executeOnHide: function () {
    this.args.order.set('custDisBankBinPromotionFlowInProgress', false);
    if(!OB.UTIL.isNullOrUndefined(this.args.order.get('custDisBankBinPromotion'))) {
      var promotionid = this.args.order.get('custDisBankBinPromotion').get('id');
    }
    _.each(this.args.promotions.models, function(promotion) {
       promotion.unset('custdisSelectedLineAmount');
       if(!OB.UTIL.isNullOrUndefined(promotionid)) {
        if(promotion.id !== promotionid) {
           promotion.unset('custdisProductLines');
         }
       }
    });
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTDIS.ModalBankBinPromotionSelector',
  name: 'CUSTDIS_ModalBankBinPromotionSelector'
});

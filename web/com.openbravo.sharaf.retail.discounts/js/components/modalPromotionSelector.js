/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo moment */

enyo.kind({
  name: 'CUSTDIS.ListPromotionsLine',
  kind: 'OB.UI.listItemButton',
  style: 'line-height: 23px; width: 100%',
  components: [{
    name: 'line',
    style: 'line-height: 23px; width: 100%',
    components: [{
      name: 'textInfo',
      style: 'float: left; ',
      components: [{
        style: 'display: table;',
        components: [{
          style: 'display: table-row;',
          components: [{
            name: 'identifier',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 14px; font-style: italic; color: #888888;',
          components: [{
            name: 'dateRange',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 12px; font-style: italic; color: #f8941d;',
          components: [{
            name: 'promoInfo',
            style: 'display: table-cell;'
          }]
        }, {
          style: 'display: table-row; font-size: 12px; font-style: italic; color: #6cb33f;',
          components: [{
            name: 'products',
            style: 'display: table-cell;'
          }]
        }]
      }]
    }, {
      style: 'display: table; float: right; text-align: right; vertical-align: middle ',
      components: [{
        style: 'display: table-row;',
        name: 'bPGroup'
      }]
    }]
  }],
  create: function () {
    this.inherited(arguments);
    this.$.identifier.setContent(this.model.get('_identifier') + (this.model.get('printName') ? ' / ' + this.model.get('printName') : ''));
    this.$.dateRange.setContent(this.model.get('startingDate') + (this.model.get('endingDate') ? ' - ' + this.model.get('endingDate') : ''));
    var promoInfo = this.model.get('obshintPromoDocno') ? this.model.get('obshintPromoDocno') : '';
    if (this.model.get('obshintPromoDesc')) {
      promoInfo += (promoInfo !== '' ? ' / ' : '') + this.model.get('obshintPromoDesc');
    }
    this.$.promoInfo.setContent(promoInfo);
    this.$.products.setContent(this.model.get('products'));
    if (this.model.get('includedBPCategories') === 'N') {
      if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Gold') {
        this.$.bPGroup.setStyle('background-color: #FFDF00 !important; color: #000000 !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Silver') {
        this.$.bPGroup.setStyle('background-color: #D3D3D3 !important; color: #FFFFFF !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else if (OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name') === 'Platinum') {
        this.$.bPGroup.setStyle('background-color: #E5E4E2 !important; color: #000000 !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      } else {
        this.$.bPGroup.setStyle('background-color: #000000 !important; color: #FFFFFF !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
      }
      this.$.bPGroup.setContent(OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name'));
    }
    if (this.model.get('discountType') === '61D65EECCF97487A82D169BB7E96832C') {
     this.$.promoInfo.setStyle('color: #000000; font-size: 16px;');
     this.$.promoInfo.setContent('Minimum Invoice of ' + OB.MobileApp.model.receipt.attributes.currency$_identifier + ' ' + this.model.get('custdisTotalMinAmount'));
        this.$.bPGroup.setStyle('background-color: #f8a824 !important; color: #000000 !important; border: none; display: inline-block; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;');
        this.$.bPGroup.setContent('CART PROMO');
    }
  }
});

/* Scrollable table (body of modal) */
enyo.kind({
  name: 'CUSTDIS.ListPromotions',
  classes: 'row-fluid',
  promotionsList: null,
  events: {
    onSelectPromotion: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'stPromotionSelector',
          kind: 'OB.UI.ScrollableTable',
          scrollAreaMaxHeight: '420px',
          renderLine: 'CUSTDIS.ListPromotionsLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],

  init: function (model) {
    var me = this;
    this.promotionsList = new Backbone.Collection();
    this.$.stPromotionSelector.setCollection(this.promotionsList);
    this.promotionsList.on('click', function (item) {
      me.doSelectPromotion(item);
    });
  }
});

enyo.kind({
  kind: 'OB.UI.Modal',
  name: 'CUSTDIS.ModalPromotionSelector',
  i18nHeader: 'CUSTDIS_LblPromotionSelectorCaption',
  topPosition: '60px',
  events: {
    onHideThisPopup: ''
  },
  handlers: {
    onSelectPromotion: 'selectPromotion'
  },
  //body of the popup
  body: {
    kind: 'CUSTDIS.ListPromotions'
  },
  selectPromotion: function (inSender, inEvent) {
    this.promotion = inEvent;
    this.doHideThisPopup();
  },
  loadPromoProducts: function (promos, callback) {
    var me = this;
    var finishLoadProd = function () {
        var finish = true;
        _.each(promos, function (promo) {
          if (promo.get('productsLoaded') === false) {
            finish = false;
          }
        });
        if (finish) {
          callback();
        }
        };

    _.each(promos, function (promo) {
      promo.set('productsLoaded', false);
    });

    _.each(promos, function (promo) {
      var select = "select m.em_custdis_primary_newprice, m.em_custdis_secondary_newprice, op.m_product_id, p._identifier, p.standardPrice, op.em_custdis_giftqty, op.em_custdis_amount, op.em_custdis_discountamount, op.em_custdis_percentage, op.em_custdis_ir_type, op.em_custdis_is_gift " + //
      "from m_offer_product op join m_product p on op.m_product_id = p.m_product_id " + //
      "join m_offer m on m.m_offer_id = op.m_offer_id " +
      "where op.m_offer_id = '" + promo.id + "'";
      var discType = promo.get('discountType');
      OB.Dal.query(OB.Model.DiscountProductQuery, select, [], function (products) {
    	  var info = '',
          giftItemArr = [];
        	products.models.forEach(function (prd){ // to add discount type to prd
            if(discType === '1424B47A341E4D38B0F61CCF26450AF3'){
            	prd.set('discType','1424B47A341E4D38B0F61CCF26450AF3');
            }
          });
        
        _.each(products.models, function (prd) {
          if (!prd.get('isGift')) {
            var discountAmount = OB.CUSTDIS.Utils.calculateLineDiscount(prd.get('standardPrice'), prd);
            if (discountAmount) {
              info += OB.I18N.getLabel('CUSTDIS_lblPromoSelectorDiscount') + ': ' + OB.I18N.formatCurrency(discountAmount);
            }
          }
        });

        _.each(products.models, function (prd) {
            if (discType === '1424B47A341E4D38B0F61CCF26450AF3') {
                prd.set('isGift', true);
            }
            if (prd.get('isGift')) {
            	var giftQty = (!OB.UTIL.isNullOrUndefined(prd.get('custdisGiftQty'))) ? prd.get('custdisGiftQty') : 1 ;
                if (info !== '') {
                    info += ', ';
                }
                info += prd.get('name');
                if (prd.get('irType')) {
                    info += ' (' + OB.I18N.formatCurrency(prd.get('discountAmount')) + ')';
                }
                for (let i = 0; i < giftQty; i++) {
                    giftItemArr.push(prd.get('name'));
                }
            }
            if (discType === '1424B47A341E4D38B0F61CCF26450AF3') {
                prd.set('isGift', false);
            }
        });
        promo.set('productsLoaded', true);
        promo.set('products', giftItemArr);
        finishLoadProd();
      }, function () {
        promo.set('productsLoaded', true);
        finishLoadProd();
      });
    });
  },
  
  executeOnShow: function () {
   var me = this;
   this.promotion = null;
   
     this.loadPromoProducts(this.args.promotions.models, function () {
     me.$.body.$.listPromotions.$.stPromotionSelector.collection.reset(me.args.promotions.models);
   });

 },
  executeOnHide: function () {
//-------------------------------

   var count=0, me=this;
   enyo.$.scrim.show();
   function successLocal(model) {
	   if(!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount'))){
		   me.promotion.unset('limitcount');
	   }
	   if(!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('totalAvailableQty'))){
		   me.promotion.unset('totalAvailableQty');
	   }
	   if(!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('count'))){
		   me.promotion.unset('count');
	   }
	   if(!OB.UTIL.isNullOrUndefined(model.get('custdisSellableLimit')) && model.get('custdisSellableLimit') >0) {
		   //---to fetch total promo qty added in the order ---> for promo limit check
		   var map = new Map();
		   var ArrayOnlyCustOffer = [];
	       var ArrayPriceAdjDis = [];
	       var lines = OB.MobileApp.model.receipt.get('lines').models;
	       var k = 0;

           for (k = 0; k < lines.length; k++) {
	            if (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions'))) {
	              if (!OB.UTIL.isNullOrUndefined(lines[k].get('custdisOffer')) && (OB.UTIL.isNullOrUndefined(lines[k].get('custdisOrderline')) || (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].discountType) && lines[k].get('promotions')[0].discountType === 'CA5491E6000647BD889B8D7CDF680795')) ){
	                    ArrayOnlyCustOffer.push(lines[k]); //add only primary lines and priceAdjustment discount types
	              }
	            }
	       }

           for (i = 0; i < ArrayOnlyCustOffer.length; i++) {
               if (!OB.UTIL.isNullOrUndefined(lines[i].get('promotions'))) {
                 if (map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) && ( OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') != 'BS' || OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey') != 'BR')) {
                   map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) + ArrayOnlyCustOffer[i].get('qty') );
                 } else {
                       map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), ArrayOnlyCustOffer[i].get('qty'));
                 }
               }
             }

           var keys = map.keys();
           var values = map.values();
		   map.forEach(function (eachMap){
			if(keys.next().value === me.promotion.get('id') ){
				count = count+values.next().value;
			}
		   });

		   me.promotion.set('count',count);
		   //-------
		   new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.PromotionLimitService').exec({
			   mofferid: me.promotion.get('id'),
			   client: OB.MobileApp.model.attributes.terminal.client
		   }, function(data) {
			   if (data && data.exception) {
				   me.promotion.set('limitcount',-1);
				   if(!OB.UTIL.isNullOrUndefined(data.exception.message) && data.exception.message.indexOf('Application server is not') >= 0){
					   me.promotion.set('limitcount','offline');
				   } else if (!OB.UTIL.isNullOrUndefined(data.exception) && !OB.UTIL.isNullOrUndefined(data.exception.status) && !OB.UTIL.isNullOrUndefined(data.exception.status.message)){
					   me.promotion.set('limitcount','error');
					   me.promotion.set('limitErrorMessage',data.exception.status.message);
				   }
				   //--------
				   if((!OB.UTIL.isNullOrUndefined(me.promotion)) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') > 0){
						  me.args.callback(me.promotion);
					  } else {
						  me.args.callback(false);
						  if(!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') === 'offline'){
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_MsgApplicationServerNotAvailable'));
						  } else if (!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') === 'error' && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitErrorMessage'))){ 
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),me.promotion.get('limitErrorMessage'));
						  } else if(!OB.UTIL.isNullOrUndefined(me.promotion)){
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDIS_PromotionLimit', [me.promotion.get('totalAvailableQty')]));
							  }
						  }
				    enyo.$.scrim.hide();
					return;
				   //-------
			   } else {
				   if(!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.remainingqty)){
                       var percentage = 0;
                       if (data.remainingqty > 0) {
                          percentage = Math.round(((me.promotion.get('custdisSellableLimit')-data.remainingqty)/me.promotion.get('custdisSellableLimit'))*100);
                       }
                      if (percentage >= 90 && percentage < 100 ){
                          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_PermittedQtyLimitAlert', [percentage]));
                          me.args.callback(me.promotion);
                      }
                       
					   if (me.promotion.get('count') >= data.remainingqty){
						   me.promotion.set('limitcount',-1);
						   me.promotion.set('totalAvailableQty', data.remainingqty);
					   }else{
						   me.promotion.set('limitcount',data.remainingqty);
						   me.promotion.set('totalAvailableQty', data.remainingqty);
					   }
				   }
				   if((!OB.UTIL.isNullOrUndefined(me.promotion)) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') > 0){
						  me.args.callback(me.promotion);
					  } else {
						  me.args.callback(false);
						  if(!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') === 'offline'){
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_MsgApplicationServerNotAvailable'));
						  } else if (!OB.UTIL.isNullOrUndefined(me.promotion) && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount')) && me.promotion.get('limitcount') === 'error' && !OB.UTIL.isNullOrUndefined(me.promotion.get('limitErrorMessage'))){ 
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),me.promotion.get('limitErrorMessage'));
						  } else if(!OB.UTIL.isNullOrUndefined(me.promotion)){
							  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'),  OB.I18N.getLabel('CUSTDIS_PromotionLimit', [me.promotion.get('totalAvailableQty')]));
							  }
						  }
				    enyo.$.scrim.hide();
					return;
			   }
		   });
	   } else{
		   if((!OB.UTIL.isNullOrUndefined(me.promotion)) && OB.UTIL.isNullOrUndefined(me.promotion.get('limitcount'))){
				  me.args.callback(me.promotion); // To add non PQL promo products avoiding PQL check
				  enyo.$.scrim.hide();
				  return;
			} 
	   }
   	}

   	function errorLocal(tx) {
	   console.log(tx);
	   enyo.$.scrim.hide();
   	}

   	if(!OB.UTIL.isNullOrUndefined(this.promotion) ){
	   OB.Dal.get(OB.Model.Discount, this.promotion.get('id'), successLocal, errorLocal);
   	}else {
	   enyo.$.scrim.hide();
	   this.args.callback(false);
   	}

  	}
	});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTDIS.ModalPromotionSelector',
  name: 'CUSTDIS_ModalPromotionSelector'
});

/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
  name: 'OB.UI.ModalLoyalityCardNumber',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  autoDismiss: false,
  i18nHeader: 'CUSTDIS_LoyaltyProgramCardNumberHeader',
  handlers: {
    
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.CardLoyalityApply'
    }, {
      kind: 'OB.UI.CardLoyalityClose'
    }]
  },
  executeOnHide: function() {
    //OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);
    this.args.args.order.save();
  },
  executeOnShow: function () {
    this.$.bodyContent.$.attributes.$.line_loyalityCardNumber.$.newAttribute.$.loyalityCardNumber.setValue('');
    this.$.bodyContent.$.attributes.$.line_validityMonth.$.newAttribute.$.validityMonth.setValue('');
    this.$.bodyContent.$.attributes.$.line_validityYear.$.newAttribute.$.validityYear.setValue('');
  },
  applyChanges: function (inSender, inEvent) {
    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'OB.UI.ModalLoyalityCardNumberImpl',
  kind: 'OB.UI.ModalLoyalityCardNumber',
  newAttributes: [{
      kind: 'OB.UI.renderTextProperty',
      name: 'loyalityCardNumber',
      i18nLabel: 'CUSTDIS_LoyaltyCardNumberTextBox',
      mandatory: true,
      maxLength: 20,
      input: function(inEvent, inSender) {
       var input = this.getValue();
       var size = input.length;

       if (size > 20) {
          this.setValue(input.substring(0, input.length - 1));
       }
    },
    actionAfterClear: function() {
       this.input(this, this);
    }
  },
  {
      kind: 'OB.UI.renderTextProperty',
      name: 'validityMonth',
      i18nLabel: 'CUSTDIS_LoyaltyCardMonthValidityTextBox',
      mandatory: true,
      maxLength: 2,
      type:'NUMBER',
      input: function(inEvent, inSender) {
       var input = this.getValue();
       var size = input.length;
       if (size > 2 ) {
          this.setValue(input.substring(0, input.length - 1));
       }
       if(input > 12) {
           this.value='';
       }
    },
    actionAfterClear: function() {
       this.input(this, this);
    }
  },
  {
      kind: 'OB.UI.renderTextProperty',
      name: 'validityYear',
      i18nLabel: 'CUSTDIS_LoyaltyCardYearValidityTextBox',
      mandatory: true,
      maxLength: 4,
      type:'NUMBER',
    input: function(inEvent, inSender) {
       var input = this.getValue();
       var size = input.length;

       if (size > 4) {
          this.setValue(input.substring(0, input.length - 1));
       }
    },
    actionAfterClear: function() {
       this.input(this, this);
    }
	  
  }]
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.CardLoyalityClose',
  classes: 'loyalityDialogCancel',
  tap: function() {
    var me = this;
    this.doHideThisPopup();
    me.owner.owner.args.args.order.set('custdisLoyaltyProgramDiscountApplied', false);
    me.owner.owner.args.args.order.set('custdisLoyaltyProgramValidated', false); 
    me.owner.owner.args.args.order.unset('custdisLoyaltyCardNumber');
    me.owner.owner.args.args.order.unset('custdisLoyaltyMonthValidity');
    me.owner.owner.args.args.order.unset('custdisLoyaltyYearValidity');
  },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTDIS_LoyaltyClose'));
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.CardLoyalityApply',
  classes: 'loyalityDialogApply',
  tap: function() {
    var me = this;
     var SETPROPS = function setProperties() {
      me.doHideThisPopup();
      if (!me.owner.owner.args.args.order.get('custdisLoyaltyCardNumber')) {
        me.owner.owner.args.args.order.unset('custdisLoyaltyCardNumber')
        me.owner.owner.args.args.order.unset('custdisLoyaltyMonthValidity');
        me.owner.owner.args.args.order.unset('custdisLoyaltyYearValidity');
        me.owner.owner.args.args.order.unset('custdisLinkedPaymentMethod');
        me.owner.owner.args.args.order.unset('custDisLoyaltyDiscountPromotion');
        me.owner.owner.args.args.order.set('custdisLoyaltyProgramValidated', true);
      }  
    }; 
    var loyaltyCardNumber = this.owner.owner.$.bodyContent.$.attributes.$.line_loyalityCardNumber.$.newAttribute.$.loyalityCardNumber.getValue();
    var validityMonth = this.owner.owner.$.bodyContent.$.attributes.$.line_validityMonth.$.newAttribute.$.validityMonth.getValue();
    var validityYear = this.owner.owner.$.bodyContent.$.attributes.$.line_validityYear.$.newAttribute.$.validityYear.getValue();
    var currentDate = moment(new Date()).format('YYYY-MM-DD');
    var strDate = currentDate.toString();
    var arr = strDate.split('-');
    var year = parseInt(arr[0]);
    var month = parseInt(arr[1]);
    
    if (OB.UTIL.isNullOrUndefined(loyaltyCardNumber) || loyaltyCardNumber === '' || loyaltyCardNumber === '0') {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyCardNumber'));
        me.owner.owner.args.args.order.set('custdisLoyaltyCardNumber', '');
        return;
    } else if (OB.UTIL.isNullOrUndefined(validityMonth) || validityMonth === '' || validityMonth > 12 || validityMonth < 1 ) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyCardMonthValidity'));
        me.owner.owner.args.args.order.set('custdisLoyaltyMonthValidity', '');
        return;
    }else if (OB.UTIL.isNullOrUndefined(validityYear) || validityYear === '' ) { 
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyCardYearValidity'));
        me.owner.owner.args.args.order.set('custdisLoyaltyYearValidity', '');
        return;
    } else if (parseInt(validityYear) <  year || (parseInt(validityYear) === year && parseInt(validityMonth) < month)) {
       OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), 'Please check Card validity');  
       return;
    }else  if(!me.owner.owner.args.args.order.get('custdisLoyaltyProgramValidated')) { 
      new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.LoyaltyDiscountFairLimitService').exec(
       { 
        mofferid: OB.MobileApp.model.receipt.get('custDisLoyaltyDiscountPromotion').id,
        cardNumber:loyaltyCardNumber,
        client: OB.MobileApp.model.get('terminal').client
       }, function(data) {
           if (data && !data.exception) {
             if (data.remainingqty > 0) {
                var ord = OB.MobileApp.model.receipt; 
                var pymentselected = OB.MobileApp.model.receipt.get('custdisLinkedPaymentMethod'); 
                ord.set('custdisLoyaltyCardNumber',loyaltyCardNumber)
                ord.set('custdisLoyaltyMonthValidity',validityMonth);
                ord.set('custdisLoyaltyYearValidity',validityYear);
                ord.set('custdisLoyaltyProgramValidated', true);
                ord.set('custdisLoyaltyDiscountApplied', true);
                var discount = 0.0;
               if (ord.get('custdisLoyaltyDiscountApplied') && pymentselected && pymentselected.paymentMethod.paymentMethodCategory$_identifier === 'Non-cash') {
                  var symbol = OB.MobileApp.model.get('terminal').symbol;
                  var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;      
                //  var grossAmount =  OB.MobileApp.model.receipt.get('amount');  //this.args.amount;
                  var amount = !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('payment')) ?OB.MobileApp.model.receipt.get('payment') :0;
                   var grossAmount =  OB.MobileApp.model.receipt.get('gross')- amount;  //this.args.amount;
                  var custdisSelectedLineAmount = ord.get('custdisSelectedLineAmount');
                  if (ord.get('custdisMinPurchaseAmountBankBin') > grossAmount) {
                     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningTitle'), OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningBodyLoyalty', [OB.I18N.formatCurrencyWithSymbol(this.args.receipt.get('custdisMinPurchaseAmountBankBin'), symbol, symbolAtRight)]));
                     return false;
                  }
                  var discountAmount = new Array();
                  var prodCatDiscount = false;
                  var cumulativeAmount = new Array();
                  var apportionedAmount = new Array();
                  var apportionedCumulative = new Array();
                  var discountCumulative = new Array();
                  
                  var prodCatDiscount = false;    
                  for (var i = 0; i < ord.get('lines').length; i++) {
                     var flag = false;
                          if (!OB.UTIL.isNullOrUndefined(ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines'))) {
                             lineid = ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines').filter((item) => item.line === ord.get('lines').at(i).id);
                          }  
                          if (lineid.length !== 0) {
                           for(var j=0; j < ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines').length; j++) { 
                            if(!OB.UTIL.isNullOrUndefined(ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines')[j].discountpercentage) && 
                             ord.get('lines').at(i).id === ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines')[j].line) {
                             flag = true; 
                            } 
                           }                    
                          }             
                     if(flag) {
                        prodCatDiscount = true; 
                       }
                   }
                   
                  var maxDiscountAmount = ord.get('custdisMaxDiscountAmt');            
                    for (var i = 0; i < ord.get('lines').length; i++) {
                      var applicableDiscountPercentage = 0;
                      if (!OB.UTIL.isNullOrUndefined(ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines'))) {
                         lineid = ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines').filter((item) => item.line === ord.get('lines').at(i).id);
                      }  
                      if (lineid.length !== 0) {
                       for(var j=0; j < ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines').length; j++) { 
                        if(ord.get('lines').at(i).id === ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines')[j].line) {
                            //getting discount percent based prodCatDiscount flag.
                          applicableDiscountPercentage = prodCatDiscount ? ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines')[j].discountpercentage : ord.get('custdisDiscountPercentage'); 
                        } 
                       }
                      }      
                    if(i == 0) {
                        cumulativeAmount[i] = ord.attributes.lines.models[0].get('discountedGross');
                        if (applicableDiscountPercentage > 0) {
                            discountCumulative[i] = ord.attributes.lines.models[i].get('discountedGross');
                        } else {
                            discountCumulative[i] = 0;
                        }
                        if (applicableDiscountPercentage > 0) { 
                        if (grossAmount - discountCumulative[i] > 0) {
                            apportionedAmount[i] = ord.attributes.lines.models[i].get('discountedGross');
                        } else {
                            apportionedAmount[i] = grossAmount;
                        }
                       } else {
                        apportionedAmount[i] = 0;
                      }
                      apportionedCumulative[i] = apportionedAmount[i];
                    } else {
                        cumulativeAmount[i] = cumulativeAmount[i - 1] + ord.attributes.lines.models[i].get('discountedGross');
                        if (applicableDiscountPercentage > 0) {
                            discountCumulative[i] = ord.attributes.lines.models[i].get('discountedGross') + discountCumulative[i - 1];
                        } else {
                            discountCumulative[i] = discountCumulative[i - 1];
                        }
                        if(grossAmount - apportionedCumulative[i-1] > 0) {
                        if (applicableDiscountPercentage > 0) { 
                        if (grossAmount - discountCumulative[i] > 0) {
                            apportionedAmount[i] = ord.attributes.lines.models[i].get('discountedGross');
                        } else {
                            apportionedAmount[i] = grossAmount - discountCumulative[i-1];
                        }
                       } else {
                        apportionedAmount[i] = 0;
                      }
                      } else {
                        apportionedAmount[i] = 0;
                    } 
                    apportionedCumulative[i] = apportionedAmount[i] + apportionedCumulative[i-1];
                    }
            
                    //calculating linediscountamount, then based on country making tofixed ,then checking that 
                    //linediscount amount is >= maximum discount amount then setting in line level then adding total discount.
                    discountAmount[i] = (applicableDiscountPercentage/100) * apportionedAmount[i];
                    if(discountAmount[i] != 0){
                        if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                          discountAmount[i] = +(discountAmount[i].toFixed(3));
                        }else{
                          discountAmount[i] = +(discountAmount[i].toFixed(2));
                        }
                        if(discountAmount[i] >= maxDiscountAmount){
                          ord.attributes.lines.models[i].set('custdisLineloyldisamt',maxDiscountAmount);
                         discountAmount[i] = maxDiscountAmount;
                       }else{
                          ord.attributes.lines.models[i].set('custdisLineloyldisamt',discountAmount[i]);
                          if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                              maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(3));
                      }else{
                              maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(2));
                      }
                       }
                        if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                            discount += +(discountAmount[i].toFixed(3));
                        }else{
                            discount += +(discountAmount[i].toFixed(2));
                        }
                    }
                    if(discount >= ord.get('custdisMaxDiscountAmt')){
                        break;
                    }
               } 
                    //This field value we needed in return receipt hook 
                if(!OB.UTIL.isNullOrUndefined(ord.get('custdisMinPurchaseAmountBankBin'))){
                    ord.set('custdisMindisamt' , ord.get('custdisMinPurchaseAmountBankBin'));
                }
              if (discount > ord.get('custdisMaxDiscountAmt')) {
                 discount = ord.get('custdisMaxDiscountAmt');
                 pymentselected.payment.amount = discount;
              }
              ord.set('amount', ord.get('gross') - discount);
        } 


            // loyalty discount
            
            if (ord.get('custdisLoyaltyDiscountApplied') && pymentselected && pymentselected.paymentMethod.paymentMethodCategory$_identifier === 'Non-cash'){
               pymentselected.payment.amount = discount;
                var me = this;
                var promotionAdded = false;
                OB.MobileApp.model.receipt.addPayment(new OB.Model.PaymentLine({
                       'kind': ord.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'name': ord.get('custdisLinkedPaymentMethod').paymentMethod.name,
                       'amount': discount,
                       'searchKey': ord.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'paymentMethod': ord.get('custdisLinkedPaymentMethod').paymentMethod,
                       'isocode':ord.get('custdisLinkedPaymentMethod').isocode,
                       'allowOpenDrawer': ord.get('custdisLinkedPaymentMethod').paymentMethod.allowopendrawer,
                       'isCash': ord.get('custdisLinkedPaymentMethod').paymentMethod.iscash,
                       'openDrawer': ord.get('custdisLinkedPaymentMethod').paymentMethod.openDrawer,
                       'printtwice': ord.get('custdisLinkedPaymentMethod').paymentMethod.printtwice,
                       'paymentData': {
                            data: {
                                custdisLinkedPaymentMethod: true,
                                custdisAppliedPaymentMethod: ord.get('custdisLinkedPaymentMethod').paymentMethod.name
                            },
                            CardNumber: ord.get('custdisLoyaltyCardNumber'),
                            CardValidity: ord.get('custdisLoyaltyMonthValidity') + '/' + ord.get('custdisLoyaltyYearValidity')
                        },
                     }));
                _.each(ord.get('lines').models, function(line) {
                   _.each(ord.get('custDisLoyaltyDiscountPromotion').get('custdisProductLines'), function(promoProducts) {
                      if ((line.get('id') === promoProducts.line) && !OB.UTIL.isNullOrUndefined(line.get('custdisLineloyldisamt'))) {
                        ord.addPromotion(line, ord.get('custDisLoyaltyDiscountPromotion'), {amt:0});
                         promotionAdded = true;
                         line.set('custdisLoyaltyDiscountOffer', ord.get('custDisLoyaltyDiscountPromotion').get('id')); 
                         line.set('loyaltyCardNumber', ord.get('custdisLoyaltyCardNumber')); 
                          
                      }   
                   });                  
                });     
                if (promotionAdded) {
                  ord.set('custdisLoyaltyDiscountApplied', false); 
                  ord.get('custDisLoyaltyDiscountPromotion').unset("custdisProductLines");
                  ord.unset('custdisLinkedPaymentMethod');
                  ord.unset('custDisLoyaltyDiscountPromotion');
                  ord.unset('custdisLoyaltyDiscountApplied');
                  ord.unset('custdisMaxDiscountAmt');
                  ord.unset('custdisDiscountPercentage');
                  ord.unset('custdisMinPurchaseAmountBankBin');
                  ord.unset('custdisSelectedLineAmount');  
                  ord.set('custdisLoyaltyProgramValidated', false);
                  ord.unset('custdisLoyaltyMonthValidity');
                  ord.unset('custdisLoyaltyYearValidity');
                  ord.save();
                  SETPROPS(); 
                }
            } 

          } else {
             OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyFairUsageLimitExceeded'));
            SETPROPS(); 
          }        
      } else {
         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyFairUsageLimitError'));
        SETPROPS();
      }
  }, function(err) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_LoyaltyFairUsageLimitError'));
     SETPROPS();
  }
  );
   
  
  } 
  },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTDIS_LoyaltyApply'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.ModalLoyalityCardNumberImpl',
  name: 'OB_UI_ModalLoyalityCardNumber'
});
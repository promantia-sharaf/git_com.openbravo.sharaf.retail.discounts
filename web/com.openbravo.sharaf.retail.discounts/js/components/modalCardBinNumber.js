/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
  name: 'OB.UI.ModalCardBinNumber',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  autoDismiss: false,
  i18nHeader: 'CUSTDIS_CreditDebitBinNumberHeader',
  handlers: {
    
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.CardBinApply'
    }, {
      kind: 'OB.UI.BankBinClose'
    }]
  },
  executeOnHide: function() {
    //OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);
    this.args.args.order.save();
  },
  executeOnShow: function () {
    this.$.bodyContent.$.attributes.$.line_bankBinNumberTextBox.$.newAttribute.$.bankBinNumberTextBox.setValue('');
    this.$.bodyContent.$.attributes.$.line_bankCardNumberTextBox.$.newAttribute.$.bankCardNumberTextBox.setValue('');
  },
  applyChanges: function (inSender, inEvent) {
    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'OB.UI.ModalCardBinNumberImpl',
  kind: 'OB.UI.ModalCardBinNumber',
  newAttributes: [{
    kind: 'OB.UI.renderTextProperty',
    name: 'bankBinNumberTextBox',
    i18nLabel: 'CUSTDIS_BankBinNumberTextBox',
    type:'NUMBER',
    maxLength: 6,
    input: function(inEvent, inSender) {
       var input = this.getValue();
       var size = input.length;

       if (size > 6) {
          this.setValue(input.substring(0, input.length - 1));
       }
    },
    actionAfterClear: function() {
       this.input(this, this);
    }
  },
  {
    kind: 'OB.UI.renderTextProperty',
    name: 'bankCardNumberTextBox',
    i18nLabel: 'CUSTDIS_CreditCardNumberTextBox',
    type:'NUMBER',
    maxLength: 4,
    input: function(inEvent, inSender) {
       var input = this.getValue();
       var size = input.length;

       if (size > 4) {
          this.setValue(input.substring(0, input.length - 1));
       }
    },
    actionAfterClear: function() {
       this.input(this, this);
    }
  }]
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.BankBinClose',
  classes: 'obUiReceiptPropertiesDialogCancel',
  tap: function() {
    var me = this;
    this.doHideThisPopup();
    me.owner.owner.args.args.order.set('custdisBankBinDiscountApplied', false);
    me.owner.owner.args.args.order.set('custdisBankBinValidated', false); 
    
    /*me.owner.owner.args.args.order.unset('custdisLinkedPaymentMethod');
    me.owner.owner.args.args.order.unset('custDisBankBinPromotion');
    me.owner.owner.args.args.order.unset('custdisBankBinDiscountApplied');
    me.owner.owner.args.args.order.unset('custdisCardBinNumber');
    me.owner.owner.args.args.order.unset('custdisMaxDiscountAmt');
    me.owner.owner.args.args.order.unset('custdisDiscountPercentage');
    me.owner.owner.args.args.order.unset('custdisMinPurchaseAmountBankBin');
    me.owner.owner.args.args.order.unset('custdisSelectedLineAmount');*/
  },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTDIS_BankBinClose'));
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.CardBinApply',
  classes: 'obUiModalReceiptLinesPropertiesImpl',
  tap: function() {
    var me = this;
    var binNumber = this.owner.owner.$.bodyContent.$.attributes.$.line_bankBinNumberTextBox.$.newAttribute.$.bankBinNumberTextBox.getValue();
    var cardNumber = this.owner.owner.$.bodyContent.$.attributes.$.line_bankCardNumberTextBox.$.newAttribute.$.bankCardNumberTextBox.getValue();
    var SETPROPS = function setProperties() {
      me.doHideThisPopup();
      if (!me.owner.owner.args.args.order.get('custdisCardBinNumber')) {
        me.owner.owner.args.args.order.unset('custdisCardNumber');
        me.owner.owner.args.args.order.unset('custdisLinkedPaymentMethod');
        me.owner.owner.args.args.order.unset('custDisBankBinPromotion');
        me.owner.owner.args.args.order.set('custdisBankBinValidated', true);
      }
    }; 
    if (OB.UTIL.isNullOrUndefined(binNumber) || binNumber.length != 6) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('SHARCC_ErrorCardNumber6'));
        me.owner.owner.args.args.order.set('custdisCardBinNumber', '');
        return;
    } else if (OB.UTIL.isNullOrUndefined(cardNumber) || cardNumber.length != 4) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('SHARCC_ErrorCardNumber4'));
        me.owner.owner.args.args.order.set('custdisCardBinNumber', '');
        return;
    }else {
        var offer = me.owner.owner.args.promotions;
        var query = "select distinct bn.* from BankBinNumberConfig bn left join BankBinConfig bb on bn.bankBinId=bb.id left join OfferBankBinConfig ob on ob.bankBinId=bb.id where ob.promotionDiscountId='" + offer.get('id') + "'";
        var numberLines = 0;
        OB.Dal.queryUsingCache(OB.Model.BankBinNumberConfig, query, [], function (numbers) {
            if (numbers.length === 0) {
                   OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_NotAvailableBankBinPromotion'));
                   me.doHideThisPopup();
            } else {
               var bankBinPresent = false;
               _.each(numbers.models, function(number) {
                   numberLines = numberLines + 1; 
                   var numberFromDB = number.get('bankBinNumbers');
                   var regex = new RegExp('^' + numberFromDB.replace('*', '[0-9]*') + '$');
                   if (regex.test(binNumber)) { 
                     bankBinPresent = true;                       
                     new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.BankBinFairLimitService').exec(
                        { mofferid: offer.get('id'),
                          cardNumber: binNumber + "******" + cardNumber,
                          client: OB.MobileApp.model.get('terminal').client
                        }, function(data) {
                           if (data && !data.exception) {
                              if (data.remainingqty > 0) {
                                 me.owner.owner.args.args.order.set('custdisBankBinDiscountApplied', true);
                                 me.owner.owner.args.args.order.set('custdisCardBinNumber', binNumber);
                                 me.owner.owner.args.args.order.set('custdisCardNumber', cardNumber);
                                 me.owner.owner.args.args.order.set('custdisBankBinValidated', true);
                                 SETPROPS();
                                 
                              } else {
                                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_FairUsageLimitExceeded'));
                                SETPROPS();
                                
                              } 
                           } else {
                              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_FairUsageLimitError'));
                              SETPROPS();
                              
                           }
                        }, function(err) {
                          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_FairUsageLimitError'));
                          SETPROPS();
                        }
                     );
                   } else {
                     if (numberLines === numbers.models.length && !bankBinPresent) {
                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_NotAvailableBankBinPromotion')); 
                        SETPROPS();
                     }
                   }  
               });
            } 
        });
    } 
  },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTDIS_BinApply'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.ModalCardBinNumberImpl',
  name: 'OB_UI_ModalCardBinNumber'
});
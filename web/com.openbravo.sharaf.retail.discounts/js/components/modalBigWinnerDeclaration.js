/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
  name: 'OB.UI.ModalBigWinnerDeclaration',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  i18nHeader: '',
  handlers: {
    
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'winnerdeclaration'
    }, {
        kind: 'WinnerDeclarationInformation',
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.WinnerDeclarationOk'
    }]
  },
  executeOnHide: function() {
    var me = this;
    //OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);
    //this.args.args.order.save();
          _.each(me.args.args.context.get('order').get('lines').models, function(line) {
              if (!OB.UTIL.isNullOrUndefined(line.get('product')) && !OB.UTIL.isNullOrUndefined(line.get('product').get('custDisBigWinnerFOCProduct')) 
              && line.get('product').get('custDisBigWinnerFOCProduct')) {
                  me.args.args.context.get('order').set("custDisBigWinnerPromotionApplied",true);
                  line.set('custDisBigWinnerPromotion', me.args.args.context.get('order').get('custDisBigWinnerPromotion').get('id'));
                  line.set('custDisBigWinnerPromotionSlotID', me.args.args.context.get('order').get("custDisBigWinnerPromotionSlotID"));
                  line.set('custdisAmount', line.get('product').get('standardPrice'));
                  me.args.args.context.get('order').save();
                  OB.UTIL.HookManager.callbackExecutor(me.args.args, me.args.callbacks);
              }
      });   
  },
  executeOnShow: function () {
     this.$.bodyContent.$.winnerdeclaration.setContent('Congratulation..You Are the Big Winner of the Hour, You Won ' +OB.MobileApp.model.receipt.attributes.FocProduct.get('description'));
  },
  applyChanges: function (inSender, inEvent) {
    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
  }
});

enyo.kind({
  name: 'WinnerDeclarationInformation',
  components: [{

  }],

  init: function () {
    this.inherited(arguments);
  }
});


enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.WinnerDeclarationOk',
  tap: function() {
    var me = this;
   var prod = OB.MobileApp.model.receipt.attributes.FocProduct;
        //OB.UTIL.HookManager.callbackExecutor(me.owner.owner.args.args, me.owner.owner.args.callbacks);
        //OB.CUSTDIS.Utils.addPromotionProducts(me.owner.owner.args.args.context.get('order').get('custDisBigWinnerPromotion'), args.context.get('order'), args.orderline);
       me.owner.owner.args.args.context.get('order').addProductToOrder(prod, 1, undefined, {
          originalLine: null,
          custdisAddByPromo: true,
          custdisPromotion: me.owner.owner.args.args.context.get('order').get('custDisBigWinnerPromotion'),
          custdisOrderline: null,
          custdisIrType: null,
          custdisRedemptionAmount: prod.get('standardPrice'),
          custdisAmount: null,
          cUSTDELDeliveryCondition: null,
          cUSTDELDeliveryTime: null,
          relatedLines: null
        }, function () {
        //  createProductGiftLine(products, idx + 1);
        });
        //OB.MobileApp.model.receipt.addProduct(OB.MobileApp.model.receipt.attributes.FocProduct);
        me.owner.owner.args.args.context.get('order').save();
    me.doHideThisPopup();
    },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBPOS_LblOk'));
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.ModalBigWinnerDeclaration',
  name: 'OB_UI_ModalBigWinnerDeclaration'
});
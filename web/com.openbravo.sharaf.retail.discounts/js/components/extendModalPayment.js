/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
OB.UI.ModalPayment.extend({
  executeOnShow: function () {
    if(this.args.receipt.get('custDisBankBinPromotionFlowInProgress') || this.args.receipt.get('custdisBankBinDiscountApplied') || this.args.key !== 'CPSS_SpotiiPaymentmethod' || (this.args.key === 'CPSS_SpotiiPaymentmethod' && this.args.receipt.get('isVerifiedReturn'))) {
    if (this.args.receipt && this.args.paymentMethod && !OB.UTIL.isNullOrUndefined(this.args.paymentMethod.overpaymentLimit) && this.args.paymentMethod.overpaymentLimit >= 0 && (OB.DEC.abs((this.args.receipt.getTotal()-this.args.receipt.get('payment'))+this.args.paymentMethod.overpaymentLimit) < this.args.amount)) {
      var symbol = OB.MobileApp.model.get('terminal').symbol;
      var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;
      var amount = OB.DEC.abs(this.args.receipt.getTotal()-(this.args.amount + this.args.receipt.get('payment')));
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_OverpaymentWarningTitle'), OB.I18N.getLabel('CUSTQC_OverpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(amount, symbol, symbolAtRight)]));
      return false;
    }
    if (this.args.receipt.get('custDisBankBinPromotionFlowInProgress')) {
      return false;
    } else {
    if (!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custdisBankBinValidated')) && !this.args.receipt.get('custdisBankBinValidated')) {
      this.hide();
      var me = this;
      this.args.order = this.args.receipt;
      OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
         popup: 'OB_UI_ModalCardBinNumber',
         args: {
             promotions: me.args.receipt.get('custDisBankBinPromotion'),
             args: me.args,
             order: me.args.order,
             callbacks: me.args.callbacks 
      }});
      return false; 
    }
    var discount = 0.0;
    if (this.args.receipt.get('custdisBankBinDiscountApplied') && this.args.paymentMethod && this.args.paymentMethod.paymentMethodCategory$_identifier === 'Credit Card') {
      var symbol = OB.MobileApp.model.get('terminal').symbol;
      var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;      
      var grossAmount = this.args.amount;
      var custdisSelectedLineAmount = this.args.receipt.get('custdisSelectedLineAmount');

      if (this.args.receipt.get('custdisMinPurchaseAmountBankBin') > grossAmount) {
         OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningTitle'), OB.I18N.getLabel('CUSTDIS_UnderpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(this.args.receipt.get('custdisMinPurchaseAmountBankBin'), symbol, symbolAtRight)]));
         return false;
      }
      var discountAmount = new Array();
      var prodCatDiscount = false;
      var cumulativeAmount = new Array();
      var apportionedAmount = new Array();
      var apportionedCumulative = new Array();
      var discountCumulative = new Array();
      var invoiceProdQty = 0;
      if(!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty')) && this.args.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty') < this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines').length){
    	  invoiceProdQty = this.args.receipt.get('custDisBankBinPromotion').get('custdisInvoiceqty');
      }else{
    	  invoiceProdQty = this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines').length;
      }
      var prodCatDiscount = false;    
      for (var i = 0; i < this.args.receipt.attributes.lines.models.length; i++) {
             var flag = false;
                  if (!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
                     lineid = this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === this.args.receipt.attributes.lines.models[i].get('id'));
                  }  
                  if (lineid.length !== 0) {
                   for(var j=0; j < invoiceProdQty; j++) { 
                    if(!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage) && 
                      this.args.receipt.attributes.lines.models[i].get('id') === this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                     flag = true; 
                    } 
                   }                    
                  }             
             if(flag) {
                prodCatDiscount = true; 
               }
       }
      var maxDiscountAmount = this.args.receipt.get('custdisMaxDiscountAmt');            
        for (var i = 0; i < this.args.receipt.attributes.lines.models.length; i++) {
           var applicableDiscountPercentage = 0;
                  if (!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines'))) {
                     lineid = this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines').filter((item) => item.line === this.args.receipt.attributes.lines.models[i].get('id'));
                  }  
                  if (lineid.length !== 0) {
                   for(var j=0; j < invoiceProdQty; j++) { 
                    if(this.args.receipt.attributes.lines.models[i].get('id') === this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].line) {
                    	//getting discount percent based prodCatDiscount flag.
                      applicableDiscountPercentage = prodCatDiscount ? this.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines')[j].discountpercentage : this.args.receipt.get('custdisDiscountPercentage'); 
                    } 
                   }
                  }      
                if(i == 0) {
                    cumulativeAmount[i] = this.args.receipt.attributes.lines.models[0].get('discountedGross');
                    if (applicableDiscountPercentage > 0) {
                        discountCumulative[i] = this.args.receipt.attributes.lines.models[i].get('discountedGross');
                    } else {
                        discountCumulative[i] = 0;
                    }
                    if (applicableDiscountPercentage > 0) { 
                    if (grossAmount - discountCumulative[i] > 0) {
                        apportionedAmount[i] = this.args.receipt.attributes.lines.models[i].get('discountedGross');
                    } else {
                        apportionedAmount[i] = grossAmount;
                    }
                   } else {
                    apportionedAmount[i] = 0;
                  }
                  apportionedCumulative[i] = apportionedAmount[i];
                } else {
                    cumulativeAmount[i] = cumulativeAmount[i - 1] + this.args.receipt.attributes.lines.models[i].get('discountedGross');
                    if (applicableDiscountPercentage > 0) {
                        discountCumulative[i] = this.args.receipt.attributes.lines.models[i].get('discountedGross') + discountCumulative[i - 1];
                    } else {
                        discountCumulative[i] = discountCumulative[i - 1];
                    }
                    if(grossAmount - apportionedCumulative[i-1] > 0) {
                    if (applicableDiscountPercentage > 0) { 
                    if (grossAmount - discountCumulative[i] > 0) {
                        apportionedAmount[i] = this.args.receipt.attributes.lines.models[i].get('discountedGross');
                    } else {
                        apportionedAmount[i] = grossAmount - discountCumulative[i-1];
                    }
                   } else {
                    apportionedAmount[i] = 0;
                  }
                  } else {
                    apportionedAmount[i] = 0;
                } 
                apportionedCumulative[i] = apportionedAmount[i] + apportionedCumulative[i-1];
                }

                //calculating linediscountamount, then based on country making tofixed ,then checking that 
                //linediscount amount is >= maximum discount amount then setting in line level then adding total discount.
                discountAmount[i] = (applicableDiscountPercentage/100) * apportionedAmount[i];
                if(discountAmount[i] != 0){
                if(OB.MobileApp.model.get('currency').standardPrecision > 2){
              	  discountAmount[i] = +(discountAmount[i].toFixed(3));
                }else{
              	  discountAmount[i] = +(discountAmount[i].toFixed(2));
                }
                if(discountAmount[i] >= maxDiscountAmount){
              	  this.args.receipt.attributes.lines.models[i].set('custdisLinedisamt',maxDiscountAmount);
              	 discountAmount[i] = maxDiscountAmount;
               }else{
             	  this.args.receipt.attributes.lines.models[i].set('custdisLinedisamt',discountAmount[i]);
             	  if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(3));
          	  }else{
                	  maxDiscountAmount = +((maxDiscountAmount - discountAmount[i]).toFixed(2));
          	  }
               }
                if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                    discount += +(discountAmount[i].toFixed(3));
                }else{
                    discount += +(discountAmount[i].toFixed(2));
                }
        }
                if(discount >= this.args.receipt.get('custdisMaxDiscountAmt')){
                	break;
                }
        } 
        //This field value we needed in return receipt hook 
        if(!OB.UTIL.isNullOrUndefined(this.args.receipt.get('custdisMinPurchaseAmountBankBin'))){
            this.args.receipt.set('custdisMindisamt' , this.args.receipt.get('custdisMinPurchaseAmountBankBin'));
        }
      if (discount > this.args.receipt.get('custdisMaxDiscountAmt')) {
         discount = this.args.receipt.get('custdisMaxDiscountAmt');
      }
	  this.args.amount = this.args.amount - discount;
    }
      
    this.$.header.setContent(this.args.receipt && this.args.receipt.getTotal() > 0 ? OB.I18N.getLabel('OBPOS_LblModalPayment', [OB.I18N.formatCurrency(this.args.amount)]) : OB.I18N.getLabel('OBPOS_LblModalReturn', [OB.I18N.formatCurrency(this.args.amount)]));
    this.$.bodyContent.destroyComponents();
    //default values to reset changes done by a payment method
    this.closeOnEscKey = this.dfCloseOnEscKey;
    this.autoDismiss = this.dfAutoDismiss;
    
    if (this.args.receipt.get('custdisBankBinDiscountApplied') && this.args.paymentMethod && this.args.paymentMethod.paymentMethodCategory$_identifier === 'Credit Card') {
      this.executeOnShown = function() {        
        this.$.bodyContent.children[0].$.cardNumber6.setValue(this.args.receipt.get('custdisCardBinNumber'));
        this.$.bodyContent.children[0].$.cardNumber6.setDisabled(true);
        this.$.bodyContent.children[0].$.cardNumber4.setValue(this.args.receipt.get('custdisCardNumber'));
        this.$.bodyContent.children[0].$.cardNumber4.setDisabled(true);
        this.$.bodyContent.children[0].$.error.setContent(OB.I18N.getLabel('CUSTDIS_AppliedBankBinDiscountAmount') + OB.I18N.formatCurrency(discount));
      };
    } else {
      this.executeOnShown = null;
    }
    if (this.args.receipt.get('custdisBankBinDiscountApplied') && this.args.paymentMethod && this.args.paymentMethod.paymentMethodCategory$_identifier === 'Credit Card') {
      this.executeOnHide = function() {
        var me = this;
        var promotionAdded = false;
        _.each(this.args.receipt.getPaymentStatus().payments.models, function(payment) {
           if (payment.get('kind') === me.args.key) {
                 if (Math.floor(me.args.amount) === Math.floor(payment.get('amount'))) {
                    payment.get('paymentData').data.custdisMainPaymentMethod = true;
                 }
                 me.args.receipt.addPayment(new OB.Model.PaymentLine({
                       'kind': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.searchKey,
                       'name': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.name,
                       'amount': discount,
                       'isocode': me.args.isocode,
                       'allowOpenDrawer': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.allowopendrawer,
                       'isCash': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.iscash,
                       'openDrawer': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.openDrawer,
                       'printtwice': me.args.receipt.get('custdisLinkedPaymentMethod').paymentMethod.printtwice,
                       'paymentData': {
                            data: {
                                custdisLinkedPaymentMethod: true,
                                custdisAppliedPaymentMethod: me.args.name
                            },
                            CardNumber: ''
                        },
                 }));
            _.each(me.args.receipt.get('lines').models, function(line) {
               _.each(me.args.receipt.get('custDisBankBinPromotion').get('custdisProductLines'), function(promoProducts) {
                  if ((line.get('id') === promoProducts.line) && !OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))) {
                     me.args.receipt.addPromotion(line, me.args.receipt.get('custDisBankBinPromotion'), {amt:0});
                     promotionAdded = true;
                     line.set('custdisBankBinOffer', me.args.receipt.get('custDisBankBinPromotion').get('id')); 
                  }   
               });                  
            });     
           }
        });
        if (promotionAdded) {
          me.args.receipt.set('custdisBankBinDiscountApplied', false); 
          me.args.receipt.get('custDisBankBinPromotion').unset("custdisProductLines");
          me.args.receipt.unset('custdisLinkedPaymentMethod');
          me.args.receipt.unset('custDisBankBinPromotion');
          me.args.receipt.unset('custdisBankBinDiscountApplied');
          me.args.receipt.unset('custdisCardBinNumber');
          me.args.receipt.unset('custdisCardNumber');
          me.args.receipt.unset('custdisMaxDiscountAmt');
          me.args.receipt.unset('custdisDiscountPercentage');
          me.args.receipt.unset('custdisMinPurchaseAmountBankBin');
          me.args.receipt.unset('custdisSelectedLineAmount');
          me.args.receipt.save();
        }
      };   
    } else {
      this.executeOnHide = null;
    }
    this.executeBeforeHide = null;
    this.$.bodyContent.createComponent({
        mainPopup: this,
        kind: this.args.provider,
        paymentMethod: this.args.paymentMethod,
        paymentType: this.args.name,
        paymentAmount: this.args.amount,
        isocode: this.args.isocode,
        key: this.args.key,
        receipt: this.args.receipt,
        cashManagement: this.args.cashManagement,
        allowOpenDrawer: this.args.paymentMethod.allowopendrawer,
        isCash: this.args.paymentMethod.iscash,
        openDrawer: this.args.paymentMethod.openDrawer,
        printtwice: this.args.paymentMethod.printtwice,
        isReversePayment: this.args.isReversePayment,
        reversedPaymentId: this.args.reversedPaymentId,
        reversedPayment: this.args.reversedPayment,
    }).render();
    }
   } else {     
     var spotiiPayment = false;   
     this.args.receipt.get('payments').models.forEach(function(payment) {
            if (payment.get('kind') === 'CPSS_SpotiiPaymentmethod') {
                spotiiPayment = true;
            }
        });
    
    if(this.args.key && this.args.key === 'CPSS_SpotiiPaymentmethod' && !spotiiPayment) {
    if (this.args.receipt && this.args.paymentMethod && !OB.UTIL.isNullOrUndefined(this.args.paymentMethod.overpaymentLimit) && this.args.paymentMethod.overpaymentLimit >= 0 && (OB.DEC.abs((this.args.receipt.getTotal()-this.args.receipt.get('payment'))+this.args.paymentMethod.overpaymentLimit) < this.args.amount)) {
      var symbol = OB.MobileApp.model.get('terminal').symbol;
      var symbolAtRight = OB.MobileApp.model.get('terminal').currencySymbolAtTheRight;
      var amount = OB.DEC.abs(this.args.receipt.getTotal()-(this.args.amount + this.args.receipt.get('payment')));
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_OverpaymentWarningTitle'), OB.I18N.getLabel('CUSTQC_OverpaymentWarningBody', [OB.I18N.formatCurrencyWithSymbol(amount, symbol, symbolAtRight)]));
      return false;
    }    
        if ((localStorage.getItem('SpotiiPaymentMethodData') !== null) && (typeof localStorage.getItem('receipt') !== 'SpotiiPaymentMethodData')) {
            var me = this;
            this.args.order = this.args.receipt;
            var prod = JSON.parse(localStorage.getItem('SpotiiPaymentMethodData'))['linkedServiceArticleId'];
            if (OB.UTIL.isNullOrUndefined(prod)) {
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CPSS_ConfigMissing'));
            } else {
                me = this;
                var product = OB.MobileApp.model.receipt.attributes.linkedServiceArticleProduct;
                var serviceArticlePrice = 0.0;
                if (me.args.order.get('gross') !== me.args.amount) {
                    serviceArticlePrice = OB.DEC.mul( me.args.amount, (JSON.parse(localStorage.getItem('SpotiiPaymentMethodData'))['additonalPercentage'] / 100));
                } else {
                    serviceArticlePrice = OB.DEC.mul(me.args.order.get('gross'), (JSON.parse(localStorage.getItem('SpotiiPaymentMethodData'))['additonalPercentage'] / 100));
                }
                serviceArticlePrice = Number(serviceArticlePrice).toFixed(OB.MobileApp.model.get('currency').standardPrecision);
                if (!OB.UTIL.isNullOrUndefined(product) && me.args.order.get('linkedServiceArticle') !== true) {
                    me.args.order.set('linkedServiceArticle', false);
                    product.set('originalStandardPrice', serviceArticlePrice);
                    product.set('listPrice', serviceArticlePrice);
                    product.set('standardPrice', serviceArticlePrice);
                    me.args.order.addProduct(product);
                    me.args.order.set('linkedServiceArticle', true);
                    if (!OB.UTIL.isNullOrUndefined(me.args.order.get('linkedServiceArticleTaxRate'))) {
                        var serviceArticleTax = OB.DEC.mul( serviceArticlePrice, me.args.order.get('linkedServiceArticleTaxRate') / 100);
                        serviceArticleTax = Number(serviceArticleTax.toFixed(OB.MobileApp.model.get('currency').standardPrecision));
                        var totalServiceArticleAmt = OB.DEC.add(Number(serviceArticlePrice),serviceArticleTax);
                        me.args.amount = OB.DEC.add(me.args.amount ,totalServiceArticleAmt);
                    } else if (me.args.order.get('linkedServiceArticle') == true) {
                         me.args.amount = OB.DEC.add(me.args.amount ,serviceArticlePrice);
                    }
                }
            }
        }
      this.$.header.setContent(this.args.receipt && this.args.receipt.getTotal() >= 0 ? OB.I18N.getLabel('OBPOS_LblModalPayment', [OB.I18N.formatCurrency(this.args.amount)]) : OB.I18N.getLabel('OBPOS_LblModalReturn', [OB.I18N.formatCurrency(this.args.amount)]));

      this.$.bodyContent.destroyComponents();
      //default values to reset changes done by a payment method
      this.closeOnEscKey = this.dfCloseOnEscKey;
      this.autoDismiss = this.dfAutoDismiss;
      this.executeOnShown = null;
      this.executeBeforeHide = null;

        this.executeOnHide = function() {
          var me = this;
          this.args.order = this.args.receipt;
          var spotiiPaymentAdded = false;
            me.args.order.get('payments').models.forEach(function(payment) {
                if (payment.get('kind') === 'CPSS_SpotiiPaymentmethod') {
                    spotiiPaymentAdded = true;
                }
            });
            _.each(me.args.order.get('lines').models, function(line) {
                if (!OB.UTIL.isNullOrUndefined(line.get('product').get('linkedServiceArticle')) && line.get('product').get('linkedServiceArticle') == true && !spotiiPaymentAdded) {
                    me.args.order.deleteLinesFromOrder([line]);
                    me.args.order.unset('linkedServiceArticle');
                }
            });
        };
      this.$.bodyContent.createComponent({
        mainPopup: this,
        kind: this.args.provider,
        paymentMethod: this.args.paymentMethod,
        paymentType: this.args.name,
        paymentAmount: this.args.amount,
        isocode: this.args.isocode,
        key: this.args.key,
        receipt: this.args.receipt,
        cashManagement: this.args.cashManagement,
        allowOpenDrawer: this.args.paymentMethod.allowopendrawer,
        isCash: this.args.paymentMethod.iscash,
        openDrawer: this.args.paymentMethod.openDrawer,
        printtwice: this.args.paymentMethod.printtwice,
        isReversePayment: this.args.isReversePayment,
        reversedPaymentId: this.args.reversedPaymentId,
        reversedPayment: this.args.reversedPayment
      }).render();
    } else if(this.args.key && this.args.key === 'CPSS_SpotiiPaymentmethod' && spotiiPayment){
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Spotii Payment already added in the receipt');
     return false;
    }     
   }
  }
});

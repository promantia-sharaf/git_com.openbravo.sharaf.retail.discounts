/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global Backbone _*/

// Price Adjustment
OB.Model.Discounts.discountRules.CA5491E6000647BD889B8D7CDF680795 = {
  isManual: true,
  isFixed: true,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

// FOC and Instant Redemption
OB.Model.Discounts.discountRules['6A3C2313136147A6B2D1B8D2F5F768E6'] = {
  isManual: true,
  isFixed: true,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

//FOC and Instant Redemption
OB.Model.Discounts.discountRules['61D65EECCF97487A82D169BB7E96832C'] = {
  isManual: true,
  isFixed: true,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};


//Buy1 get1
OB.Model.Discounts.discountRules['1424B47A341E4D38B0F61CCF26450AF3'] = {
  isManual: true,
  isFixed: false,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

//Bank Bin Promotion
OB.Model.Discounts.discountRules['B277B63BDA6E4B67B3D8F491FC3C4E3B'] = {
  isManual: true,
  isFixed: false,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

//Big Winner Promotion
OB.Model.Discounts.discountRules['399EF97C508B40E787A444CE3855A7E4'] = {
  isManual: true,
  isFixed: true,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
 };
 
 //Sharaf BUY X GET Y with Discount %
 
 OB.Model.Discounts.discountRules['9AC09641F86C46C4AF7BC8630325B26C'] = {
  isManual: true,
  isFixed: true,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

//Loyalty Discount Program Promotion
OB.Model.Discounts.discountRules['25ECBE384EAB4C1EB69948FF99FC8456'] = {
  isManual: true,
  isFixed: false,
  isAmount: true,
  addManual: function (receipt, line, promotion) {
    OB.CUSTDIS.Utils.addSharafPromotion(receipt, line, promotion);
  }
};

/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    enyo.$.scrim.show();
    if (args.cancelOperation) {
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
    var error = false;
    var enabledLoyaltyPayMethod = [];
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
           enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
        }
        
      var  payments = _.find(args.order.get('payments').models, function (payment) {
       return enabledLoyaltyPayMethod[payment.get('kind')]  ;
      });
      var loyaltyDiscountRelatedLine = false;
      _.each(args.selectedLines, function(selLine) {  
          if (selLine.has('custdisLoyaltyDiscountOffer')) {
              loyaltyDiscountRelatedLine = true;
          }
      });
    if (payments) {
      args.cancelOperation = true;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), "Loyalty Payment is added into the receipt.Before deleting an article please remove the Loyalty payment");
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
 });
  
}());
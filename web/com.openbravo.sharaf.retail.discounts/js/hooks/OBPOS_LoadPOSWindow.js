/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_LoadPOSWindow', function (args, callbacks) {
    
    var success, error;
    success  = function() {};
    error = function() {
       console.log('error while saving Discount Product Category in local storage ');
    };
    var query = 'select distinct OF.* from m_offer_prod_cat OF';
    var qureyProdCat = 'select distinct PD.* from m_product_category_tree PD where PD.parent_id="';
    var checkQuery = 'select * from m_offer_prod_cat where m_offer_id="';
    OB.Dal.queryUsingCache(OB.Model.DiscountFilterProductCategory, query, [], function (prodCats) {
     _.each(prodCats.models, function(cat) {
       OB.Dal.queryUsingCache(OB.Model.ProductCategoryTree, qureyProdCat + cat.get('productCategory') + '"', [],
                function (prodCatGroups) {
          _.each(prodCatGroups.models, function(prodCatGroup) {
             OB.Dal.queryUsingCache(OB.Model.ProductCategoryTree, qureyProdCat + prodCatGroup.get('parentId') + '"', [], function (prodCatSubGroups) {
               _.each(prodCatSubGroups.models, function(prodCatSubGroup) {
                  var newProdCategorySub = new OB.Model.DiscountFilterProductCategory;
                  newProdCategorySub.set('id', OB.UTIL.get_UUID());
                  newProdCategorySub.set('productCategory', prodCatSubGroup.get('categoryId'));
                  newProdCategorySub.set('custdisDiscountPercentage', cat.get('custdisDiscountPercentage'));
                  newProdCategorySub.set('priceAdjustment', cat.get('priceAdjustment'));
                  newProdCategorySub.set('_identifier', cat.get('_identifier'));
                  
                  
                  OB.Dal.queryUsingCache(OB.Model.DiscountFilterProductCategory, checkQuery + cat.get('priceAdjustment') + '" and m_product_category_id="' + prodCatSubGroup.get('categoryId') + '"', [], 
                         function (data) {
                            if (data.length == 0) {
                               OB.Dal.transaction(function(tx) {
                                      OB.Dal.getInTransaction(
                                            tx,
                                            OB.Model[newProdCategorySub.modelName],
                                            newProdCategorySub.get('id'),
                                            function() {
                                              OB.Dal.save(newProdCategorySub, success, error, false, tx);
                                            },
                                            error,
                                            function() {
                                              OB.Dal.saveInTransaction(tx, newProdCategorySub, success, error, true);
                                            },
                                            true
                                      );
                               });    
                            }
                  });
               });
             });
             var newProdCategory = new OB.Model.DiscountFilterProductCategory;
             newProdCategory.set('id', OB.UTIL.get_UUID());
                  newProdCategory.set('productCategory', prodCatGroup.get('categoryId'));
                  newProdCategory.set('custdisDiscountPercentage', cat.get('custdisDiscountPercentage'));
                  newProdCategory.set('priceAdjustment', cat.get('priceAdjustment'));
                  newProdCategory.set('_identifier', cat.get('_identifier'));
                  
                  OB.Dal.queryUsingCache(OB.Model.DiscountFilterProductCategory, checkQuery + cat.get('priceAdjustment') + '" and m_product_category_id="' + prodCatGroup.get('categoryId') + '"', [], 
                         function (data) {
                       if (data.length == 0) {   
                          OB.Dal.transaction(function(tx) {
                              OB.Dal.getInTransaction(
                                    tx,
                                    OB.Model[newProdCategory.modelName],
                                    newProdCategory.get('id'),
                                    function() {
                                      OB.Dal.save(newProdCategory, success, error, false, tx);
                                    },
                                    error,
                                    function() {
                                      OB.Dal.saveInTransaction(tx, newProdCategory, success, error, true);
                                    },
                                    true
                                  );
                          });
                       }
                  });
          });
       });       
       
     });    
    });    
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);    
  });
}());
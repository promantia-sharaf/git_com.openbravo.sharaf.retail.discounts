/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */
OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function(args, callbacks) {
    args.allReturnLinesSelected = false;
    var isLoyaltyDiscountPromotionApplied = false,
         linkedPayment,
         likedPaymentAvailable = false,
         linkedPaymentMethodName,
         nonLoyaltyDiscountAutoSettle = false,
         hasLoyaltyLineDiscountField = false,
         minPurchaseAmount,
         loyaltyPaymentMethod = false,
         loyaltyPaymentMethodApplied = false,
         enabledLoyaltyPayMethod;
    
    if(!OB.UTIL.isNullOrUndefined(args.order.custdisMindisamt)){
         minPurchaseAmount = args.order.custdisMindisamt;
    }
    if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) 
      && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
      enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
    }
    _.each(args.order.receiptPayments, function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.data) &&
            !OB.UTIL.isNullOrUndefined(payment.paymentData.data.custdisLinkedPaymentMethod) && payment.paymentData.data.custdisLinkedPaymentMethod) {
            if (!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod) 
                && !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[payment.paymentData.data.custdisAppliedPaymentMethod]) 
                && enabledLoyaltyPayMethod[payment.paymentData.data.custdisAppliedPaymentMethod] === true) {
                loyaltyPaymentMethodApplied = true; 
                isLoyaltyDiscountPromotionApplied = true;
                linkedPayment = payment;
                linkedPaymentMethodName = payment.name;
                return;
            }
            
        }
    });
    var autoSettlePayments = [];
    if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData')) ) {
      var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;
    _.each(args.order.receiptPayments, function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.kind) && paymentData[payment.kind]) {
            if(!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[payment.kind])) {
                autoSettlePayments.push(payment.name);
            }
            if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && OB.UTIL.isNullOrUndefined(payment.paymentData.data) && (args.selectedLines.length !== args.order.receiptLines.length)) {
                nonLoyaltyDiscountAutoSettle = true;
            } 
        }      
    });
   }    
    if (isLoyaltyDiscountPromotionApplied) {
        if(nonLoyaltyDiscountAutoSettle) {
            let uniquePayments = [...new Set(autoSettlePayments)];
            args.cancellation = true;
            args.cancelOperation = true;
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
        }
        if (!OB.UTIL.isNullOrUndefined(linkedPayment)) {
            var count = 0;
            var totalPaymentMethods = 0;
            _.each(OB.MobileApp.model.paymentnames, function(payment) {
                totalPaymentMethods = totalPaymentMethods + 1;
            });
            var callback = function() {
                if (!likedPaymentAvailable) {
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_OriginalLinkedPaymentNotConfigured', [linkedPaymentMethodName]));
                    args.cancellation = true;
                    args.cancelOperation = true;
                } else if (args.order.receiptLines.length !== args.selectedLines.length) {
                    var loyaltyDiscountOfferLength = 0;
                    var count = 0;
                    for (var i = 0; i < args.order.receiptLines.length; i++) {
                        if (!OB.UTIL.isNullOrUndefined(args.order.receiptLines[i].custdisLoyaltyDiscountOffer)) {
                            count = count + 1;
                        }
                    }
                    for (var i = 0; i < args.selectedLines.length; i++) {
                        if (!OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisLoyaltyDiscountOffer)) {
                            loyaltyDiscountOfferLength = loyaltyDiscountOfferLength + 1;
                        }
                        if (!OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisLineloyldisamt) && !OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisLoyaltyDiscountOffer)) {
                        	hasLoyaltyLineDiscountField = true;
                        }
                    }
                    if(!hasLoyaltyLineDiscountField){
                    	var totalRemainingLineAmount = 0.0;
                    	if(totalRemainingLineAmount === 0.0){
                			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                            if (count !== loyaltyDiscountOfferLength && loyaltyDiscountOfferLength !== 0) {
                                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + "Loyalty Discount Offer present in the Original Invoice -No partial Return Allowed");
                                args.cancellation = true;
                            }
                    	}

                    }else{
                     var linenetamount = new Array(),
                    	 taxAmount = new Array(),
                    	 lineid = new Array(),
                    	 totalRemainingLineAmount = 0.0,
                    	 totalLineAmount = 0.0,
                    	 totalTaxAmount = 0.0;
                    	for (var i = 0; i < args.order.receiptLines.length; i++) {
                    	lineid = args.selectedLines.filter((item) => item.line.lineId === args.order.receiptLines[i].lineId);
                    	if(hasLoyaltyLineDiscountField){
                    			if(!OB.UTIL.isNullOrUndefined(args.order.receiptLines[i].custdisLoyaltyDiscountOffer) && lineid.length == 0 && args.order.receiptLines[i].remainingQuantity !== 0){
                        			 linenetamount[i] = args.order.receiptLines[i].linenetamount;
                        			 totalLineAmount += linenetamount[i];
                        			 for(var k=0; k < args.order.receiptLines[i].taxes.length; k++){
                        				 taxAmount[k] = args.order.receiptLines[i].taxes[k].taxAmount;
                            			 totalTaxAmount += taxAmount[k]; 
                        			 }
                        			
                        			 totalRemainingLineAmount = totalLineAmount + totalTaxAmount;
                            	}
                    		
                    	}
                    	}
                		if(totalRemainingLineAmount === 0.0 || totalRemainingLineAmount >= minPurchaseAmount){
                			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                		}else{
                            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_loyaltyDiscountreturn'));
                            args.cancellation = true;
                		}
                    }
                } else {
                	var totalRemainingLineAmount = 0.0;
                	if(totalRemainingLineAmount === 0.0){
            			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                      }
                    args.allReturnLinesSelected = true;
                    args.cancellation = false;
                    args.cancelOperation = false;
                }
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
            _.each(OB.MobileApp.model.paymentnames, function(payment) {
                count = count + 1;
                if (payment.paymentMethod.searchKey === linkedPayment.kind) {
                    likedPaymentAvailable = true;
                }
                if (count === totalPaymentMethods) {
                    callback();
                }
            });
        } else {
            let uniquePayments = [...new Set(autoSettlePayments)];
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
            args.cancellation = true;
            args.cancelOperation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
});                                                                                                                            
(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_AddProductToOrder', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.options) && !OB.UTIL.isNullOrUndefined(args.options.isVerifiedReturn) && args.options.isVerifiedReturn === true) {
      args.productToAdd.set('groupProduct', true);
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
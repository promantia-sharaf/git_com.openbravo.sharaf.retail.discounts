/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function() {

    OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function(args, callbacks) {
        if (!args.receipt.get('isVerifiedReturn')) {
            var loyaltyIssued = false;
            args.receipt.get('lines').models.forEach(function(line) {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisLoyaltyDiscountOffer'))) {
                  loyaltyIssued = true;
                  return;  
                }
            });
            if (loyaltyIssued) {
                args.cancelOperation = true;
                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), " New products not allowed since Loyalty Payment is added for this receipt");
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
        } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    });
}());
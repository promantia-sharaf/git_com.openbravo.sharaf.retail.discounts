/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global moment */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function (args, callbacks) {

    // Verified Returns && Add Quotations
    if (args.options && (args.options.isVerifiedReturn || OB.UTIL.isNullOrUndefined(args.options.blockAddProduct))) {
      if ((!OB.UTIL.isNullOrUndefined(args.options.line) && !OB.UTIL.isNullOrUndefined(args.options.line.get('isReturnsNegative')) && args.options.line.get('isReturnsNegative'))) {
        //do nothing...
        //If isReturnsNegative flag is true: Not verified returns flow should be execute.
      } else {
        var line = args.orderline;
        args.receipt.attributes.isVerifiedReturn = args.options.isVerifiedReturn;
        if (line.get('custdisOffer') && line.get('custdisOrderline')) {
          var promoLine = _.find(args.receipt.get('lines').models, function (l) {
            return l.get('originalOrderLineId') === line.get('custdisOrderline') || l.get('shaquoOriginalOrderLineId') === line.get('custdisOrderline');
          });
          if (promoLine) {
            line.set('custdisOrderline', promoLine.get('id'), {
              silent: true
            });
          }
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
        return;
      }
    }

    if (OB.UTIL.isNullOrUndefined(args.orderline.get('isStockValidationNeeded')) && !OB.UTIL.isNullOrUndefined(args.orderline.get('isStockAvailable')) && !args.orderline.get('isStockAvailable')) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    } else if (!OB.UTIL.isNullOrUndefined(args.orderline.get('isStockValidationNeeded')) && args.orderline.get('isStockValidationNeeded')) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    // Returns 
    if (args.receipt.get('orderType') === 1) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    var isPrimaryProductDigital = false;
    if ((args.orderline.get('product').get('digiProductType') === 'P' || args.orderline.get('product').get('digiProductType') === 'A' || args.orderline.get('product').get('digiProductType') === 'E'|| args.orderline.get('product').get('digiProductType') === 'SP' || args.orderline.get('product').get('digiProductType') === 'SA') && args.orderline.get('product').get('invoice_type') === 'Re Invoice') {
      isPrimaryProductDigital = true;
    }

    // Not Verified Returns
    if (args.orderline && !args.orderline.get('custdisAddByPromo')) {
      var query = "select distinct O.* from m_offer_product OF join m_offer O on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id" //
      + " where O.m_offer_type_id in ('CA5491E6000647BD889B8D7CDF680795', '6A3C2313136147A6B2D1B8D2F5F768E6','1424B47A341E4D38B0F61CCF26450AF3') " //
      + "and OF.em_custdis_is_gift = 'false' and OF.m_product_id = '" + args.productToAdd.id //
      + "' and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.receipt.get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
      + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') 
      + "' and (O.dateto is null or O.dateto >= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "') " 
     // + OB.CUSTDIS.Utils.addAvailabilityRule(moment())
      + " order by O.name";
      OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {
        if (offers.length === 0) {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else if (args.orderline.get('qty') === 1 && OB.UTIL.isNullOrUndefined(args.orderline.get('custdisOffer')) && !isPrimaryProductDigital) {
          OB.info('Custom discount popup is launched');
          OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
            popup: 'CUSTDIS_ModalPromotionSelector',
            args: {
              promotions: offers,
              callback: function (promotion) {
                if (promotion) {
                  OB.info('Applying selected custom discount');
                  OB.CUSTDIS.Utils.findPromotionProduct(args.orderline, {
                    rule: promotion
                  }, function (product) {
                    if (product) {
                      OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                      product.set('custdisPrimaryNewPrice', promotion.get('custdisPrimaryNewprice'));
                      product.set('custdisSecondaryNewPrice', promotion.get('custdisSecondaryNewprice'));
                      OB.CUSTDIS.Utils.setLineDiscount(args.orderline, product);
                      if (promotion.get('discountType') === 'CA5491E6000647BD889B8D7CDF680795') {
                        OB.Model.Discounts.addManualPromotion(args.receipt, [args.orderline], {
                          rule: promotion,
                          definition: {}
                        });
                      }
                      if (promotion.get('discountType') === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                        OB.info('The previous product will be added to the ticket shortly');
                        OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.receipt, args.orderline);
                      }
                    }
                    OB.info('Ending of selected custom discount');
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  });
                } else {
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              }
            }
          });
        } else if (!OB.UTIL.isNullOrUndefined(args.attrs.unitsToAdd) && args.attrs.unitsToAdd === 1) {
          OB.info('Custom discount popup is launched');
          OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
            popup: 'CUSTDIS_ModalPromotionSelector',
            args: {
              promotions: offers,
              callback: function (promotion) {
                if (promotion) {
                  OB.info('Applying selected custom discount');
                  OB.CUSTDIS.Utils.findPromotionProduct(args.orderline, {
                    rule: promotion
                  }, function (product) {
                    if (product) {
                      OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                      OB.CUSTDIS.Utils.setLineDiscount(args.orderline, product);
                      OB.Model.Discounts.addManualPromotion(args.receipt, [args.orderline], {
                        rule: promotion,
                        definition: {}
                      });
                      if (promotion.get('discountType') === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                        OB.info('The previous product will be added to the ticket shortly');
                        OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.receipt, args.orderline);
                      }
                    }
                    OB.info('Ending of selected custom discount');
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  });
                } else {
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              }
            }
          });
        } else if (!OB.UTIL.isNullOrUndefined(args.attrs.kindOriginator) && args.attrs.kindOriginator === 'OB.UI.SearchProductCharacteristic' && !isPrimaryProductDigital) {
          OB.info('Custom discount popup is launched');
          OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
            popup: 'CUSTDIS_ModalPromotionSelector',
            args: {
              promotions: offers,
              callback: function (promotion) {
                if (promotion) {
                  OB.info('Applying selected custom discount');
                  OB.CUSTDIS.Utils.findPromotionProduct(args.orderline, {
                    rule: promotion
                  }, function (product) {
                    if (product) {
                      OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                      OB.CUSTDIS.Utils.setLineDiscount(args.orderline, product);
                      OB.Model.Discounts.addManualPromotion(args.receipt, [args.orderline], {
                        rule: promotion,
                        definition: {}
                      });
                      if (promotion.get('discountType') === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                        OB.info('The previous product will be added to the ticket shortly');
                        OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.receipt, args.orderline);
                      }
                    }
                    OB.info('Ending of selected custom discount');
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  });
                } else {
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              }
            }
          });
        } else if (args.orderline.get('qty') >= 1 && !OB.UTIL.isNullOrUndefined(args.orderline.get('custdisOffer'))) {
          var lines = args.receipt.get('lines').models;
          var modifySecondaryLine = true
          //to block increasing promo product qty when there is mismatch in quantities b/w primary and secondary products.
/*for (i = 0; i<lines.length; i++) {
    	            if (!OB.UTIL.isNullOrUndefined(lines[i].get('promotions'))) {
    	              if (!OB.UTIL.isNullOrUndefined(lines[i].get('custdisOffer')) && !OB.UTIL.isNullOrUndefined(lines[i].get('custdisOrderline')) ){
    	            	  if((lines[i].get('custdisOffer') === args.orderline.get('custdisOffer')) && (lines[i].get('qty') === args.orderline.get('qty')) ){
    	            		  // To check whether primary product qty is same as secondary product qty. If same then don't proceed to modify secondary line
    	            		  modifySecondaryLine=false;
    	            	  }
    	              }
    	            }
    	       	 }*/
          //-------
          if (modifySecondaryLine === false) {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          } else {
            var promotion = offers.models[0];
            var promoArray = [];
            promoArray.push.apply(promoArray, offers.models);
            args.orderline.get('promotions').forEach(function (promo) {
              var linePromo = promo.rule.get('name');
              for (var i = 0; i < promoArray.length; i++) {
                var offerName = promoArray[i].get('name');
                if (linePromo === offerName) {
                  promotion = promoArray[i];
                }
              }
            });
            //---to fetch total promo qty added in the order ---> for promo limit check
            var map = new Map();
            var ArrayOnlyCustOffer = [];
            var ArrayPriceAdjDis = [];
            var k = 0,
                count = 0; //count of promotion added in the receipt
            for (k = 0; k < lines.length; k++) {
              if (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions'))) {
                if (!OB.UTIL.isNullOrUndefined(lines[k].get('custdisOffer')) && (OB.UTIL.isNullOrUndefined(lines[k].get('custdisOrderline')) || (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].discountType) && lines[k].get('promotions')[0].discountType === 'CA5491E6000647BD889B8D7CDF680795'))) {
                  if (lines[k].get('custdisOffer') === promotion.get('id')) {
                    ArrayOnlyCustOffer.push(lines[k]); //compare the added promo with all promo's in line. Add only selected promo line  
                  }
                }
              }
            }

            for (let i = 0; i < ArrayOnlyCustOffer.length; i++) {
              if (!OB.UTIL.isNullOrUndefined(lines[i].get('promotions'))) {
                if (map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) && (args.receipt.get('custsdtDocumenttypeSearchKey') != 'BS' || args.receipt.get('custsdtDocumenttypeSearchKey') != 'BR')) {
                  map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), map.get(ArrayOnlyCustOffer[i].get('custdisOffer')) + ArrayOnlyCustOffer[i].get('qty'));
                } else {
                  map.set(ArrayOnlyCustOffer[i].get('custdisOffer'), ArrayOnlyCustOffer[i].get('qty'));
                }
              }
            }

            var keys = map.keys();
            var values = map.values();
            map.forEach(function (eachMap) {
              if (keys.next().value === promotion.get('id')) {
                count = count + values.next().value;
              }
            });
            //---------
            var promoLimitAvailable;
            count <= promotion.get('limitcount') ? promoLimitAvailable = true : promoLimitAvailable = false

            if (OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) || (!OB.UTIL.isNullOrUndefined(promotion.get('limitcount')) && promotion.get('limitcount') > 0 && promoLimitAvailable)) {
              //promotion.set('limitcount', promotion.get('limitcount') - args.orderline.get('qty'))
              if (promotion) {
                OB.info('Applying selected custom discount');
                OB.CUSTDIS.Utils.findPromotionProduct(args.orderline, {
                  rule: promotion
                }, function (product) {
                  if (product) {
                    OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                    OB.CUSTDIS.Utils.setLineDiscount(args.orderline, product);
                    OB.Model.Discounts.addManualPromotion(args.receipt, [args.orderline], {
                      rule: promotion,
                      definition: {}
                    });
                    if (promotion.get('discountType') === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                      OB.info('The previous product will be added to the ticket shortly');
                      OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.receipt, args.orderline);
                    }
                  }
                  OB.info('Ending of selected custom discount');
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                });
              } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              }
            } else {
              //console.log('limit not available for promotion: '+custDisOffer + ' with limit '+ totalAvailableQty);
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_PromotionLimit', [promotion.get('totalAvailableQty')]));
              //return;
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
          }
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      }, function () {
        //not to show promotion popup while increasing the primary prod qty if promo is not selected first time
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
    } else if (args.orderline && args.orderline.get('custdisAddByPromo')) {
      // Avoid to display mandatory proposal services while adding promotions
      args.receipt._loadRelatedServices(args.orderline.get('product').get('productType'), args.orderline.get('product').get('id'), args.orderline.get('product').get('productCategory'), function (data) {
        if (data && data.hasmandatoryservices) {
          args.newLine = false;
          args.orderline.set('hasRelatedServices', true);
          args.orderline.trigger('showServicesButton');
          args.orderline.set('hasMandatoryServices', true);
        }


        for (var i = 0; i < args.receipt.get('lines').length; i++) {
          if (args.receipt.get('lines').models[i].get('custdisAddByPromo')) {
            if (OB.UTIL.isNullOrUndefined(args.receipt.get('lines').models[i].get('custdisPromotion'))) {
              if (!OB.UTIL.isNullOrUndefined(args.receipt.get('lines').models[i + 1]) && args.receipt.get('lines').models[i].get('id') === args.receipt.get('lines').models[i + 1].get('custdisOrderline')) {
                args.receipt.get('lines').models[i].set('custdisPromotion', args.receipt.get('lines').models[i + 1].get('custdisPromotion'));
                args.receipt.get('lines').models[i].set('custdisAmount', 0);
                args.receipt.save();
              }
            }
          }
        }

        for (var i = 0; i < args.receipt.get('lines').length; i++) {
          if (args.receipt.get('lines').models[i].get('custdisAddByPromo') && !OB.UTIL.isNullOrUndefined(args.receipt.get('lines').models[i].get('custdisPromotion'))) {
            OB.Model.Discounts.addManualPromotion(args.receipt, [args.receipt.get('lines').models[i]], {
              rule: args.receipt.get('lines').models[i].get('custdisPromotion'),
              definition: {}
            });
          }
        }

        var returnLineArr = [],
            selectedLinesArr = [],
            primaryLine = args.receipt.get('lines').models[0],
            line;
        if (!OB.UTIL.isNullOrUndefined(primaryLine.get('returnLineId')) && !OB.UTIL.isNullOrUndefined(primaryLine.get('receiptLines'))) {
          for (var i = 0; i < primaryLine.get('receiptLines').length; i++) {
            returnLineArr.push(primaryLine.get('receiptLines')[i].lineId);
          }
        } else if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('returnLineId'))) {
          for (var i = 0; i < args.receipt.get('returnLineId').length; i++) {
            returnLineArr.push(args.receipt.get('returnLineId')[i]);
          }
        }

        if (!OB.UTIL.isNullOrUndefined(args.receipt.get('selectedLines')) && args.receipt.get('selectedLines').length > 0) {
          for (var j = 0; j < args.receipt.get('selectedLines').length; j++) {
            selectedLinesArr.push(args.receipt.get('selectedLines')[j]);
          }
          args.receipt.unset('selectedLines');
        }

        var saveReturnOrderLineForBundlePromoReinvoice = function (args, selectedLinesArr, returnLineArr, idx) {
          var selectedReturnLine;
          if (selectedLinesArr.length > 0) {
            if (idx < selectedLinesArr.length) {
              for (var j = 0; j < args.receipt.get('lines').models.length; j++) {
                line = args.receipt.get('lines').models[j];
                if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line)) {
                  selectedReturnLine = selectedLinesArr[idx].line.lineId;
                } else {
                  selectedReturnLine = selectedLinesArr[idx].lineId;
                }
                if (OB.UTIL.isNullOrUndefined(line.get('returnLineId'))) {
                  line.set('returnLineId', selectedReturnLine);
                  saveReturnOrderLineForBundlePromoReinvoice(args, selectedLinesArr, returnLineArr, idx + 1);
                } else if (line.get('returnLineId') === selectedReturnLine) {
                  idx++;
                }
              }
            }
          } else {
            if (returnLineArr.length > 0) {
              for (var i = 0; i < args.receipt.get('lines').models.length; i++) {
                line = args.receipt.get('lines').models[i];
                if (OB.UTIL.isNullOrUndefined(line.get('returnLineId'))) {
                  line.set('returnLineId', returnLineArr[i]);
                }
              }
            }
          }
        }

        var saveEpayDetailsForBundlePromoReinvoice = function (args, selectedLinesArr, idx) {
          var line, originalLineId;
          if (selectedLinesArr.length > 0) {
            if (idx < selectedLinesArr.length) {
              for (var j = 0; j < args.receipt.get('lines').models.length; j++) {
                line = args.receipt.get('lines').models[j];
                if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line)) {
                  originalLineId = selectedLinesArr[idx].line.lineId;
                } else {
                  originalLineId = selectedLinesArr[idx].lineId;
                }

                if (line.get('product').get('invoice_type') === 'Re Invoice' && !OB.UTIL.isNullOrUndefined(line.get('product').get('digiProductType')) && (line.get('product').get('digiProductType') === 'E' || line.get('product').get('digiProductType') === 'P' || line.get('product').get('digiProductType') === 'A' || line.get('product').get('digiProductType') === 'SP' || line.get('product').get('digiProductType') === 'SA')) {
                  if (!OB.UTIL.isNullOrUndefined(originalLineId) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx] && line.get('returnLineId') === originalLineId)) {
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.cpseEpaySerialno)) {
                      line.get('product').set('originalEpaySerialno', selectedLinesArr[idx].line.cpseEpaySerialno);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].cpseEpaySerialno)) {
                      line.get('product').set('originalEpaySerialno', selectedLinesArr[idx].cpseEpaySerialno);
                    }
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.cpseEpayrequest)) {
                      line.get('product').set('originalEpayRequest', selectedLinesArr[idx].line.cpseEpayrequest);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].cpseEpayrequest)) {
                      line.get('product').set('originalEpayRequest', selectedLinesArr[idx].cpseEpayrequest);
                    }
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.cpseEpayresponse)) {
                      line.get('product').set('originalEpayResponse', selectedLinesArr[idx].line.cpseEpayresponse);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].cpseEpayresponse)) {
                      line.get('product').set('originalEpayResponse', selectedLinesArr[idx].cpseEpayresponse);
                    }
                  }
                }
                idx++;
              }
            }
          }
        }

        var saveSerialNoandRfidForBundlePromoReinvoice = function (args, selectedLinesArr, idx) {
          var line, originalLineId;
          if (selectedLinesArr.length > 0) {
            if (idx < selectedLinesArr.length) {
              for (var j = 0; j < args.receipt.get('lines').models.length; j++) {
                line = args.receipt.get('lines').models[j];
                if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line)) {
                  originalLineId = selectedLinesArr[idx].line.lineId;
                } else {
                  originalLineId = selectedLinesArr[idx].lineId;
                }

                if (line.get('product').get('invoice_type') === 'Re Invoice') {
                  if (!OB.UTIL.isNullOrUndefined(originalLineId) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx] && line.get('returnLineId') === originalLineId)) {
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.obposEpccode)) {
                      line.get('product').set('obposEpccode', selectedLinesArr[idx].line.obposEpccode);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].obposEpccode)) {
                      line.get('product').set('obposEpccode', selectedLinesArr[idx].obposEpccode);
                    }
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.obposSerialNumber)) {
                      line.get('product').set('obposSerialNumber', selectedLinesArr[idx].line.obposSerialNumber);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].obposSerialNumber)) {
                      line.get('product').set('obposSerialNumber', selectedLinesArr[idx].obposSerialNumber);
                    }
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.obposRfidValid)) {
                      line.get('product').set('obposRfidValid', selectedLinesArr[idx].line.obposRfidValid);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].obposRfidValid)) {
                      line.get('product').set('obposRfidValid', selectedLinesArr[idx].obposRfidValid);
                    }
                    if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line) && !OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].line.obposBarcode)) {
                      line.get('product').set('obposBarcode', selectedLinesArr[idx].line.obposBarcode);
                    } else if (!OB.UTIL.isNullOrUndefined(selectedLinesArr[idx].obposBarcode)) {
                      line.get('product').set('obposBarcode', selectedLinesArr[idx].obposBarcode);
                    }
                  }
                }
                idx++;
              }
            }
          }
        }

        saveReturnOrderLineForBundlePromoReinvoice(args, selectedLinesArr, returnLineArr, 0);
        saveEpayDetailsForBundlePromoReinvoice(args, selectedLinesArr, 0);
        saveSerialNoandRfidForBundlePromoReinvoice(args, selectedLinesArr, 0);

        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      });
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());

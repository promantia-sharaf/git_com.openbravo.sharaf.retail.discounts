/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    var payments = _.filter(OB.MobileApp.model.get('payments'), function (payment) {
      return (payment.payment.searchKey !== 'CUSTSPM_payment.instantA' && payment.payment.searchKey !== 'CUSTSPM_payment.instantB');
    }).length;
    if (!args.context.get('order').get('isQuotation') && !payments) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_AvoidPaymentsWindowIfNoPayments'));
      return;
    }
    if (args.context.get('order').getGross() === 0) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_ErrNotCloseZeroAmount'));
      return;
    }
    var errFlag = false;
    order = args.context.get('order');
    if (order.get('isEditable')) {
      _.each(order.get('lines').models, function (line) {
        if (line.getQty() > 0 && line.get('discountedGross') < 0 || line.getQty() < 0 && line.get('discountedGross') > 0) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_ErrInValidAmount'));
          errFlag = true;
          return;
        }
      });
    }
    if (!errFlag) {
      if (OB.UTIL.isNullOrUndefined(order.attributes.isVerifiedReturn) || !order.attributes.isVerifiedReturn) {
         OB.CUSTDIS.Utils.validateBPCategoryPromotions(order.attributes.bp, "CUSTDIS_ErrBPCategoryInvalidPromotions", function (info) {
        if(info.length !== 0){
                 OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), info);
               } else {
                 var i = 0;
                 var lineHasCartPromo = false;
                 var nonPromotionalLines = [];
                 var productIds = [];
                 var promotionGross = 0;
                 _.each(args.context.get('order').get('lines').models, function (line) {  
                	 if (line.get('promotions') && line.get('promotions')[0] && line.get('promotions')[0].discountType === '61D65EECCF97487A82D169BB7E96832C'){
                		 lineHasCartPromo = true;
                     }
                	 if (line.get('promotions') && line.get('promotions')[0]){
                    	   productIds.push("'"+ line.attributes.product.id + "'");
                    	   nonPromotionalLines.push(line);
                    	   return;
                       }
                       
                       if (!lineHasCartPromo) {
                    	   if (line.get('custdisOffer')) {
                                 promotionGross = promotionGross + line.get('discountedGross');
                    	   } else {
                               var relatedLineId;
                               var relatedPromoLine;
                               if(line.get('relatedLines') && line.get('relatedLines')[0]){
                                       relatedLineId = line.get('relatedLines')[0].orderlineId;
                                       relatedPromoLine = _.find(args.context.get('order').get('lines').models, function (line) {
                                             if (line.id === relatedLineId) {
                                               return true;
                                             }
                                       });
                               }
                               if (relatedPromoLine && relatedPromoLine.get('custdisOffer')) {
                            	   if (line.get('product').get('productType') !== 'S') {
                            		   promotionGross = promotionGross + line.get('discountedGross');
                            	   }
                               } else {
                            		   productIds.push("'"+ line.attributes.product.id + "'");
                                       nonPromotionalLines.push(line);
                               }
                           }        
                       }
                 });

                if(!lineHasCartPromo){
                 var totalGross = args.context.get('order').getGross() - promotionGross;
                 var query = "select distinct O.* from m_offer_product OF join m_offer O on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id" //
                 + " where O.m_offer_type_id in ('61D65EECCF97487A82D169BB7E96832C') " //
                 + "and O.em_custdis_total_min_amount <= '" + totalGross  //
                 + "' and O.em_custdis_is_buy_and = 'false' and O.em_custdis_is_get_and = 'true'"  //
                 + " and OF.em_custdis_is_gift = 'false' and OF.m_product_id in (" + productIds.toString()  //
                 + ") and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.context.get('order').get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
                 + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "' and (O.dateto is null or O.dateto >= '" 
                 + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "') " 
                 //+ OB.CUSTDIS.Utils.addAvailabilityRule(moment())
                 + " order by O.name";
                 OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {
                       if (offers.length === 0) {
                         OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                       } else {
                         OB.info('Custom cart level discount popup is launched');
                         OB.MobileApp.view.$.containerWindow.getRoot().bubble('onShowPopup', {
                               popup: 'CUSTDIS_ModalPromotionSelector',
                               args: {
                                 promotions: offers,
                                 callback: function (promotion) {
                                       if (promotion) {
                                         OB.info('Applying selected custom discount');
                                         var select = "select OF.* from m_offer_product OF where OF.m_offer_id ='"+ promotion.id + "' and OF.em_custdis_is_gift = 'false'";
                                         OB.Dal.queryUsingCache(OB.Model.DiscountFilterProduct, select, [], function (products) {

                                                 var i,j;                                              
                                                 for(i = 0; i < products.length; i++) {
                                                 var p = products.models[i];
                                                 for(j = 0; j < nonPromotionalLines.length; j++) {
                                                       var l = nonPromotionalLines[j];
                                                         if (p.attributes.product === l.get('product').id) {
                                                               // The product is in the list of candidates products
                                                               args.orderline = l;
                                                               break;
                                                         }
                                                 }
                                                 if(args.orderline){
                                                       break;
                                                 }
                                               }
                                               OB.CUSTDIS.Utils.findPromotionProduct(args.orderline, {
                                                 rule: promotion
                                               }, function (product) {
                                                 if (product) {
                                                       OB.info('The previous promotion has the product to be added ' + product.get('_identifier'));
                                                       OB.CUSTDIS.Utils.setLineDiscount(args.orderline, product);
                                                       if (promotion.get('discountType') === '61D65EECCF97487A82D169BB7E96832C') {
                                                         OB.info('The previous product will be added to the ticket shortly - Cart Level');
                                                         OB.CUSTDIS.Utils.addPromotionProducts(promotion, args.context.get('order'), args.orderline);
                                                       }
                                                 }
                                                 OB.info('Ending of selected custom discount - Cart Level');
                                                 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                               });
                                         });
                                       } else {
                                         OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                       }
                                 }
                               }
                         });
                       }
                 }, function () {
                       OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                 });
                } else {
                 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
               }
         });
         } else{
               OB.UTIL.HookManager.callbackExecutor(args, callbacks);
         }
       }
  });

}());

/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_preRemovePayment', function (args, callbacks) {
    var searchKey = args.paymentToRem.get('kind'),
        paymentPrimaryProduct;

    if (!args.cancellation && !args.paymentToRem.get('custdisAutomaticIRPaymentRemoval') && (searchKey === 'CUSTSPM_payment.instantA' || searchKey === 'CUSTSPM_payment.instantB')) {
      paymentPrimaryProduct = _.find(args.receipt.get('lines').models, function (line) {
        return args.paymentToRem.get('paymentData') && args.paymentToRem.get('paymentData').lineId === line.get('id');
      });
      if (paymentPrimaryProduct) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_IRPaymentsCannotBeManuallyDeleted'));
        args.cancellation = true;
      }
    }
    
    // For Bank Bin Promotion. Linked payment check
    var linkedPayment = false;
    var isReturn = false;
    var isMainPayment = false;
    if (args.paymentToRem.get('paymentData') && args.paymentToRem.get('paymentData').data && args.paymentToRem.get('paymentData').data.custdisLinkedPaymentMethod) {
      linkedPayment = true;
    }
    
    if (args.paymentToRem.get('paymentData') && args.paymentToRem.get('paymentData').data && args.paymentToRem.get('paymentData').data.custdisMainPaymentMethod) {
      isMainPayment = true;
    }
    
    // For Bank Bin Promotion. Linked payment deletion in case of Return shouldn't happen
    if (linkedPayment && !OB.UTIL.isNullOrUndefined(args.receipt) && !OB.UTIL.isNullOrUndefined(args.receipt.get('isVerifiedReturn')) && args.receipt.get('isVerifiedReturn')) {
      isReturn = true;      
    }
    
    // For Bank Bin Promotion. Applicable payment deletion before linked payment in Return shouldn't happen
    _.each(args.receipt.get('lines').models, function(line) {
       var promotions = line.get('promotions');
       _.each(promotions, function(promotion) {
          if( promotion.discountType === 'B277B63BDA6E4B67B3D8F491FC3C4E3B' && !isReturn && isMainPayment) {
            args.cancellation = true;
            OB.UTIL.showWarning(OB.I18N.getLabel('CUSTDIS_CannotDeleteLinkedPayment'));
          }
       });
    });
    
    // Removing promotion in case if LinkedPayment method is deleted
    if (!args.cancellation && linkedPayment && !isReturn) {
      _.each(args.receipt.get('lines').models, function(line) {
         var promotions = line.get('promotions');
         var newPromotions = new Array();
         _.each(promotions, function(promotion) {
            if( promotion.discountType !== 'B277B63BDA6E4B67B3D8F491FC3C4E3B') {
              newPromotions.push(promotion);
            }else {
               line.unset('custdisBankBinOffer');
               line.unset('custdisLinedisamt'); 
            }
         });
         line.set('promotions', newPromotions);
      });
    }
    
    // Removing Loyalty promotion in case if LinkedPayment method is deleted 
    if (!args.cancellation && linkedPayment && !isReturn) {
      _.each(args.receipt.get('lines').models, function(line) {
         var promotions = line.get('promotions');
         var newPromotions = new Array();
         _.each(promotions, function(promotion) {
            if( promotion.discountType !== '25ECBE384EAB4C1EB69948FF99FC8456') {
              newPromotions.push(promotion);
            }else {
                line.unset('custdisLoyaltyDiscountOffer');
                line.unset('custdisLineloyldisamt');
            }
         });
         line.set('promotions', newPromotions);
      });
    }
    
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());
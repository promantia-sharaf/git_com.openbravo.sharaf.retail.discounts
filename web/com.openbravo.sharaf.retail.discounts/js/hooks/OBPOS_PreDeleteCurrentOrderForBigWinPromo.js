/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */
(function() {

    OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteCurrentOrder', function(args, callbacks) {
        if (!OB.UTIL.isNullOrUndefined(args.receipt)) {
            var isBigWinPromotionApplied = false;
            var offerid;
            var slotId;
            var me = args;
            args.receipt.get('lines').models.forEach(function (line) {
                if (!OB.UTIL.isNullOrUndefined(line.attributes.promotions)) {
                  var promotion = line.attributes.promotions;
                  promotion.forEach(function (promo) {
                    if (!OB.UTIL.isNullOrUndefined(promo) && !OB.UTIL.isNullOrUndefined(promo.discountType)) {
                  	 var discountType=promo.discountType;
                  	  if(discountType =='399EF97C508B40E787A444CE3855A7E4'){
                  		  isBigWinPromotionApplied = true;
                  		  offerid = promo.ruleId;
                          slotId = me.receipt.get('custDisBigWinnerPromotionSlotID');
                  	  }
                  	               
                    } 
                  });
                }
            });
            if(isBigWinPromotionApplied){
              args.receipt.unset('custDisBigWinnerPromotion');
              args.receipt.unset('custDisBigWinnerPromotionApplied');
              args.receipt.unset('custDisBigWinnerPromotionSlotID');
          	  var process = new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.BigWinnerPromotionService');
                      process.exec(
                        {
                          isVoid:true,
                          documentNo: OB.MobileApp.model.receipt.get('documentNo'),
                          mofferid: offerid,
                          slottimeid:slotId 
                        },
                        function(response) {}
                      );
            }
        }
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);

    });


}());
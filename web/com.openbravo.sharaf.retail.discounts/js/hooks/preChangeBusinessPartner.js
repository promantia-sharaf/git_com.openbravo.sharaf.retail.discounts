(function () {
	
  OB.UTIL.HookManager.registerHook('OBPOS_preChangeBusinessPartner', function (args, callbacks) {
	var info = OB.CUSTDIS.Utils.validateBPCategoryPromotions(args.bp, "CUSTDIS_ErrBPCategoryInvalidPromotions", function (info) {
      if(info.length !== 0){
		OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), info);
	  } else {
		OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	  }
	  });
  });
}());
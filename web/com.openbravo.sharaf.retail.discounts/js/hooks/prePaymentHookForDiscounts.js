/**
 *
 */
(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function(args, callbacks) {
          //to fetch the promo product lines
          var map = new Map();
          var multipleMap = new Map();  //Map created for buy 1 get multiple products free

          var ArrayOnlySecondaryLine = [];
          var ArrayOnlyPriceAdj = [];
          var lines = args.context.get('order').get('lines').models;
          var k = 0, limitCount;
          var withoutAttr = false;

          //for sharaf promo products
          for (k = 0; k < lines.length; k++) {
            if (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions'))) {
              if (lines[k].get('promotions').length>0 && (!OB.UTIL.isNullOrUndefined(lines[k].get('custdisOffer')) && !OB.UTIL.isNullOrUndefined(lines[k].get('custdisOrderline'))) && (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].discountType) && lines[k].get('promotions')[0].discountType !== 'CA5491E6000647BD889B8D7CDF680795')) {
            	if(!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule) && multipleMap.get(lines[k].get('custdisOffer')) !== lines[k].get('custdisOrderline') ){ //to avoid error in return flow
            		//===== To check whether PQL is available for this promo
            		withoutAttr = lines[k].get('promotions')[0].rule.name ? true : false;
            		if(withoutAttr){
            			limitCount = !OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule.limitcount) ? lines[k].get('promotions')[0].rule.limitcount : undefined;
            		}else {
            			limitCount = !OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule.get('limitcount')) ? lines[k].get('promotions')[0].rule.get('limitcount') : undefined;
            		}
            		//=====
              		if(!OB.UTIL.isNullOrUndefined(limitCount)){ //to add promotions to array only if PQL is available
    					ArrayOnlySecondaryLine.push(lines[k]); //add only secondary lines
    					//Code for handling buy1getmultiple
    					multipleMap.set(lines[k].get('custdisOffer'),lines[k].get('custdisOrderline'));
              			}
            		}
                  }
                }
              }

          //For priceAdjustment promo
          for (k = 0; k < lines.length; k++) {
              if (!OB.UTIL.isNullOrUndefined(lines[k].get('promotions'))) {
                if (lines[k].get('promotions').length>0 && !OB.UTIL.isNullOrUndefined(lines[k].get('custdisOffer')) && ((!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].discountType) && lines[k].get('promotions')[0].discountType === 'CA5491E6000647BD889B8D7CDF680795')) ){
                	if(!OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule)){ //to avoid error in return flow
                		//===== To check whether PQL is available for this promo
                		withoutAttr = lines[k].get('promotions')[0].rule.name ? true : false;
                		if(withoutAttr){
                			limitCount = !OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule.limitcount) ? lines[k].get('promotions')[0].rule.limitcount : undefined;
                		}else {
                			limitCount = !OB.UTIL.isNullOrUndefined(lines[k].get('promotions')[0].rule.get('limitcount')) ? lines[k].get('promotions')[0].rule.get('limitcount') : undefined;
                		}
                		//=====
                    	if(!OB.UTIL.isNullOrUndefined(limitCount)){//to add promotions to array only if PQL is available
                    		ArrayOnlyPriceAdj.push(lines[k]); //add only primary lines and priceAdjustment discount types
                            lines[k].set('priceAdjPromo', true);
        				}
                	}
                   }
                  }
                }

          //Adding secondary line offers to map
          for (var i = 0; i < ArrayOnlySecondaryLine.length; i++) {
              if (!OB.UTIL.isNullOrUndefined(ArrayOnlySecondaryLine[i].get('promotions'))) {
                if (map.get(ArrayOnlySecondaryLine[i].get('custdisOffer')) && (args.context.get('order').get('custsdtDocumenttypeSearchKey') !== 'BS' && args.context.get('order').get('custsdtDocumenttypeSearchKey') !== 'BR')) {
                  map.set(ArrayOnlySecondaryLine[i].get('custdisOffer'), map.get(ArrayOnlySecondaryLine[i].get('custdisOffer')) + ArrayOnlySecondaryLine[i].get('qty') );
                  } else {
                      map.set(ArrayOnlySecondaryLine[i].get('custdisOffer'), ArrayOnlySecondaryLine[i].get('qty'));
                    }
                  }
                }

          //Adding Price Adjustment offers to map
          for (i = 0; i < ArrayOnlyPriceAdj.length; i++) {
              if (!OB.UTIL.isNullOrUndefined(ArrayOnlyPriceAdj[i].get('promotions'))) {
                if (map.get(ArrayOnlyPriceAdj[i].get('custdisOffer')) && (args.context.get('order').get('custsdtDocumenttypeSearchKey') !== 'BS' || args.context.get('order').get('custsdtDocumenttypeSearchKey') !== 'BR')) {
                     map.set(ArrayOnlyPriceAdj[i].get('custdisOffer'), map.get(ArrayOnlyPriceAdj[i].get('custdisOffer')) + ArrayOnlyPriceAdj[i].get('qty') );
                  } else {
                      map.set(ArrayOnlyPriceAdj[i].get('custdisOffer'), ArrayOnlyPriceAdj[i].get('qty'));
                    }
                  }
                }


          		  var data = [];
                  var keys = map.keys();
                  var values = map.values();
                  map.forEach(function(eachMap) {
                    var obj = {
                      'id': keys.next().value,
                      'qty': values.next().value
                    }
                    data.push(obj);
                  });
                  // ajax call
                  if(!OB.UTIL.isNullOrUndefined(data) && data.length > 0){
                  new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.PromotionLimitServicePaymentCheck').exec({
                    jsondata: JSON.stringify(data),
                    client: OB.MobileApp.model.attributes.terminal.client
                  }, function(data) {
                    if (data.exception) {
                      // me.args.promotions.get(promotion).set('limitcount',-1);
                      OB.UTIL.showLoading(false);
                      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBPOS_MsgApplicationServerNotAvailable'));
                    } else {
                      var stringbuild = "\n";
                      var flag = false;
                      if (!OB.UTIL.isNullOrUndefined(data) && !OB.UTIL.isNullOrUndefined(data.OfferList)) {
                        data.OfferList.forEach(function(obj) {
                          flag = true;
                          stringbuild = stringbuild + obj.name + ':' + 'Total Available Quantity:' + obj.qty_avail + '\n';
                        });
                        if (flag === true) {
                          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_PromotionLimitForPayment', [stringbuild]));
                        }
                      }
                      if (!OB.UTIL.isNullOrUndefined(data) && data === "success") {
                        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                      }
                      OB.UTIL.showLoading(false);
                    }
                  });
                  } else{
                	  OB.UTIL.showLoading(false);
                	  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                  }
    });
}());
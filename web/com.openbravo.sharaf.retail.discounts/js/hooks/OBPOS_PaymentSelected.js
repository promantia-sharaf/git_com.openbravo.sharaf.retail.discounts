/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {
  OB.BANKBIN = OB.BANKBIN || {};
  OB.BANKBIN.CHECKANDAPPLY = OB.BANKBIN.CHECKANDAPPLY || {};
  OB.UTIL.HookManager.registerHook('OBPOS_PaymentSelected', function (args, callbacks) {
    args.order.unset('custdisLinkedPaymentMethod');
    args.order.unset('custDisBankBinPromotion');
    args.order.unset('custdisBankBinDiscountApplied');
    args.order.unset('custdisCardBinNumber');
    args.order.unset('custdisMaxDiscountAmt');
    args.order.unset('custdisDiscountPercentage');
    args.order.unset('custdisMinPurchaseAmountBankBin');
    args.order.unset('custdisMinPurchaseAmountBankBin');
    args.order.unset('custdisBankBinDiscountApplied');
    args.order.unset('custdisBankBinValidated');
    args.order.unset('custdisSelectedLineAmount'); 
    
    // Loyalty Discount
    
    args.order.unset('custDisLoyaltyDiscountPromotion');
    args.order.unset('custdisLoyaltyDiscountApplied');
    args.order.unset('custdisLoyaltyCardNumber');
    args.order.unset('custdisLoyaltyMonthValidity');
    args.order.unset('custdisLoyaltyYearValidity');
        
    var loyalpaymentalreadypresent = false,
    loyaltyPaymentMethodSelected = false,   
    studentDiscountPayment = false,
    alredyBundleOrBuyXAndBuyYPresent = false,
    PromoPreferenceName,
    enabledLoyaltyPayMethod;
    
  if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTDIS_studentDiscount)){
	  PromoPreferenceName = OB.MobileApp.model.get('permissions').CUSTDIS_studentDiscount;
  }
        var enabledLoyaltyDiscountPayMethod = new Array();
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
          enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
        }
         if (!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) && 
           !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.searchKey) && !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod) &&
           !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[args.paymentSelected.paymentMethod.searchKey])) {
              loyaltyPaymentMethodSelected = true; 
         } 
         
         if (!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) && 
                 !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.searchKey) &&
                 !OB.UTIL.isNullOrUndefined(args.paymentSelected.payment._identifier) && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTDIS_studentDiscount)
                 && args.paymentSelected.payment._identifier === OB.MobileApp.model.get('permissions').CUSTDIS_studentDiscount){
        	 studentDiscountPayment = true;
         }
         for(var i=0; i<args.order.get('lines').models.length;i++){
        	 if(!OB.UTIL.isNullOrUndefined(args.order.get('lines').models[i].get('promotions'))){
        		 var promo = args.order.get('lines').models[i].get('promotions');
       		   _.each(promo, function(promotions){        			   
       			   if(promotions.discountType === '6A3C2313136147A6B2D1B8D2F5F768E6' || promotions.discountType === '9AC09641F86C46C4AF7BC8630325B26C'){
       				alredyBundleOrBuyXAndBuyYPresent = true;
       			   }
       		   });
        	 }
         }
         
     
    // Allow only one loyalty payment Method in the receipt
    var bankbinIssued = false;
            OB.MobileApp.model.receipt.get('lines').models.forEach(function(line) {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisBankBinOffer'))) {
                  bankbinIssued = true;  
                }
            });        
    if(bankbinIssued && loyaltyPaymentMethodSelected) {
     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Loyalty Program discount not applicable as Bank Bin Promotion discount has been applied in the invoice");
     args.cancelOperation = true; 
    } else if (loyaltyPaymentMethodSelected) { 
        _.each(OB.MobileApp.model.receipt.getPaymentStatus().payments.models, function(payment) {
         if (!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[payment.get('kind')])) {
           loyalpaymentalreadypresent = true;  
           OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Loyalty Payment Method already added in the receipt");
           args.cancelOperation = true;
         }
       }); 
    } 
    
    if(studentDiscountPayment && alredyBundleOrBuyXAndBuyYPresent){
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),OB.I18N.getLabel('CUSTDIS_StudentPromoError',[PromoPreferenceName]));
        args.cancelOperation = true;
    }else{
        if(!bankbinIssued && !loyalpaymentalreadypresent && loyaltyPaymentMethodSelected && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt) && OB.MobileApp.model.receipt.get('net') > 0) {
            OB.BANKBIN.CHECKANDAPPLY(args,callbacks); 
         } else if (!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) && 
            !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier) && 
             args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier === 'Credit Card' && !args.order.get('isVerifiedReturn')) {
            OB.BANKBIN.CHECKANDAPPLY(args,callbacks);      
         } else {
             OB.UTIL.HookManager.callbackExecutor(args, callbacks);
         }  	
    }
  });
}());
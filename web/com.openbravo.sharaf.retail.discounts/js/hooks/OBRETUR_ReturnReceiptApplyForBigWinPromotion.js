/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function() {
    OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function(args, callbacks) {
    	
     	var isBigWinPromotionApplied = false;
     	for (var i = 0; i < args.order.receiptLines.length; i++) {
            if (!OB.UTIL.isNullOrUndefined(args.order.receiptLines[i].custDisBigWinnerPromotion)) {
            	isBigWinPromotionApplied = true;
            }
        }
     	
        if (args.selectedLines.length > 0 && isBigWinPromotionApplied && args.selectedLines.length !== args.order.receiptLines.length) {
             OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Big Winner Promotion present in the Original Invoice -No partial Return Allowed");
             args.cancellation = true;
             OB.UTIL.HookManager.callbackExecutor(args, callbacks);
         }else {
        	 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
         }

    });
}());
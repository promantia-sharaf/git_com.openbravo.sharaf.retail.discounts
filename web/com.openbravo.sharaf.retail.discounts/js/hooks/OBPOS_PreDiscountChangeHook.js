/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_preDiscountChangeHook', function (args, callbacks) {
    if (args && args.inEvent && args.inEvent.model && args.inEvent.model.attributes && args.inEvent.model.attributes.discountType && args.inEvent.model.attributes.discountType === '5B59AAF5B6BF477C912A7DFD8A7EE7C7') {
      if (args.discountsContainer && OB.UTIL.isNullOrUndefined(args.discountsContainer.amt)) {
        args.discountsContainer.amt = 0;
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
/*
Promantia Development
*/
 
OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function(args, callbacks) {
  // For handling loyalty and tamara partial return issue   
 if (args.receipt.get('isVerifiedReturn')) {
    var returnableLinesDiscountAmount = 0;
    var linkedPayment;
    OB.MobileApp.model.receipt.get('lines').models.forEach(function (line) {          
          if(!OB.UTIL.isNullOrUndefined(line.get('custdisLineloyldisamt'))){
              returnableLinesDiscountAmount += line.get('custdisLineloyldisamt');
              if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                  returnableLinesDiscountAmount = +returnableLinesDiscountAmount.toFixed(3);
              }else{
                  returnableLinesDiscountAmount = +returnableLinesDiscountAmount.toFixed(2);
              }
          }
          if (!OB.UTIL.isNullOrUndefined(line.get('custdisLineloyldisamt')) && !OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
                var originalPayments = line.get('originalPayments');
                originalPayments.forEach(function (orgPayment) {
                  if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
                  && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
                   && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
                       linkedPayment = orgPayment;
                   } 
                });
          }
                   
    });
    if(!OB.UTIL.isNullOrUndefined(linkedPayment) && args.paymentToAdd.get('kind') === linkedPayment.kind && args.paymentToAdd.get('amount') > returnableLinesDiscountAmount) {
      args.paymentToAdd.set('amount',returnableLinesDiscountAmount);
      args.cancellation = true;
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
    }  
 } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
 }    
});
 
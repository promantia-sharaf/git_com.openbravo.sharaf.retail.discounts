/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    enyo.$.scrim.show();
    var deleteUtils = {

      mainProductLine: [],

      // Determine if all products for a promotion are selected to delete
      promotionHaveAllProductIncluded: function (lineId) {
        var result = true,
            promoLines = _.filter(args.order.get('lines').models, function (line) {
             flg = true;  
              if(OB.UTIL.isNullOrUndefined(line.get('isVerifiedReturn')) && !line.get('isVerifiedReturn')) {
                flg = line.get('IsbuyXGetYpercen') !== 'G' ? true :false;
              }
            return flg && line.get('custdisOrderline') === lineId || line.id === lineId;
          });
        _.each(promoLines, function (line) {
          var selectedLine = _.find(args.selectedLines, function (l) {
            return line.id === l.id;
          });
          if (!selectedLine) {
            result = false;
          }
        });
        return result;
      },

      // Add main product line
      addMainProduct: function (lineId) {
        var line = _.find(this.mainProductLine, function (l) {
          return l === lineId;
        });
        if (!line) {
          this.mainProductLine.push(lineId);
        }
      }

    };
    
    // Find main promotion products
	var deleteGross = 0;
	var deleteLines = [];
    _.each(args.selectedLines, function (line) {
    
      if(OB.UTIL.isNullOrUndefined(line.get('custdisOffer')) && line.get('IsbuyXGetYpercen')=== 'P' && !line.get('isVerifiedReturn')) {
        promoarr = _.filter(args.order.get('lines').models, function (l) {
            return l.get('custdisOrderline') === line.id ;
          });
          if(promoarr.length > 0) {
             promoarr[0].unset('custdisOffer');
             promoarr[0].unset('promotions');
             promoarr[0].unset('custdisAmount');
             promoarr[0].unset('custdisOrderline');
             promoarr[0].unset('IsbuyXGetYpercen');
        }  
        
      } else if(line.get('custdisOffer') && !OB.UTIL.isNullOrUndefined(line.get('IsbuyXGetYpercen')) &&
              line.get('IsbuyXGetYpercen') === 'G' && line.get('promotions')[0].discountType === '9AC09641F86C46C4AF7BC8630325B26C'
              && !line.get('isVerifiedReturn')) {
        promoarr = _.filter(args.order.get('lines').models, function (l) {
            return line.get('custdisOrderline') === l.id ;
          });
        if(promoarr.length > 0) {
             promoarr[0].unset('IsbuyXGetYpercen');
             line.unset('custdisOrderline');
        }  
       
      }else if(line.get('custdisOffer')) {
        deleteUtils.addMainProduct(line.get('custdisOrderline') ? line.get('custdisOrderline') : line.id);
        deleteGross = deleteGross + line.get('discountedGross');
      } else {
        deleteGross = deleteGross + line.get('discountedGross');
      }
      deleteLines.push(line);
    });
    
    // Find service products 
    _.each(args.order.get('lines').models, function (receiptLine) {
      	  var relatedLineId;
            var relatedServiceLine;
            if(receiptLine.get('relatedLines') && receiptLine.get('relatedLines')[0]) {
                    relatedLineId = receiptLine.get('relatedLines')[0].orderlineId;
                    relatedServiceLine = _.find(args.order.get('lines').models, function (line) {
                          if (line.id === relatedLineId) {
                            return true;
                          }
                    });
            }
            if (relatedServiceLine && (deleteLines[0].id === relatedLineId)) {
         	   if (receiptLine.get('product').get('productType') === 'S') {
         		deleteGross = deleteGross + receiptLine.get('discountedGross');
         	   }
            }    	  
      });
    
   
    var error = false;
	var cartLevelDiscountType = false;
	var afterDeleteGross = args.order.get('gross') - deleteGross;
	if(deleteUtils.mainProductLine.length === 0) {
	  var j;
	  for(j = 0; j < args.order.get('lines').length; j++) {
		var line = args.order.get('lines').models[j];
        if (line.get('promotions') && line.get('promotions')[0] && line.get('promotions')[0].discountType === '61D65EECCF97487A82D169BB7E96832C' && args.order.get('custdisTotalMinAmount') > afterDeleteGross) {
		  deleteUtils.addMainProduct(line.get('custdisOrderline') ? line.get('custdisOrderline') : line.id);
		  cartLevelDiscountType = true;
		  break;
        }
      }
	}
    // Verify if delete a complete promotion
    _.each(deleteUtils.mainProductLine, function (line) {
      if (cartLevelDiscountType || !deleteUtils.promotionHaveAllProductIncluded(line)) {
        error = true;
        args.cancelOperation = true;
        enyo.$.scrim.hide();
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		/*if(cartLevelDiscountType){
		  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTDIS_ReturnCartLevelLines'));
		} else {*/
		  var info = [],
            promoLines = _.filter(args.order.get('lines').models, function (l) {
              return l.get('custdisOrderline') === line || l.get('originalOrderLineId') === line || l.id === line;
          });
          ln = _.filter(args.order.get('lines').models, function (l) {
            return l.id === line;
          });
          if(OB.UTIL.isNullOrUndefined(ln[0].get('isVerifiedReturn')) || !ln[0].get('isVerifiedReturn')) {
                 promoLines = _.filter(promoLines, function (line) {
                return line.get('IsbuyXGetYpercen') !== 'G';
              });
          }
          _.each(promoLines, function (l) {
            if (info.length === 0) {
              var promo = _.find(l.get('promotions'), function (promo) {
                return promo.ruleId === l.get('custdisOffer');
              });
              info.push(OB.I18N.getLabel('CUSTDIS_ErrPromotionRemoveLines', [promo ? promo.name : '']));
            }
            info.push('- ' + l.get('product').get('_identifier') + ' (' + OB.I18N.formatCurrency(l.get('gross')) + ')');
          });
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
		}
 //     }
    });

    if (!error) {
      // Remove redemption payments
      _.each(args.selectedLines, function (line) {
         if(OB.UTIL.isNullOrUndefined(line.get('isVerifiedReturn'))) {
               promoLines = _.filter(args.order.get('lines').models, function (l) {
              return l.get('custdisOrderline') === line.id && l.get('IsbuyXGetYpercen') === 'G' 
              && OB.UTIL.isNullOrUndefined(l.get('isVerifiedReturn'));
              });
         if(promoarr.length === 1) {
             promoarr[0].unset('custdisOffer');
             promoarr[0].unset('promotions');
             promoarr[0].unset('custdisAmount');
             promoarr[0].unset('custdisOrderline');
             promoarr[0].unset('IsbuyXGetYpercen');
         }
         }
        if (line.get('custdisIrType') === 'IRA' || line.get('custdisIrType') === 'IRB') {
          var payments = _.filter(args.order.get('payments').models, function (p) {
            return p.get('paymentData') && (p.get('paymentData').lineId === line.get('custdisOrderline') || p.get('paymentData').lineId === line.id);
          });
          if (payments && payments.length > 0) {
            _.each(payments, function (payment) {
              payment.set('custdisAutomaticIRPaymentRemoval', true, {
                silent: true
              });
              args.order.removePayment(payment);
            });
          }
        }
      });
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
   }
  });

}());
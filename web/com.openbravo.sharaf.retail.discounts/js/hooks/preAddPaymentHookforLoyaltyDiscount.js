/*
Promantia Development
*/
 
OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function(args, callbacks) {
 if (!args.receipt.get('isVerifiedReturn')) { 
  var loyaltyPaymentMethodSelected = false, 
      enabledLoyaltyPayMethod;
  if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
     enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
  }
  var searchKey = !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('kind'))?args.paymentToAdd.get('kind'):args.paymentToAdd.get('paymentMethod').searchKey
  if (!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod) && !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[searchKey])) {
          loyaltyPaymentMethodSelected = true; 
  }
  var flag = false;     
 if (loyaltyPaymentMethodSelected 
      && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('custdisLoyaltyDiscountApplied'))
      && OB.MobileApp.model.receipt.get('custdisLoyaltyDiscountApplied')) {
      _.each(OB.MobileApp.model.receipt.get('payments').models, function(payment) {
         var skey = (!OB.UTIL.isNullOrUndefined(payment.get('paymentMethod')) && !OB.UTIL.isNullOrUndefined(payment.get('paymentMethod')).searchKey)
           ? payment.get('paymentMethod').searchKey : payment.get('kind');
         if (!OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[skey])) { 
           flag = true;
           OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Loyalty Discount Payment already applied to the receipt. Please choose some other Payment Method');
            args.cancellation = true;
        //    args.cancelOperation = true;
         }
       }); 
  } else if(loyaltyPaymentMethodSelected && (OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('custdisLoyaltyDiscountApplied')) ||
            !OB.MobileApp.model.receipt.get('custdisLoyaltyDiscountApplied'))){
    flag = true;
    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Loyalty Discount Payment is not allowed to pay, Please choose some other payment Method');
    args.cancellation = true;
  } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
  if(flag = false) {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  }
 } else {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
 }    

});
 
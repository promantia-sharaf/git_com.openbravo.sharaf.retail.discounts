/**
 *Promantia development
 */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_LineSelected', function (args, callbacks) {
    var enableButton = true,
        selectedLines = args.selectedLines,
        selectedLinesSameQty = args.context.$.multiColumn.$.rightPanel.$.keyboard.selectedModelsSameQty,
        selectedLinesLength = selectedLines ? args.selectedLines.length : 0,
        product, i;
    if (selectedLinesLength > 1) {
      for (i = 0; i < selectedLinesLength; i++) {
        product = selectedLines[i].get('product');
      if (!product.get('groupProduct') || (product.get('productType') === 'S' && product.get('isLinkedToProduct')) || !selectedLines[i].get('isEditable') || product.get('isSerialNo')) {
              enableButton = false;
              break;
          }
    	  
    	  if (!OB.UTIL.isNullOrUndefined(selectedLines[i].get('custdisAddByPromo')) && !(product.get('productType') === 'S')) {
    		  enableButton = false;
    		  break;
    	  }
      }
      
      if (enableButton && !selectedLinesSameQty) {
          enableButton = false;
      }
    } else if (selectedLinesLength === 1) {
      product = selectedLines[0].get('product');
      if (!product.get('groupProduct') || (product.get('productType') === 'S' && product.get('isLinkedToProduct')) || !selectedLines[0].get('isEditable') || product.get('isSerialNo')) {
          enableButton = false;
      }
      
      if (!OB.UTIL.isNullOrUndefined(selectedLines[0].get('custdisAddByPromo')) && !(product.get('productType') === 'S')) {
        enableButton = false;
      }
    } else {
      enableButton = false;
    }
    args.context.enableKeyboardButton(enableButton);
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
  OB.UTIL.CUSTDISUtils = OB.UTIL.CUSTDISUtils || {};
  OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {

    if (args.selectedLines.length === 0) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }

    //For promo products, to allow primary line to return if secondary lines are completely returned
    _.each(args.selectedLines, function (line) {
      if (OB.UTIL.isNullOrUndefined(line.line.custdisOrderline) && !OB.UTIL.isNullOrUndefined(line.line.custdisOffer)) {
        args.order.receiptLines.forEach(function (rline) {
          if ((!OB.UTIL.isNullOrUndefined(rline.custdisOffer) && !OB.UTIL.isNullOrUndefined(rline.custdisOrderline)) && rline.custdisOrderline === line.line.lineId) {
            if (rline.remainingQuantity < 1) {
              line.line.isSecondaryQtyZero = true;
            }
          }
        });
      }
    });

    var returnUtils = {

      mainProductLine: [],
      selectedLineIds: [],

      // Determine if all products for a promotion are selected to return
      promotionHaveAllProductIncluded: function (lineId) {
        var result = true,
            promoLines = _.filter(args.order.receiptLines, function (line) {
            return line.custdisOrderline === lineId || line.lineId === lineId;
          });
        _.each(promoLines, function (line) {
          var selectedLine = _.find(args.selectedLines, function (l) {
            return line.lineId === l.line.lineId || (l.line.isSecondaryQtyZero ? l.line.isSecondaryQtyZero : false);
          });
          if (!selectedLine) {
            result = false;
          }
        });
        return result;
      },

      // Add main product line
      addMainProduct: function (lineId) {
        var line = _.find(this.mainProductLine, function (l) {
          return l === lineId;
        });
        if (!line) {
          this.mainProductLine.push(lineId);
        }
      }
    };

    // Find main promotion products selected to be returned
    var selectedProduct = [];
    _.each(args.selectedLines, function (line) {
      selectedProduct.push(line.line.custdisOrderline ? line.line.custdisOrderline : line.line.lineId);
    });

    // Find main promotion products in ticket
    _.each(args.order.receiptLines, function (line) {
      if (line.custdisOffer && line.productType !== 'S') {
        returnUtils.addMainProduct(line.custdisOrderline ? line.custdisOrderline : line.lineId);
      }
    });

    // Calculate total amount of order
    var orderTotal = 0;
    _.each(args.order.receiptLines, function (line) {
      if (OB.UTIL.isNullOrUndefined(line.custdisOrderline) && !OB.UTIL.isNullOrUndefined(line.taxes) && line.remainingQuantity > 0) {
        if (line.taxes.length > 0) {
          orderTotal = orderTotal + (line.linenetamount > 0 ? line.linenetamount : line.baseNetUnitPrice) + line.taxes[0].taxAmount;
        } else {
          orderTotal = orderTotal + (line.linenetamount > 0 ? line.linenetamount : line.baseNetUnitPrice);
        }
      } else if (line.priceIncludesTax) {
        orderTotal = orderTotal + line.grossUnitPrice;
      }
    });

    var priceAdjustmentAmt = 0;
    _.each(args.order.receiptLines, function (line) {
      if (line.promotions && line.promotions.length > 0) {
        for (i = 0; i < line.promotions.length; i++) {
          var disc = line.promotions[i];
          if (disc.discountType === 'CA5491E6000647BD889B8D7CDF680795') {
            priceAdjustmentAmt = priceAdjustmentAmt + disc.amt;
          }
        }
      }
    });

    orderTotal = orderTotal - priceAdjustmentAmt;

    var returnGross = 0;
    _.each(args.selectedLines, function (line) {
      returnUtils.selectedLineIds.push(line.line.lineId);
      if (!OB.UTIL.isNullOrUndefined(line.line.linenetamount) && !OB.UTIL.isNullOrUndefined(line.line.taxes)) {
        if (line.line.taxes.length > 0 && !OB.UTIL.isNullOrUndefined(line.line.taxes[0].taxAmount)) {
          returnGross = returnGross + (line.line.linenetamount > 0 ? line.line.linenetamount : (line.line.custdisOrderline ? (line.line.linenetamount > 0 ? line.line.linenetamount : line.line.baseNetUnitPrice) : line.line.baseNetUnitPrice)) + line.line.taxes[0].taxAmount;
        } else {
          returnGross = returnGross + (line.line.linenetamount > 0 ? line.line.linenetamount : (line.line.custdisOrderline ? (line.line.linenetamount > 0 ? line.line.linenetamount : line.line.baseNetUnitPrice) : line.line.baseNetUnitPrice));
        }
      } else if (line.line.priceIncludesTax) {
        returnGross = returnGross + line.line.grossUnitPrice;
      }
    });

    // Verify if main product with promo has been already returned
    var retMainProductWithPromo = false;
    _.each(args.order.receiptLines, function (line) {
      if (line.lineId === returnUtils.mainProductLine[0] && line.remainingQuantity === 0) {
        retMainProductWithPromo = true;
      }
    });

    var error = false;

    var cartLevelDiscountType = false;
    var cartValueError = false;
    var cartLevelNGiftItem = [];
    var cartLevelGiftItem = [];
    var cartLevelDiscountId = "";
    var returnNGiftItem = [];
    var returnGiftItem = [];

    OB.UTIL.CUSTDISUtils.abcd = function (returnGross, callback) {
      if (returnGross > 0) {
        var afterReturnGross = orderTotal - returnGross;
        var j, count = 0;
        for (j = 0; j < args.order.receiptLines.length; j++) {
          var line = args.order.receiptLines[j];
          if (line.promotions && line.promotions[0] && line.promotions[0].discountType === '61D65EECCF97487A82D169BB7E96832C') {
            cartLevelDiscountId = line.custdisOffer;
            cartLevelDiscountType = true;
            var query = "select MO.* from m_offer MO where MO.m_offer_id='" + line.promotions[0].ruleId + "'";
            OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {
              if (offers.models[0].get('custdisTotalMinAmount') > afterReturnGross) {
                cartValueError = true;
                count++;
                callback();
              } else {
                count++;
                if (count === args.order.receiptLines.length) {
                  callback();
                }
              }
            }, null);
            if (cartValueError) {
              break;
            }
          } else {
            count++;
            if (count === args.order.receiptLines.length) {
              callback();
            }
          }
        }
      }
    }

    OB.UTIL.CUSTDISUtils.abcd(returnGross, function () {

      _.each(args.order.receiptLines, function (l) {
        if (l.custdisOffer === cartLevelDiscountId) {
          if (OB.UTIL.isNullOrUndefined(l.promotions) || l.promotions.length === 0) {
            cartLevelNGiftItem.push(l.lineId);
            if (returnUtils.selectedLineIds.includes(l.lineId)) {
              returnNGiftItem.push(l.lineId);
            }
          } else {
            if (returnUtils.selectedLineIds.includes(l.lineId)) {
              returnGiftItem.push(l.lineId);
            }
            cartLevelGiftItem.push(l.lineId);
          }
        }
      });

      j = 0;
      for (j = 0; j < returnUtils.selectedLineIds.length; j++) {
        var l = returnUtils.selectedLineIds[j];
        if (cartLevelNGiftItem.includes(l) || cartLevelGiftItem.includes(l)) {
          cartLevelDiscountType = true;
          if ((cartLevelNGiftItem.length <= returnNGiftItem.length || cartLevelGiftItem.includes(l)) && !(returnGiftItem.length >= cartLevelGiftItem.length && returnNGiftItem.length >= cartLevelNGiftItem.length)) {
            var info = [],
                promoLines = _.filter(args.order.receiptLines, function (pl) {
                return pl.custdisOffer === cartLevelDiscountId;
              });
            _.each(promoLines, function (lp) {
              if (info.length === 0) {
                info.push(OB.I18N.getLabel('CUSTDIS_ErrReturnOtherLines'));
              }
              info.push('- ' + lp.name + ' (' + OB.I18N.formatCurrency(lp.unitPrice) + ')');
            });
            args.cancellation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
            break;
          }
        }
      }


      if (!args.cancellation && cartValueError && (!retMainProductWithPromo) && !(returnGiftItem.length >= cartLevelGiftItem.length && returnNGiftItem.length >= cartLevelNGiftItem.length)) {

        _.each(returnUtils.mainProductLine, function (line) {
          cartValueError = true;
          args.cancellation = true;
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);

          var info = [],
              promoLines = null;
          promoLines = _.filter(args.order.receiptLines, function (l) {
            return l.custdisOrderline === line || l.originalOrderLineId === line || l.lineId === line;
          });

          _.each(promoLines, function (l) {
            if (info.length === 0) {
              info.push(OB.I18N.getLabel('CUSTDIS_ReturnCartLevelLines'));
              info.push(OB.I18N.getLabel('CUSTDIS_ErrReturnOtherLines'));
            }
            info.push('- ' + l.name + ' (' + OB.I18N.formatCurrency(l.unitPrice) + ')');
          });
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
        });
      }

      if (!args.cancellation) {
        if (cartValueError || (!retMainProductWithPromo)) {

          var indexArr = [];
          if (selectedProduct.length > 0) {
            for (var i = 0; i < returnUtils.mainProductLine.length; i++) {
              if (!selectedProduct.includes(returnUtils.mainProductLine[i])) {
                indexArr.push(returnUtils.mainProductLine[i]);
              }
            }
          }

          for (var i = 0; i < indexArr.length; i++) {
            var indexMP = returnUtils.mainProductLine.indexOf(indexArr[i]);
            if (indexMP > -1) {
              returnUtils.mainProductLine.splice(indexMP, 1);
            }
          }

          // Verify if return a complete promotion
          _.each(returnUtils.mainProductLine, function (line) {
            if (!returnUtils.promotionHaveAllProductIncluded(line)) {
              error = true;
              args.cancellation = true;
              var info = [],
                  promoLines = _.filter(args.order.receiptLines, function (l) {
                  return l.custdisOrderline === line || l.lineId === line;
                });
              _.each(promoLines, function (l) {
                if (info.length === 0) {
                  info.push(OB.I18N.getLabel('CUSTDIS_ErrReturnOtherLines'));
                }
                info.push('- ' + l.name + ' (' + OB.I18N.formatCurrency(l.unitPrice) + ')');
              });
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
            }
          });
        }

        if (!cartLevelDiscountType) {

          var indexArr = [];
          if (selectedProduct.length > 0) {
            for (var i = 0; i < returnUtils.mainProductLine.length; i++) {
              if (!selectedProduct.includes(returnUtils.mainProductLine[i])) {
                indexArr.push(returnUtils.mainProductLine[i]);
              }
            }
          }

          for (var i = 0; i < indexArr.length; i++) {
            var indexMP = returnUtils.mainProductLine.indexOf(indexArr[i]);
            if (indexMP > -1) {
              returnUtils.mainProductLine.splice(indexMP, 1);
            }
          }

          // Verify if return a complete promotion for other promotion types
          _.each(returnUtils.mainProductLine, function (line) {
            if (!returnUtils.promotionHaveAllProductIncluded(line)) {
              error = true;
              args.cancellation = true;
              var info = [],
                  promoLines = _.filter(args.order.receiptLines, function (l) {
                  return l.custdisOrderline === line || l.lineId === line;
                });
              _.each(promoLines, function (l) {
                if (info.length === 0) {
                  info.push(OB.I18N.getLabel('CUSTDIS_ErrReturnOtherLines'));
                }
                info.push('- ' + l.name + ' (' + OB.I18N.formatCurrency(l.unitPrice) + ')');
              });
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
            }
          });
        }

        if (!error) {
          // Set Credit Notes payment method as selected payment method.
          if (OB.MobileApp.model.paymentnames['GCNV_payment.creditnote']) {
            OB.MobileApp.model.receipt.set('selectedPayment', 'GCNV_payment.creditnote');
          }
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      }
    });

  });

}());
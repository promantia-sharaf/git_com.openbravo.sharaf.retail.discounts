/**
 *Promantia development
 */
(function () {
  OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {
    var promArr = [];
    var primaryLine = [],
        promProd = [],
        flag = false;
    args.selectedLines.forEach(function (line) {
      //choose both primary and secondary lines
      if ((!OB.UTIL.isNullOrUndefined(line.line.promotions) && (line.line.promotions.length > 0)) || !OB.UTIL.isNullOrUndefined(line.line.custdisOffer)) {
        promArr.push(line);
      }
      //choose only primary lines
      if (!OB.UTIL.isNullOrUndefined(line.line.custdisOffer) && OB.UTIL.isNullOrUndefined(line.line.custdisOrderline)) {
        primaryLine.push(line);
      }
      //choose only secondary lines
      if (!OB.UTIL.isNullOrUndefined(line.line.custdisOffer) && !OB.UTIL.isNullOrUndefined(line.line.custdisOrderline)) {
        promProd.push(line);
      }
    });
    
    
    //To check whether primary returned qty is same as secondary returned qty. If not assign flag as true
    if (!OB.UTIL.isNullOrUndefined(primaryLine.length) && !OB.UTIL.isNullOrUndefined(promProd.length)) {
      for (i = 0; i < primaryLine.length; i++) {
        for (j = 0; j < promProd.length; j++) {
          if (primaryLine[i].line.custdisOffer === promProd[j].line.custdisOffer) {
        	  if(primaryLine[i].line.deliveredQuantity === promProd[j].line.deliveredQuantity) {
        		  if (primaryLine[i].qty != promProd[j].qty) {
        			  flag = true;
        		  }
        	  } else {
        		  var secondary = promProd[j].line.deliveredQuantity;
        		  var primary = primaryLine[i].line.deliveredQuantity;
        		  var originalGiftQty = String(secondary/primary);
        		  if (promProd[j].qty != (String(primaryLine[i].qty) * originalGiftQty)) {
        			  flag = true;
        		  }
        	  }
          }
        }
      }
    }

    // Calculate discretionary discount and user defined amount at line level
    if (!OB.UTIL.isNullOrUndefined(promArr.length)) {
      for (i = 0; i < promArr.length; i++) {
        if (!OB.UTIL.isNullOrUndefined(promArr[i].line.promotions.length) && !OB.UTIL.isNullOrUndefined(promArr[i].qty)) {
          var line = promArr[i].line;
          var salesQty = promArr[i].line.quantity;
          var returnQty = promArr[i].qty;
          for (j = 0; j < line.promotions.length; j++) {
            if ((line.promotions[j].discountType === "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (line.promotions[j].discountType ==="D1D193305A6443B09B299259493B272A")
            		|| (line.promotions[j].discountType === "7B49D8CC4E084A75B7CB4D85A6A3A578") || (line.promotions[j].discountType === "8338556C0FBF45249512DB343FEFD280")
            		|| (line.promotions[j].discountType === "20E4EC27397344309A2185097392D964")) {
            	if (args.order.custsdtDocumenttypeSearchKey !== 'BS') {
            		var actualDiscount = (line.promotions[j].amt / salesQty) * returnQty;
            		line.promotions[j].amt = actualDiscount;
            		line.promotions[j].actualAmt = actualDiscount;
            	}
            }
          }
        }
      }
    }

    if (flag) {
      OB.UTIL.showWarning('Returned quantity should match for primary and secondary items');
    } else {
      //To make promotions same as qty selected for promotion products
      if (!OB.UTIL.isNullOrUndefined(promArr.length)) {
        for (i = 0; i < promArr.length; i++) {
          var line = promArr[i].line;
          var returnQty = promArr[i].qty;
          var count = 0;
          if (!OB.UTIL.isNullOrUndefined(line.promotions.length) && !OB.UTIL.isNullOrUndefined(returnQty)) {
            if (line.promotions.length > returnQty) {
              for (var j = 0; j < line.promotions.length; j++) {
                if (line.promotions.length > 0) {
                	if((line.promotions[j].discountType === "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (line.promotions[j].discountType ==="D1D193305A6443B09B299259493B272A")
                    		|| (line.promotions[j].discountType === "7B49D8CC4E084A75B7CB4D85A6A3A578") || (line.promotions[j].discountType === "8338556C0FBF45249512DB343FEFD280")
                    		|| (line.promotions[j].discountType === "20E4EC27397344309A2185097392D964")){
                		count++;
                	}
                }
              }
              for (var j = 0; j < line.promotions.length; j++) {
                if ((line.promotions[j].discountType !== "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (line.promotions[j].discountType !== "D1D193305A6443B09B299259493B272A")
                		|| (line.promotions[j].discountType !== "7B49D8CC4E084A75B7CB4D85A6A3A578") || (line.promotions[j].discountType !== "8338556C0FBF45249512DB343FEFD280")
                		|| (line.promotions[j].discountType !== "20E4EC27397344309A2185097392D964")) {
                  var loop = (line.promotions.length - count) - returnQty;
                  for (var k = loop; k > 0; k--) {
                    line.promotions.shift();
                  }
                }
              }
              for (var j = 0; j < line.promotions.length; j++) {
                  if (args.order.custsdtDocumenttypeSearchKey === 'BS' && ((line.promotions[j].discountType === "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (line.promotions[j].discountType === "D1D193305A6443B09B299259493B272A")
                  		|| (line.promotions[j].discountType === "7B49D8CC4E084A75B7CB4D85A6A3A578") || (line.promotions[j].discountType === "8338556C0FBF45249512DB343FEFD280")
                  		|| (line.promotions[j].discountType === "20E4EC27397344309A2185097392D964"))) {
                    var loop = line.promotions.length - returnQty;
                    for (var k = loop; k > 0; k--) {
                      line.promotions.shift();
                    }
                  }
               }
            }
          }
        }
      }
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());

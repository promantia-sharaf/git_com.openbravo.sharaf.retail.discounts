/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.order) ) {
        var isBigWinPromotionApplied = false;
        var isLinefocid = false;
        var focProductLineid;
        var isOriginalDocNoPresent = false;
        var offerid;
        var slotId;
        var me = args;
      args.order.get('lines').models.forEach(function (line) {
          if (!OB.UTIL.isNullOrUndefined(line.attributes.promotions)) {
            var promotion = line.attributes.promotions;
            promotion.forEach(function (promo) {
              if (!OB.UTIL.isNullOrUndefined(promo) && !OB.UTIL.isNullOrUndefined(promo.discountType)) {
            	 var discountType=promo.discountType;
            	  if(discountType =='399EF97C508B40E787A444CE3855A7E4'){
            		  isBigWinPromotionApplied = true;
            		   focProductLineid = line.id;
            		   offerid = promo.ruleId;
                       slotId = me.order.get('custDisBigWinnerPromotionSlotID');
            	  }
            	               
              } 
            });
          }
      });
      if(args.selectedLines.length>0){
    	  for(var i = 0; i < args.selectedLines.length; i++){
    		  var lineid = args.selectedLines[i].id;
    		  if(focProductLineid == lineid){
    			  isLinefocid = true;
    		  }if(!OB.UTIL.isNullOrUndefined(args.selectedLines[i].attributes.originalDocumentNo)){
    			  isOriginalDocNoPresent = true;
    		  }
    	  }
      }
      if(isBigWinPromotionApplied && (isLinefocid && !args.order.get('isVerifiedReturn'))){
    	 args.order.unset('custDisBigWinnerPromotion');
         args.order.unset('custDisBigWinnerPromotionApplied');
         args.order.unset('custDisBigWinnerPromotionSlotID');
         var process = new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.BigWinnerPromotionService');
                process.exec(
                  {
                    isVoid: true,
                    documentNo: OB.MobileApp.model.receipt.get('documentNo'),
                    mofferid: offerid,
                    slottimeid:slotId 
                  },
                  function(response) {}
                );
    	  
      }else if(isBigWinPromotionApplied && args.order.get('isVerifiedReturn')){
    	  if(args.selectedLines.length !== args.order.get('lines').length){
    		  if(isOriginalDocNoPresent){
    		  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Line(s) can not be deleted since Big Win Promotion added in the receipt");
              args.cancelOperation = true;
        	  }
          }
      }
      else if(isBigWinPromotionApplied && !(isLinefocid && args.order.get('isVerifiedReturn'))){
    	  if(args.selectedLines.length !== args.order.get('lines').length){
    	  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Line(s) can not be deleted since FOC Product added in the receipt,to delete line(s) first need to delete FOC product");
          args.cancelOperation = true;
    	  }
      }
      
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_preApplyDiscountsHook', function (args, callbacks) {
    var discountType = args.context.$.discountsContainer.model.get('discountType'),
        sharafDiscountTypes = ['CA5491E6000647BD889B8D7CDF680795', '6A3C2313136147A6B2D1B8D2F5F768E6'],
        sharafDiscount = _.find(sharafDiscountTypes, function (disc) {
        return disc === discountType;
      });

    if (sharafDiscount) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_ErrSharafPromotionNotApply'));
      args.cancelOperation = true;
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());
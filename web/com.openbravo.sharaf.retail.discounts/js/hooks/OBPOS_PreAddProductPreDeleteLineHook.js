/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

/*(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.receipt) && !OB.UTIL.isNullOrUndefined(args.receipt.get('isVerifiedReturn')) && args.receipt.get('isVerifiedReturn')) {
      var isBankBinPromotionApplied = false;
      var hasLineDiscountField = false;
      args.receipt.get('lines').models.forEach(function (line) {
    	  if(!OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))){
    		  hasLineDiscountField = true;
    	  }
          if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
            var originalPayments = line.get('originalPayments');
            originalPayments.forEach(function (orgPayment) {
              if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
              && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
               && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
                 isBankBinPromotionApplied = true;
               } 
            });
          }
      });
      
      if (isBankBinPromotionApplied && !hasLineDiscountField) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_NoNewProductsInReturn'));
        args.cancelOperation = true;
      }  
     }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());*/

/*(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.order) && !OB.UTIL.isNullOrUndefined(args.order.get('isVerifiedReturn')) && args.order.get('isVerifiedReturn')) {
      var isBankBinPromotionApplied = false;
      args.order.get('lines').models.forEach(function (line) {
          if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
            var originalPayments = line.get('originalPayments');
            originalPayments.forEach(function (orgPayment) {
              if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
              && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
               && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
                 isBankBinPromotionApplied = true;
               } 
            });
          }
      });
      
      if (isBankBinPromotionApplied) {
        if (args.selectedLines.length !== args.order.get('lines').length) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_NoPartialReturn'));
          args.cancelOperation = true;
        } else {
           args.order.get('payments').models.forEach(function (payment) {
             if (!OB.UTIL.isNullOrUndefined(payment.get('paymentData')) && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data) 
              && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data.custdisLinkedPaymentMethod)
               && payment.get('paymentData').data.custdisLinkedPaymentMethod) {
               payment.set('custdisAutomaticIRPaymentRemoval', true, {
                silent: true
               });
               args.order.removePayment(payment);              
             } 
           });
        }          
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());*/
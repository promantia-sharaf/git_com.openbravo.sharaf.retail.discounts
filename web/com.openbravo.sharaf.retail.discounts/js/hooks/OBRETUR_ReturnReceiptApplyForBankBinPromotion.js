/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */
OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function(args, callbacks) {
    args.allReturnLinesSelected = false;
    var isBankBinPromotionApplied = false;
    var linkedPayment;
    var likedPaymentAvailable = false;
    var linkedPaymentMethodName;
    var nonBankBinAutoSettle = false;
    var hasLineDiscountAmountField = false;
    var minPurchaseAmount;
    if(!OB.UTIL.isNullOrUndefined(args.order.custdisMindisamt)){
         minPurchaseAmount = args.order.custdisMindisamt;
    }
    _.each(args.order.receiptPayments, function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && !OB.UTIL.isNullOrUndefined(payment.paymentData.data) &&
            !OB.UTIL.isNullOrUndefined(payment.paymentData.data.custdisLinkedPaymentMethod) && payment.paymentData.data.custdisLinkedPaymentMethod) {
            isBankBinPromotionApplied = true;
            linkedPayment = payment;
            linkedPaymentMethodName = payment.name;
        }
    });
    var autoSettlePayments = [];
    if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('paymentData'))) {
      var paymentData = JSON.parse(localStorage.getItem('paymentData')).paymentData;
    _.each(args.order.receiptPayments, function(payment) {
        if (!OB.UTIL.isNullOrUndefined(payment.kind) && paymentData[payment.kind]) {
            autoSettlePayments.push(payment.name); 
            if (!OB.UTIL.isNullOrUndefined(payment.paymentData) && OB.UTIL.isNullOrUndefined(payment.paymentData.data) && (args.selectedLines.length !== args.order.receiptLines.length)) {
                nonBankBinAutoSettle = true;
            } 
        }      
    });
   }    
    if (isBankBinPromotionApplied) {
        if(nonBankBinAutoSettle) {
            let uniquePayments = [...new Set(autoSettlePayments)];
            args.cancellation = true;
            args.cancelOperation = true;
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
        }
        if (!OB.UTIL.isNullOrUndefined(linkedPayment)) {
            var count = 0;
            var totalPaymentMethods = 0;
            _.each(OB.MobileApp.model.paymentnames, function(payment) {
                totalPaymentMethods = totalPaymentMethods + 1;
            });
            var callback = function() {
                if (!likedPaymentAvailable) {
                    OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_OriginalLinkedPaymentNotConfigured', [linkedPaymentMethodName]));
                    args.cancellation = true;
                    args.cancelOperation = true;
                } else if (args.order.receiptLines.length !== args.selectedLines.length) {
                    var bankBinOfferLength = 0;
                    var count = 0;
                    for (var i = 0; i < args.order.receiptLines.length; i++) {
                        if (!OB.UTIL.isNullOrUndefined(args.order.receiptLines[i].custdisBankBinOffer)) {
                            count = count + 1;
                        }
                    }
                    for (var i = 0; i < args.selectedLines.length; i++) {
                        if (!OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisBankBinOffer)) {
                            bankBinOfferLength = bankBinOfferLength + 1;
                        }
                        if (!OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisLinedisamt) && !OB.UTIL.isNullOrUndefined(args.selectedLines[i].line.custdisBankBinOffer)) {
                        	hasLineDiscountAmountField = true;
                        }
                    }
                    if(!hasLineDiscountAmountField){
                    	var totalRemainingLineAmount = 0.0;
                    	if(totalRemainingLineAmount === 0.0){
                			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                            if (count !== bankBinOfferLength && bankBinOfferLength !== 0) {
                                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + "Bank Bin Offer present in the Original Invoice -No partial Return Allowed");
                                args.cancellation = true;
                            }
                    	}

                    }else{
                    	var linenetamount = new Array(); 
                    	var taxAmount = new Array();
                    	var lineid = new Array();
                    	var totalRemainingLineAmount = 0.0;
                    	var totalLineAmount = 0.0; 
                    	var totalTaxAmount = 0.0;
                    	for (var i = 0; i < args.order.receiptLines.length; i++) {
                    	lineid = args.selectedLines.filter((item) => item.line.lineId === args.order.receiptLines[i].lineId);
                    	if(hasLineDiscountAmountField){
                    			if(!OB.UTIL.isNullOrUndefined(args.order.receiptLines[i].custdisBankBinOffer) && lineid.length == 0 && args.order.receiptLines[i].remainingQuantity !== 0){
                        			 linenetamount[i] = args.order.receiptLines[i].linenetamount;
                        			 totalLineAmount += linenetamount[i];
                        			 for(var k=0; k < args.order.receiptLines[i].taxes.length; k++){
                        				 taxAmount[k] = args.order.receiptLines[i].taxes[k].taxAmount;
                            			 totalTaxAmount += taxAmount[k]; 
                        			 }
                        			
                        			 totalRemainingLineAmount = totalLineAmount + totalTaxAmount;
                            	}
                    		
                    	}
                    	}
                		if(totalRemainingLineAmount === 0.0 || totalRemainingLineAmount >= minPurchaseAmount){
                			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                		}else{
                            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_bankbinreturn'));
                            args.cancellation = true;
                		}
                    }
                } else {
                	var totalRemainingLineAmount = 0.0;
                	if(totalRemainingLineAmount === 0.0){
            			OB.MobileApp.model.receipt.set("isAllLinesFullReturn", (totalRemainingLineAmount === 0.0)? true:false );
                      }
                    args.allReturnLinesSelected = true;
                    args.cancellation = false;
                    args.cancelOperation = false;
                }
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
            _.each(OB.MobileApp.model.paymentnames, function(payment) {
                count = count + 1;
                if (payment.paymentMethod.searchKey === linkedPayment.kind) {
                    likedPaymentAvailable = true;
                }
                if (count === totalPaymentMethods) {
                    callback();
                }
            });
        } else {
            let uniquePayments = [...new Set(autoSettlePayments)];
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "" + [uniquePayments] + " present in the Original Invoice -No partial Return Allowed");
            args.cancellation = true;
            args.cancelOperation = true;
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
});                                                                                                                            
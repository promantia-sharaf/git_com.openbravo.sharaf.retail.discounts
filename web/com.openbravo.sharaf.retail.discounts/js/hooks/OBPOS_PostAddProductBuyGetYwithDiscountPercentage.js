
 
(function () {
 OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function (args, callbacks) {
	 var lineidsOfBuyYGetX = [];
	 var alredyBundlePromoAppliedForSameProd = false;
    if (args.options && (args.options.isVerifiedReturn || OB.UTIL.isNullOrUndefined(args.options.blockAddProduct))) {
      if ((!OB.UTIL.isNullOrUndefined(args.options.line) && !OB.UTIL.isNullOrUndefined(args.options.line.get('isReturnsNegative')) && args.options.line.get('isReturnsNegative'))) {
        //do nothing...
        //If isReturnsNegative flag is true: Not verified returns flow should be execute.
      } else {
        var line = args.orderline;
        args.receipt.attributes.isVerifiedReturn = args.options.isVerifiedReturn;
         if(args.receipt.attributes.isVerifiedReturn) { 
            if (line.get('custdisOffer') && line.get('custdisOrderline')) {
                
               var promoLine = _.find(args.receipt.get('lines').models, function (l) {
                    return l.id === line.get('custdisOrderline') &&
                                    !OB.UTIL.isNullOrUndefined(line.get('promotions')) 
                                    && line.get('promotions').length > 0 
                                    && line.get('promotions')[0].discountType === '9AC09641F86C46C4AF7BC8630325B26C';
                  }); 
                 if (promoLine) {
                     if(OB.UTIL.isNullOrUndefined(promoLine.get('custdisOffer'))) {
                         promoLine.set('custdisOffer', line.get('custdisOffer'), {
                             silent: true
                        });
                     }  
                promoLine.set('IsbuyXGetYpercen','P');
                line.set('IsbuyXGetYpercen','G');  
                 }    
            }      
         }  else if (line.get('custdisOffer') && line.get('custdisOrderline')) {
          var promoLine = _.find(args.receipt.get('lines').models, function (l) {
            return l.id === line.get('custdisOrderline') && 
                            !OB.UTIL.isNullOrUndefined(line.get('promotions'))
                             && line.get('promotions').length > 0
                             && line.get('promotions')[0].discountType === '9AC09641F86C46C4AF7BC8630325B26C';
          });
          if (promoLine) {
            promoLine.set('IsbuyXGetYpercen','P');
            line.set('IsbuyXGetYpercen','G');
          }
          }
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }  
        return;
      }
    
    
   if (args.orderline) {  
       var query = "select distinct O.* from m_offer_product OF join m_offer O on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id left join m_offer_prod_cat pc on pc.m_offer_id = O.m_offer_id " //
      + " where O.m_offer_type_id in ('9AC09641F86C46C4AF7BC8630325B26C') " //
      + " and OF.m_product_id = '" + args.productToAdd.id //
      + "' and ((pc.m_product_category_id = '" + args.productToAdd.get('productCategory') + "'" + " and O.prod_cat_selection = 'N') or " + "(O.prod_cat_selection = 'Y' and (select count(*) from m_offer_prod_cat where m_product_category_id = '" + args.productToAdd.get('productCategory') + "' and m_offer_id = O.m_offer_id" + ") = 0)) " // Prod Cat compar ison
      + " and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.receipt.get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
      + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') 
      + "' and (O.dateto is null or O.dateto >= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "') " 
     // + OB.CUSTDIS.Utils.addAvailabilityRule(moment())
      + " order by O.name"; 
    
     OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {  
        if (offers.length === 0) {
           OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else  { 
           for(var j=0;j<offers.models.length;j++) { 
               promotion = offers.models[j];
              if (promotion) { 
                OB.CUSTDIS.Utils.findPromotionProductList(args.orderline, {  
                    rule: promotion
                }, function (productlist) {	         
                     var primaryProductFilter = [];
                     var giftProductFilter = [];
                     var orderLines = [];
                         
                      var primaryProductFilter = _.filter(productlist.models, function(line) {
                         return  !line.get('custdisIsGift');
                      });
                       
                       var giftProductFilter = _.filter(productlist.models, function(line) {
                         return  line.get('custdisIsGift');
                      });
                                      
                    var primaryLines = [];
                    var giftLines = [];
                  
                    for(var j=0; j< OB.MobileApp.model.receipt.get('lines').models.length ;j++) {
                      var line = OB.MobileApp.model.receipt.get('lines').models[j];
                      for(var k=0; k< primaryProductFilter.length;k++) {
                        var pline = primaryProductFilter[k];
                        if(pline.get('product') === line.get('product').id) {
                          primaryLines.push(line);
                        }
                      }
                      for(var k=0; k< giftProductFilter.length;k++) {
                        var pline = giftProductFilter[k];
                        if(pline.get('product') === line.get('product').id) {
                          giftLines.push(line);
                        }
                      }
                    }
                    
                    
                    var PresentofferArticle = [];
                    var isDiscountCalculatedForGift = false;
                    for(var i=0; i< giftLines.length;i++){
                    	var gline = giftLines[i];
                    	if(OB.UTIL.isNullOrUndefined(giftLines[i].attributes.custdisAmount)){
                    		PresentofferArticle.push(gline);
                    		isDiscountCalculatedForGift = true;
                    	}
                    	}
                   
                  while (primaryLines.length >0 && giftLines.length > 0 && isDiscountCalculatedForGift) {
                       var primaryArticle = [];
                        for(j=0;j<primaryProductFilter.length;j++) {
                          var line = primaryProductFilter[j];
                          if(line.get('product')  === primaryLines[0].get('product').id) {
                            primaryArticle.push(line);
                          }
                        }
                    var offerArticle = [];               
                        for(j=0;j<giftProductFilter.length;j++) {
                            var line = giftProductFilter[j];
                            if(PresentofferArticle.length > 0){
                                if(line.get('product')  === PresentofferArticle[0].get('product').id) {
                                    offerArticle.push(line);
                                  }
                            }
                          }
                   
                    giftLines = [];   
                    primaryLines = [];
                   for(var j=0; j< OB.MobileApp.model.receipt.get('lines').models.length ;j++) {
                      var line = OB.MobileApp.model.receipt.get('lines').models[j];
                      for(var k=0; k< primaryProductFilter.length;k++) {
                        var pline = primaryProductFilter[k];
                        if(pline.get('product') === line.get('product').id 
                           && (OB.UTIL.isNullOrUndefined(line.get('IsbuyXGetYpercen')))) {
                          primaryLines.push(line);
                        }
                      }
                      for(var k=0; k< giftProductFilter.length;k++) {
                        var pline = giftProductFilter[k];
                        if( pline.get('product') === line.get('product').id 
                            && (OB.UTIL.isNullOrUndefined(line.get('IsbuyXGetYpercen')))) {
                          giftLines.push(line);
                        }
                      }
                    }   
                   
                        if(primaryLines.length >0 && giftLines.length > 0) {
                          OB.CUSTDIS.Utils.setLineDiscount(PresentofferArticle[0], offerArticle[0]);
                         var amt = OB.CUSTDIS.Utils.calculateLineDiscount(PresentofferArticle[0].get('product').get('standardPrice'), offerArticle[0]);
                          OB.Model.Discounts.addManualPromotion(args.receipt, [PresentofferArticle[0]], {
                                rule: promotion,
                                definition: {}
                          });  
                         var promo = PresentofferArticle[0].get('promotions')[0];
                         PresentofferArticle[0].get('promotions')[0].amt = amt;
                         PresentofferArticle[0].get('promotions')[0].userAmt = amt;
                         PresentofferArticle[0].get('promotions')[0].rule.set('productsLoaded',true);
                         PresentofferArticle[0].set('custdisOffer', promo.rule.id);
                         PresentofferArticle[0].set('custdisOrderline', primaryLines[0].id);
                         primaryLines[0].set('IsbuyXGetYpercen', 'P');
                         PresentofferArticle[0].set('IsbuyXGetYpercen', 'G');
                         args.receipt.save();
                        OB.MobileApp.model.receipt.get('lines').reset(
                              OB.MobileApp.model.receipt.get('lines').sortBy(function(line) {
                                return (!OB.UTIL.isNullOrUndefined(line.get('IsbuyXGetYpercen')) &&
                                  line.get('IsbuyXGetYpercen') === 'G') ;
                                  
                              })
                         );                  
                        }
                  }  
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
               },function () {
                   OB.UTIL.HookManager.callbackExecutor(args, callbacks);
               });   
              }      
           }
        } 
     });
   } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
   } 
   });   
}());    
/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function(args, callbacks) {
        var me = args;
        var promotionId = null;
        var lineCount = 0;
        var lines = args.context.get('order').get('lines');
        var promotionSelected = false;
        if (!OB.UTIL.isNullOrUndefined(me.context) && !OB.UTIL.isNullOrUndefined(me.context.get('order')) && OB.UTIL.isNullOrUndefined(me.context.get('order').get('isVerifiedReturn'))
            && (!OB.UTIL.isNullOrUndefined(args.context.get('order').get('isQuotation')) && !args.context.get('order').get('isQuotation'))) {
            if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.Alloweddoctype_bigwin) && 
            OB.MobileApp.model.attributes.permissions.Alloweddoctype_bigwin.includes(args.context.get('order').get('custsdtDocumenttypeSearchKey').trim())) {
             checkLine(args, lines, lineCount, promotionSelected, callbacks);
            } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
        } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
          });
   }());
           var checkLine = function (args, lines, lineCount, promotionSelected, callbacks) {     
            if (lineCount < lines.models.length && !promotionSelected) {
                var line = lines.models[lineCount];
                var query = "select distinct O.* from m_offer O left join m_offer_product OF on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id left join OfferBrandConfig ob on ob.promotionDiscountId = O.m_offer_id left join OfferPaymentMethodConfig om on om.promotionDiscountId = O.m_offer_id left join m_offer_prod_cat pc on pc.m_offer_id = O.m_offer_id" 
                    + " where O.m_offer_type_id in ('399EF97C508B40E787A444CE3855A7E4') " //
                    + "and O.em_custdis_min_purchase_amount <= '" + args.context.get('order').get('gross') + "'"
                    // + "and ((OF.m_product_id = '" + line.get('product').get('id') + "'" + " and O.product_selection = 'N') or " + "(O.product_selection = 'Y' and (select count(*) from m_offer_product where m_product_id = '" + line.get('product').get('id') + "' and m_offer_id = O.m_offer_id" + ") = 0 )) " // Product comparison
                    + "and ((ob.brandId = '" + line.get('product').get('brand') + "'" + " and O.em_custdis_brand_selection = 'N') or " + "(O.em_custdis_brand_selection = 'Y' and (select count(*) from OfferBrandConfig where brandId = '" + line.get('product').get('brand') + "' and promotionDiscountId = O.m_offer_id" + ") = 0))" // Brand comparison
                    + "and ((pc.m_product_category_id = '" + line.get('product').get('productCategory') + "'" + " and O.prod_cat_selection = 'N') or " + "(O.prod_cat_selection = 'Y' and (select count(*) from m_offer_prod_cat where m_product_category_id = '" + line.get('product').get('productCategory') + "' and m_offer_id = O.m_offer_id" + ") = 0)) " // Prod Cat comparison
                    + "and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.context.get('order').get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
                    + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD')
                    + "' and (O.dateto is null or O.dateto >= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "')"
                    + OB.CUSTDIS.Utils.addAvailabilityRule(moment())
                    + " order by O.name";
                OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function(offers) {
                        var offerCount = 0;
                        checkoffer(args, lines, offers, offerCount, lineCount, promotionSelected, callbacks);
                });
            } else {
               OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
            }
            };

                      var checkoffer = function (args, lines, offers, offerCount, lineCount, promotionSelected, callbacks) { 
                       if (offerCount < offers.models.length && !promotionSelected)  {
                            var offer = offers.models[offerCount];
                            currentoffer = offer;
                            promotionId = offer.id;
                            var query1 = "select st.id,st.startTime,st.eNDTime from BigWinSlotTime st where st.offer='" + promotionId + "'";
                            OB.Dal.queryUsingCache(OB.Model.BigWinSlotTime, query1, [], function(slottimedata) {
                                var slotimeCount = 0;
                                checkSlot(args, slottimedata, slotimeCount, offerCount, offers, lines, lineCount, promotionSelected, callbacks);
                            });
                        } else {
                            lineCount++;
                            checkLine(args, lines, lineCount, promotionSelected, callbacks);
                        }
                        };
                                 var checkSlot = function (args, slottimedata, slotimeCount, offerCount, offers, lines, lineCount, promotionSelected, callbacks) {
                                   if (slotimeCount < slottimedata.models.length && !promotionSelected)  {
                                        var slot = slottimedata.models[slotimeCount];
                                        var currenttime = new Date();
                                        var formattedStartTime = new Date(); 
                                        var formattedEndTime = new Date();
                                        currenttime.setHours(new Date(moment()).getHours(), new Date(moment()).getMinutes(), new Date(moment()).getSeconds());
                                        var startTime = null;
                                        var endTime= null;
                                        var slotId = null;

                                        if((!OB.UTIL.isNullOrUndefined(slot.get('StartTime')))) {
                                            startTime = slot.get('StartTime');
                                            formattedStartTime.setHours(new Date(startTime).getUTCHours(), new Date(startTime).getUTCMinutes(), new Date(startTime).getUTCSeconds());
                                        }
                                        if ((!OB.UTIL.isNullOrUndefined(slot.get('EndTime')))) {
                                            endTime = slot.get('EndTime');
                                            formattedEndTime.setHours(new Date(endTime).getUTCHours(), new Date(endTime).getUTCMinutes(), new Date(endTime).getUTCSeconds());
                                        }
                                        if ((!OB.UTIL.isNullOrUndefined(slot.get('id')))) {
                                            slotId = slot.get('id');
                                        }
                                      if (!OB.UTIL.isNullOrUndefined(startTime) && !OB.UTIL.isNullOrUndefined(endTime) && (currenttime > formattedStartTime 
                                      && currenttime < formattedEndTime) &&  !args.context.get('order').get("custDisBigWinnerPromotionApplied")) {
                                       promotionSelected = true;
                                       new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.BigWinnerPromotionService').exec({
                                            mofferid: promotionId,
                                            documentNo: args.context.get('order').get('documentNo'),
                                            slottimeid: slotId,
                                            phoneNumber: OB.MobileApp.model.receipt.get('custshaCustomerPhone') !=='' ? OB.MobileApp.model.receipt.get('custshaCustomerPhone') : OB.MobileApp.model.receipt.get('bp').get('phone')
                                        }, function(promodata) {
                                               if (promodata.winnerselected) {
                                                   if (!OB.UTIL.isNullOrUndefined(promodata.focProduct)) {
                                                      var query = "SELECT prd.* from m_product prd where prd.m_product_id ='" + promodata.focProduct + "'";
                                                       OB.Dal.queryUsingCache(OB.Model.Product, query, [], function(product) {
                                                           if (product.models.length > 0) {
                                                               args.context.get('order').set('custDisBigWinnerPromotion', currentoffer);
                                                               args.context.get('order').set("custDisBigWinnerPromotionSlotID",slotId);
                                                               product.models[0].set('custDisBigWinnerFOCProduct', true);
                                                               OB.MobileApp.model.receipt.attributes.FocProduct = product.models[0];
                                                               OB.MobileApp.view.waterfallDown('onShowPopup', {
                                                                   popup: 'OB_UI_ModalBigWinnerDeclaration',
                                                                   args: {
                                                                       args: args,
                                                                       slotIdData:slotId, 
                                                                       callbacks: callbacks,
                                                                   }
                                                               });
                                                           } 
                                                       }, null);
                                                     return;
                                                   }  else {
                                                   slotimeCount++;
                                                   checkSlot(args, slottimedata, slotimeCount, offerCount, offers, lines, lineCount, promotionSelected, callbacks);
                                                    }
                                               } else {
                                                   slotimeCount++;
                                                   checkSlot(args, slottimedata, slotimeCount, offerCount, offers, lines, lineCount, promotionSelected, callbacks);
                                            }
                                        });
                                        } else {
                                           slotimeCount++;
                                           checkSlot(args, slottimedata, slotimeCount, offerCount, offers, lines, lineCount, promotionSelected, callbacks);
                                        }
                                    } else {
                                       offerCount++; 
                                       checkoffer(args, lines, offers, offerCount, lineCount, promotionSelected, callbacks);
                                    }
                                    };

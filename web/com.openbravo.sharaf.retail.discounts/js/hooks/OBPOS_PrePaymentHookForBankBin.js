/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
 
(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args.context) && !OB.UTIL.isNullOrUndefined(args.context.get('order')) && !OB.UTIL.isNullOrUndefined(args.context.get('order').get('isVerifiedReturn')) && args.context.get('order').get('isVerifiedReturn')) {
      var isBankBinPromotionApplied = false;
      var linkedPayment;
      var amount;
      var bankBinOfferLinesPresent = false;
      var returnableLinesDiscountAmount = 0.0;
      var hasLineDiscountField = false;
      args.context.get('order').get('lines').models.forEach(function (line) {    	  
          if(!OB.UTIL.isNullOrUndefined(line.get('custdisLinedisamt'))){
        	  hasLineDiscountField = true;
        	  returnableLinesDiscountAmount += line.get('custdisLinedisamt');
        	  if(OB.MobileApp.model.get('currency').standardPrecision > 2){
        		  returnableLinesDiscountAmount = +returnableLinesDiscountAmount.toFixed(3);
        	  }else{
        		  //returnableLinesDiscountAmount = Math.floor(returnableLinesDiscountAmount);
        		  returnableLinesDiscountAmount = +returnableLinesDiscountAmount.toFixed(2);
        	  }
          }
          if (!OB.UTIL.isNullOrUndefined(line.get('originalPayments'))) {
            var originalPayments = line.get('originalPayments');
            originalPayments.forEach(function (orgPayment) {
              if (!OB.UTIL.isNullOrUndefined(orgPayment.paymentData) && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data) 
              && !OB.UTIL.isNullOrUndefined(orgPayment.paymentData.data.custdisLinkedPaymentMethod)
               && orgPayment.paymentData.data.custdisLinkedPaymentMethod) {
                 isBankBinPromotionApplied = true;
                 if(!hasLineDiscountField){
                     amount = orgPayment.amount;
                 }else{
                	 amount = returnableLinesDiscountAmount;
                 }
                 linkedPayment = orgPayment;
               } 
            });
          }
      });
      
      if (isBankBinPromotionApplied) {
        if (!OB.UTIL.isNullOrUndefined(linkedPayment)) {
          _.each(OB.MobileApp.model.paymentnames, function(payment) {
            if (payment.paymentMethod.searchKey === linkedPayment.kind) {
               linkedPayment = payment;
            }
          });
        }
        
      args.context.get('order').get('lines').models.forEach(function (line) {
          if (!OB.UTIL.isNullOrUndefined(line.get('custdisBankBinOffer'))) {
            bankBinOfferLinesPresent = true;
          }
      });        
        
        var paymentAlreadyAdded = false;
        _.each(args.context.get('order').get('payments').models, function(payment) {
          if (payment.get('kind') === linkedPayment.paymentMethod.searchKey && !OB.UTIL.isNullOrUndefined(payment.get('paymentData')) 
              && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data) && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data.custdisLinkedPaymentMethod)
              && payment.get('paymentData').data.custdisLinkedPaymentMethod) {
            paymentAlreadyAdded = true;
          } else if (payment.get('kind') === linkedPayment.paymentMethod.searchKey && payment.get('amount') === amount) {
            paymentAlreadyAdded = true;
            var paymentData = new Object();
            var data = new Object();
            data.custdisLinkedPaymentMethod = true;
            paymentData.data = data;
            payment.set('paymentData', paymentData);
          }
          
        });
        if (!paymentAlreadyAdded && bankBinOfferLinesPresent) {
          args.context.get('order').addPayment(new OB.Model.PaymentLine({
            'kind': linkedPayment.paymentMethod.searchKey,
            'name': linkedPayment.paymentMethod.name,
            'amount': amount,
            'allowOpenDrawer': linkedPayment.paymentMethod.allowopendrawer,
            'isCash': linkedPayment.paymentMethod.iscash,
            'openDrawer': linkedPayment.paymentMethod.openDrawer,
            'printtwice': linkedPayment.paymentMethod.printtwice,
            'paymentData': {
                  data: {
                    custdisLinkedPaymentMethod: true
                  }
             }
          }));
        }                   
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());
            
/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function() {

    OB.UTIL.HookManager.registerHook('OBRETUR_postAddVerifiedReturnLines', function(args, callbacks) {
        var mainProductLine;
        args.receipt.calculateReceipt(function() {
            _.each(args.receipt.get('lines').models, function(line) {
                if (!OB.UTIL.isNullOrUndefined(line.get('custdisIrType')) && mainProductLine.id === line.get('custdisOrderline')) {
                    var searhKey = line.get('custdisIrType') === 'IRA' ? 'CUSTSPM_payment.instantA' : 'CUSTSPM_payment.instantB',
                        paymentMethod = OB.CUSTDIS.Utils.getPaymentMethod(searhKey, 'WebPOS Instant Redemption ' + (line.get('custdisIrType') === 'IRA' ? 'A' : 'B')),
                        paymentAlreadyExists = _.find(args.receipt.get('payments').models, function(payment) {
                            return payment.get('paymentData') && payment.get('paymentData').lineId === line.get('id');
                        });
                    if (paymentMethod && !paymentAlreadyExists) {
                        // Add Instant Redemption payment
                        for (var i = 0; i < line.get('selectedQuantity'); i++) {
                            OB.CUSTDIS.Utils.addReceiptPayment(searhKey, paymentMethod, line.get('custdisRedemptionAmount'), args.receipt, mainProductLine ? mainProductLine : line);
                        }
                    }
                } else {
                    mainProductLine = line;
                }
            });
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        });
    });

}());
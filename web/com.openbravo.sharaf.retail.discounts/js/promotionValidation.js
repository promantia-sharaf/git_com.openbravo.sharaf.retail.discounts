OB.PROMOTION = {
    PROMOTIONVALIDATION: function promotionValidation(orderid, callback) {
        new OB.DS.Process('org.openbravo.retail.posterminal.PaidReceipts').exec({
            orderid: orderid
        }, function(data) {
            if (data && data[0]) {
                var promotionid = [];
                var count = 0;
                lineslength = data[0].receiptLines.length;
                _.each(data[0].receiptLines, function(lines) {
                    var promocount = 0;
                    var promotionlength = lines.promotions.length;
                    if (promotionlength != 0) {
                        _.each(lines.promotions, function(promo) {
                            if (!OB.UTIL.isNullOrUndefined(promo)) {
                                promotionid = promo.ruleId;
                                var query = "select O.dateto,O.name,O.em_custdis_sellable_limit from m_offer O where O.m_offer_id='" + promotionid + "'";
                                OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function(data) {
                                    var businessdate = moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD');
                                    var enddate = null;
                                    var promotionname = null;
                                    if (data) {     
                                        if(data.length == 0) {
                                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_PromotionNotLoaded'),
                                        [
                                            {
                                              label: OB.I18N.getLabel('OBMOBC_LblOk'),
                                              isConfirmButton: true,
                                              action: function() {
                                              if(OB.UTIL.localStorage.getItem('POSLastTotalRefresh')) {
                                                OB.UTIL.localStorage.removeItem('POSLastTotalRefresh');
                                              }  
                                              if(OB.UTIL.localStorage.getItem('POSLastIncRefresh')) {
                                                OB.UTIL.localStorage.removeItem('POSLastIncRefresh'); 
                                              }                                                
                                                window.location.reload(); 
                                              }    
                                            },
                                            {
                                              label: OB.I18N.getLabel('OBMOBC_LblCancel')
                                            }
                                          ]);   
                                        } else {
                                            enddate = data.models[0].get('endingDate');
                                            promotionname = data.models[0].get('name');
                                            if(!OB.UTIL.isNullOrUndefined(enddate)) {
                                            if (enddate >= businessdate) {
                                                if (!OB.UTIL.isNullOrUndefined(data.models[0].get('custdisSellableLimit'))) {
                                                    new OB.DS.Process('com.openbravo.sharaf.retail.discounts.service.PromotionLimitService').exec({
                                                        mofferid: promotionid,
                                                        client: OB.MobileApp.model.get('terminal').client
                                                    }, function(promodata) {
                                                        if (OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('promoadded'))) {
                                                            var map = new Map();
                                                            OB.MobileApp.model.receipt.attributes.promoadded = map;
                                                        }
                                                         var cLimit =0;
                                                         if(OB.UTIL.isNullOrUndefined(lines.custdisOrderline)) {
                                                         cLimit = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('promoadded').get(promotionid))
                                                         ? (OB.MobileApp.model.receipt.get('promoadded').get(promotionid) + lines.quantity) : lines.quantity); 
                                                         }                                                                                           
                                                        if (promodata && promodata.remainingqty >= cLimit) { 
                                                            if(OB.UTIL.isNullOrUndefined(lines.custdisOrderline)) {
                                                             OB.MobileApp.model.receipt.get('promoadded').set(promotionid, cLimit);  
                                                            }                                                        
                                                            promocount = promocount + 1;
                                                            if (promocount == promotionlength)
                                                                count = count + 1;
                                                            if (count == lineslength)
                                                                callback(true);
                                                        } else {
                                                            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_Promotion') + ' ' + promotionname + ' ' + OB.I18N.getLabel('CUSTDIS_PromotionLimitExceeded'));
                                                            callback(false)
                                                        }
                                                    });
                                                } else {
                                                    promocount = promocount + 1;
                                                    if (promocount == promotionlength)
                                                        count = count + 1;
                                                    if (count == lineslength)
                                                        callback(true);
                                                }

                                            } else {
                                                OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTDIS_Promotion') + ' ' + promotionname + ' ' + OB.I18N.getLabel('CUSTDIS_PromotionDateExpired'));
                                                callback(false);
                                            }
                                       } else {
                                        promocount = promocount + 1;
                                        if (promocount == promotionlength)
                                          count = count + 1;
                                        if (count == lineslength)
                                          callback(true);
                                        }
                                      }
                                    }
                                });
                            }
                            if (promocount === promotionlength)
                                count = count + 1;
                        });
                    } else {
                        if (promocount === promotionlength)
                            count = count + 1;
                    }
                    if (count === lineslength) {
                        callback(true);
                    }
                });
            }
        });
    }
};











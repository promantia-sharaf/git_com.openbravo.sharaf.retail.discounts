/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var slottime = OB.Data.ExtensibleModel.extend({
    modelName: 'BigWinSlotTime',
    tableName: 'BigWinSlotTime',
    entityName: 'BigWinSlotTime',
    source:
      'com.openbravo.sharaf.retail.discounts.service.BigWinSlotTimeModel'
  });
  slottime.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
   {
      name: 'StartTime',
      column: 'startTime',
      primaryKey: false,
      type: 'TEXT'
    },
    {
      name: 'EndTime',
      column: 'eNDTime',
      type: 'TEXT'
    },
    {
        name: 'client',
        column: 'client',
        type: 'TEXT'
      },
    {
    	name: 'm_offer_id',
    	column: 'offer',
        type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(slottime);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(slottime);
})();

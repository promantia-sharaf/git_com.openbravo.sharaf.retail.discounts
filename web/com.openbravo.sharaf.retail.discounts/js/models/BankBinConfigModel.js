/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var bbConfig = OB.Data.ExtensibleModel.extend({
    modelName: 'BankBinConfig',
    tableName: 'BankBinConfig',
    entityName: 'BankBinConfig',
    source:
      'com.openbravo.sharaf.retail.discounts.service.BankBinConfigModel'
  });
  bbConfig.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'orgId',
      column: 'orgId',
      primaryKey: false,
      type: 'TEXT'
    },
    {
      name: 'clientId',
      column: 'clientId',
      type: 'TEXT'
    },
    {
      name: 'groupName',
      column: 'groupName',
      type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(bbConfig);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(bbConfig);
})();

/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var bbConfig = OB.Data.ExtensibleModel.extend({
    modelName: 'BankBinNumberConfig',
    tableName: 'BankBinNumberConfig',
    entityName: 'BankBinNumberConfig',
    source:
      'com.openbravo.sharaf.retail.discounts.service.BankBinNumberConfigModel'
  });
  bbConfig.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'orgId',
      column: 'orgId',
      primaryKey: false,
      type: 'TEXT'
    },
    {
      name: 'clientId',
      column: 'clientId',
      type: 'TEXT'
    },
    {
      name: 'lineNo',
      column: 'lineNo',
      type: 'TEXT'
    },
    {
      name: 'bankBinId',
      column: 'bankBinId',
      type: 'TEXT'
    },
    {
      name: 'bankBinNumbers',
      column: 'bankBinNumbers',
      type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(bbConfig);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(bbConfig);
})();

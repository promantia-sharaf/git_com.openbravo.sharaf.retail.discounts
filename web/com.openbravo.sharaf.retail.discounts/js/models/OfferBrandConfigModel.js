/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var config = OB.Data.ExtensibleModel.extend({
    modelName: 'OfferBrandConfig',
    tableName: 'OfferBrandConfig',
    entityName: 'OfferBrandConfig',
    source:
      'com.openbravo.sharaf.retail.discounts.service.OfferBrandConfigModel'
  });
  config.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'orgId',
      column: 'orgId',
      primaryKey: false,
      type: 'TEXT'
    },
    {
      name: 'clientId',
      column: 'clientId',
      type: 'TEXT'
    },
    {
      name: 'promotionDiscountId',
      column: 'promotionDiscountId',
      type: 'TEXT'
    },
    {
      name: 'brandId',
      column: 'brandId',
      type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(config);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(config);
})();

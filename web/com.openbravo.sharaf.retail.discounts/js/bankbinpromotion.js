/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global moment*/

OB.BANKBIN = {
  
  CHECKANDAPPLY : function checkAndApply(args, callbacks) {
      var paymentLineLength = args.order.get('payments').models.length;
      if (paymentLineLength === 0) {
        OB.BANKBIN.PROMOTION(args, callbacks);
      } else {
        var currentModel = 0;
        _.each(args.order.get('payments').models, function(payment) {
          currentModel = currentModel + 1;
          if (!OB.UTIL.isNullOrUndefined(payment.get('paymentData')) && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data) 
               && !OB.UTIL.isNullOrUndefined(payment.get('paymentData').data.custdisLinkedPaymentMethod) && payment.get('paymentData').data.custdisLinkedPaymentMethod) {
             args.order.set('custdisBankBinDiscountApplied', false); 
             args.order.unset('custdisLinkedPaymentMethod');
             args.order.unset('custDisBankBinPromotion');
             args.order.unset('custdisBankBinDiscountApplied');
             args.order.unset('custdisCardBinNumber');
             args.order.unset('custdisMaxDiscountAmt');
             args.order.unset('custdisDiscountPercentage');
             args.order.unset('custdisMinPurchaseAmountBankBin');
             args.order.unset('custdisSelectedLineAmount');
             // Loyalty Discount Promotion
             
             args.order.set('custdisLoyaltyDiscountApplied', false); 
             args.order.unset('custDisLoyaltyDiscountPromotion');
             args.order.unset('custdisLoyaltyDiscountApplied');
             args.order.unset('custdisLoyaltyCardNumber');
             args.order.unset('custdisLoyaltyMonthValidity');
             args.order.unset('custdisLoyaltyYearValidity');
             
            // OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            // return;
          }
          if (currentModel === paymentLineLength) {
            OB.BANKBIN.PROMOTION(args, callbacks);
          }
        });
      }
  },
  PROMOTION : function bankBinPromotion(args, callbacks) {      
      args.order.set('custdisBankBinDiscountApplied', false); 
      args.order.unset('custdisLinkedPaymentMethod');
      args.order.unset('custDisBankBinPromotion');
      args.order.unset('custdisBankBinDiscountApplied');
      args.order.unset('custdisCardBinNumber');
      args.order.unset('custdisMaxDiscountAmt');
      args.order.unset('custdisDiscountPercentage');
      args.order.unset('custdisMinPurchaseAmountBankBin');
      args.order.unset('custdisSelectedLineAmount');
    
      // Loyalty Discount Promotion
      
      args.order.set('custdisLoyaltyDiscountApplied', false); 
      args.order.unset('custDisLoyaltyDiscountPromotion');
      args.order.unset('custdisLoyaltyDiscountApplied');
      args.order.unset('custdisLoyaltyCardNumber');
      args.order.unset('custdisLoyaltyMonthValidity');
      args.order.unset('custdisLoyaltyYearValidity');
      args.order.unset('custdisLoyaltyProgramValidated');
      
      var tempModels = new Array();
      var models = new Array();
      var promotions = new Object();
      var count = 0;
      var totalLines = args.order.get('lines').models.length;
      var selectedPaymentId = '%';
      
      if (!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) &&
        !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.paymentMethod)) {
        selectedPaymentId = args.paymentSelected.paymentMethod.paymentMethod;
      } else if (!OB.UTIL.isNullOrUndefined(args.order.get('custdisIntegratedCC')) && 
                !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.order.get('custdisIntegratedCC')]) && 
                !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.order.get('custdisIntegratedCC')].paymentMethod) &&
                !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.paymentnames[args.order.get('custdisIntegratedCC')].paymentMethod.paymentMethod)) {
        selectedPaymentId = OB.MobileApp.model.paymentnames[args.order.get('custdisIntegratedCC')].paymentMethod.paymentMethod;
      }
      
      var SHOWPOPUP = function showpopup() {
         var remainingToPay = args.order.get('gross') - args.order.get('payment');
         var qtyPerInvoice = 0;
         for (var k = 0; k < tempModels.length; k++) {
            var currentOffer = tempModels[k];
            var discountAmount = new Array();
            var discountPercent = null;
            if(!OB.UTIL.isNullOrUndefined(currentOffer.get('custdisInvoiceqty')) && currentOffer.get('custdisInvoiceqty') > 0 && currentOffer.get('custdisInvoiceqty') < currentOffer.get('custdisProductLines').length){
                qtyPerInvoice = currentOffer.get('custdisInvoiceqty');	
            }else{
            	qtyPerInvoice = currentOffer.get('custdisProductLines').length;
            }
            var totalDiscountAmount = 0.0;
            var prodCatDiscount = false;
                for (var i = 0; i < qtyPerInvoice; i++) {
                    if (currentOffer.get('custdisProductLines')[i].discountpercentage) {
                        var flag = true;
                        if (flag) {
                            prodCatDiscount = true;
                        }
                    }
                }   
         if(prodCatDiscount) {
                 for (var i = 0; i < qtyPerInvoice; i++) {
                     discountPercent = currentOffer.get('custdisProductLines')[i].discountpercentage;
                     discountAmount[i] =  (discountPercent/100) * currentOffer.get('custdisProductLines')[i].selectedLineAmount;
                     if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                      	discountAmount[i] = +(discountAmount[i].toFixed(3));
                      }else{
                    	  discountAmount[i] = +(discountAmount[i].toFixed(2));
                      }
                     totalDiscountAmount +=  discountAmount[i];
                     if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                    	 totalDiscountAmount = +(totalDiscountAmount.toFixed(3));
                     }else{
                    	 totalDiscountAmount = +(totalDiscountAmount.toFixed(2));
                     }
                 }       		          
         }
         if (currentOffer.get('custdisMinPurchaseAmount') <= currentOffer.get('custdisSelectedLineAmount')
                && remainingToPay >= currentOffer.get('custdisMinPurchaseAmount')) {
               var discountPercentage = currentOffer.get('custdisDiscountPercentage');
               var maxDiscountAmount = currentOffer.get('custdisMaxDiscountAmt');
               var discountAmt = 0.0;
               var linedisamt;
            if(prodCatDiscount) {
              discountAmt = totalDiscountAmount;  
            } else {
            	for (var i = 0; i < qtyPerInvoice; i++) {
                     	var lineAmt = currentOffer.attributes.custdisProductLines[i].selectedLineAmount;
                     	linedisamt = (discountPercentage/100) * lineAmt;
                         if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                         	linedisamt = +(linedisamt.toFixed(3));
                          }else{
                          	linedisamt = +(linedisamt.toFixed(2));
                          }
                         discountAmt +=  linedisamt;
                         if(OB.MobileApp.model.get('currency').standardPrecision > 2){
                         	discountAmt = +(discountAmt.toFixed(3));
                         }else{
                         	discountAmt = +(discountAmt.toFixed(2));
                         }                  
            		 }        	
            }
               if (discountAmt > maxDiscountAmount) {
                  discountAmt = maxDiscountAmount;
               }
               currentOffer.set('custdisApplicableDiscountAmount', discountAmt);
               models.push(currentOffer);
            } else {
              currentOffer.unset('custdisApplicableDiscountAmount');
              currentOffer.unset('custdisSelectedLineAmount');  
            }
         }
         promotions.models = models;
         tempModels = null;
        
        // For Loyalty implementation
        
         if (models.length > 0) {      
            OB.MobileApp.view.$.containerWindow.showPopup(
                  'CUSTDIS_ModalBankBinPromotionSelector',
                {
                  args: args,
                  callbacks: callbacks,
                  promotions: promotions,
                  order: args.order,
                }
            );
         } else {
           OB.UTIL.HookManager.callbackExecutor(args, callbacks);
         }
      }
     
    var loyaltyPaymentMethodSelected = false,  
        eligibleLines = [],
        enabledLoyaltyPayMethod;
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod) {
          enabledLoyaltyPayMethod = JSON.parse(localStorage.getItem('enabledLoyaltyDiscountPayMethod')).enabledLoyaltyDiscountPayMethod;
        }
         if (!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) && 
                 !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.searchKey) && !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod) &&
                 !OB.UTIL.isNullOrUndefined(enabledLoyaltyPayMethod[args.paymentSelected.paymentMethod.searchKey])) {
            loyaltyPaymentMethodSelected = true; 
         } 
          var m_offer_type_id = null;
          var str = '';
          var offerStr = '';
          if(!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) 
             && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier) && args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier === 'Credit Card'){
              m_offer_type_id = 'B277B63BDA6E4B67B3D8F491FC3C4E3B';
          }else if(!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) 
             && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier)
             && args.paymentSelected.paymentMethod.paymentMethodCategory$_identifier === 'Non-cash'  && loyaltyPaymentMethodSelected){
              m_offer_type_id = '25ECBE384EAB4C1EB69948FF99FC8456'; 
          }
          if( m_offer_type_id === '25ECBE384EAB4C1EB69948FF99FC8456' ) {
              str = " and ((om.loyaltyPaymentMethodId like '" ;
              offerStr = " where O.m_offer_type_id in ('25ECBE384EAB4C1EB69948FF99FC8456') ";
          } else {
              str = " and ((om.paymentMethodId like '" ;
              offerStr = " where O.m_offer_type_id in ('B277B63BDA6E4B67B3D8F491FC3C4E3B') ";
         }
       // OBDEV - 319   
       if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTDIS_loyaltyPaymentValidation) 
           && OB.MobileApp.model.attributes.permissions.CUSTDIS_loyaltyPaymentValidation === true 
           && !OB.UTIL.isNullOrUndefined(m_offer_type_id) 
           && m_offer_type_id === '25ECBE384EAB4C1EB69948FF99FC8456') {
         eligibleLines = _.filter(args.order.get('lines').models, function (line) {
           if(line.get('discountedNet') ===  line.get('priceList')){
            return line; 
          }
        });  
       } else {
          eligibleLines = args.order.get('lines').models;
       }    
       totalLines = eligibleLines.length; 
      _.each(eligibleLines, function(line) {
               var offerLines = 0; 
               var query = "select distinct O.* from m_offer O left join m_offer_product OF on O.m_offer_id = OF.m_offer_id left join m_offer_bp_group OBPG on OBPG.m_offer_id = O.m_offer_id left join OfferBrandConfig ob on ob.promotionDiscountId = O.m_offer_id left join OfferPaymentMethodConfig om on om.promotionDiscountId = O.m_offer_id left join m_offer_prod_cat pc on pc.m_offer_id = O.m_offer_id" //
               + offerStr + " "
               + "and O.em_custdis_pay_method_link != '' "
               + "and ((OF.m_product_id = '" + line.get('product').get('id') + "'" + " and O.product_selection = 'N') or " + "(O.product_selection = 'Y' and (select count(*) from m_offer_product where m_product_id = '" + line.get('product').get('id') + "' and m_offer_id = O.m_offer_id" + ") = 0 )) " // Product comparison
               + "and ((ob.brandId = '" + line.get('product').get('brand') + "'" + " and O.em_custdis_brand_selection = 'N') or " + "(O.em_custdis_brand_selection = 'Y' and (select count(*) from OfferBrandConfig where brandId = '" + line.get('product').get('brand') + "' and promotionDiscountId = O.m_offer_id" + ") = 0))" // Brand comparison
               + "and ((pc.m_product_category_id = '" + line.get('product').get('productCategory') + "'" + " and O.prod_cat_selection = 'N') or " + "(O.prod_cat_selection = 'Y' and (select count(*) from m_offer_prod_cat where m_product_category_id = '" + line.get('product').get('productCategory') + "' and m_offer_id = O.m_offer_id" + ") = 0)) " // Prod Cat comparison
               + str +""+ selectedPaymentId + "'" + " and O.em_custdis_pay_method_sel = 'N') or " + "(O.em_custdis_pay_method_sel = 'Y')) " // Payment Method comparison
              //  + "and ((om.paymentMethodId like '" + selectedPaymentId + "'" + " and O.em_custdis_pay_method_sel = 'N') or " + "(O.em_custdis_pay_method_sel = 'Y')) " // Payment Method comparison
               + "and ((O.bp_group_selection = 'N' and OBPG.c_bp_group_id='" + args.order.get('bp').get('businessPartnerCategory') + "') or (O.bp_group_selection = 'Y'))" //
               + " and O.datefrom <= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') 
               + "' and (O.dateto is null or O.dateto >= '" + moment(OB.UTIL.localStorage.getItem('businessdate')).format('YYYY-MM-DD') + "') " 
               + OB.CUSTDIS.Utils.addAvailabilityRule(moment())
               + " order by O.name";
               OB.Dal.queryUsingCache(OB.Model.Discount, query, [], function (offers) {
                  if (offers.length === 0) {
                    count = count + 1;
                    if (count === totalLines && tempModels && tempModels.length > 0) {
                       SHOWPOPUP();
                    }else if(count === totalLines && loyaltyPaymentMethodSelected){
                       OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Promotion is not available for this Loyalty Payment");
                       args.cancelOperation = true;
                    }
                  } else {
                    _.each(offers.models, function(offer) {
                         var linkedPaymentMethod = null;
                         _.each(OB.MobileApp.model.paymentnames, function(payment) {
                              if (payment.paymentMethod.paymentMethod === offer.get('custdisPayMethodLink')) {
                                    linkedPaymentMethod = payment;
                               }
                         });
                         if (!OB.UTIL.isNullOrUndefined(linkedPaymentMethod)) {
                           var prodcatquery = "select pc.em_custdis_discount_percentage from m_offer_prod_cat pc  where pc.m_offer_id = '" + offer.get('id') + "' and pc.m_product_category_id = '" + line.get('product').get('productCategory') + "'";
                           var applicableDiscountPercentage = null;
                           OB.Dal.queryUsingCache(OB.Model.DiscountFilterProductCategory, prodcatquery, [], function (data) {
                            if(data.length !== 0) {
                             applicableDiscountPercentage = data.models[0].get('custdisDiscountPercentage');
                             }
                             var currentOffer = null;
                             var custdisSelectedLineAmount = 0;
                             if (!OB.UTIL.isNullOrUndefined(tempModels) && tempModels.length > 0) {
                                currentOffer = tempModels.filter((item) => item.get('id') === offer.get('id'))[0];
                             } else {
                                tempModels = [];
                             }                         
                             if (!OB.UTIL.isNullOrUndefined(currentOffer)) {
                                 tempModels.splice(tempModels.findIndex(a => a.get('id') === currentOffer.get('id')) , 1);
                             } else {
                                 currentOffer = offer;
                                 currentOffer.unset('custdisProductLines');
                             }
                             custdisSelectedLineAmount = OB.UTIL.isNullOrUndefined(currentOffer.get('custdisSelectedLineAmount')) ? line.get('discountedGross') : currentOffer.get('custdisSelectedLineAmount') + line.get('discountedGross');
                             var products = new Object();
                             products.id = line.get('product').get('id');
                             products.line = line.get('id');
                             products.discountpercentage = applicableDiscountPercentage;
                             products.selectedLineAmount = line.get('discountedGross');
                             products.custdisSelectedLineDiscountAmount = 0;
                             var lineid = null;
                             var custdisProductLines = (OB.UTIL.isNullOrUndefined(currentOffer.get('custdisProductLines'))) ? new Array() : currentOffer.get('custdisProductLines');
                             if (!OB.UTIL.isNullOrUndefined(custdisProductLines)) {
                                lineid = custdisProductLines.filter((item) => item.line === products.line);
                             }  
                             if (lineid.length == 0) {
                              custdisProductLines.push(products);  
                            }                               
                             currentOffer.set('custdisProductLines', custdisProductLines);
                             currentOffer.set('custdisLinkedPaymentMethod', linkedPaymentMethod);
                             currentOffer.set('custdisSelectedLineAmount', custdisSelectedLineAmount);
                             tempModels.push(currentOffer);    
                             offerLines = offerLines + 1;
                             if (offerLines === offers.models.length) {
                               count = count + 1;
                             } 
                             if(count=== totalLines) {
                                SHOWPOPUP();
                             }  
                           });  
                      
                        }
                    });
                  }
               });  
      });
  }
} 

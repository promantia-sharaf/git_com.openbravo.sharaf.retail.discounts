/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.CUSTDIS = OB.CUSTDIS || {};

  OB.CUSTDIS.Utils = {

    getPaymentMethod: function (searchKey, name) {
      var payMth = OB.MobileApp.model.hasPayment(searchKey);
      if (!payMth) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_ErrNotFoundPaymentMethod', [name]));
      }
      return payMth;
    },

    findPromotionProduct: function (line, promotion, callback) {
      enyo.$.scrim.show();
      var criteria = {
        priceAdjustment: promotion.rule.id || promotion.rule.get('ruleId'),
        product: line.get('product').id
      };

      OB.Dal.find(OB.Model.DiscountFilterProduct, criteria, function (products) {
        if (products.length === 0) {
          enyo.$.scrim.hide();
          callback(null);
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_ErrPromotionNotApply', [promotion.rule.get('name')]));
        } else {
          enyo.$.scrim.hide();
          callback(products.at(0));
        }
      }, function () {
        enyo.$.scrim.hide();
        callback(null);
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTDIS_ErrPromotionNotApply', [promotion.rule.get('name')]));
      });
    },

findPromotionProductList: function (line, promotion, callback) {
      enyo.$.scrim.show();
      var criteria = {
        priceAdjustment: promotion.rule.id || promotion.rule.ruleId
       // product: line.get('product').id
      };

      OB.Dal.find(OB.Model.DiscountFilterProduct, criteria, function (products) {
        if (products.length === 0) {
          enyo.$.scrim.hide();
          callback(null);
        } else {
          enyo.$.scrim.hide();
          callback(products);
        }
      }, function () {
        enyo.$.scrim.hide();
        callback(null);
      });
    },
    
    getPromotionsProducts: function (promotionId, callback) {
      var criteria = {
        priceAdjustment: promotionId
      };

      OB.Dal.find(OB.Model.DiscountFilterProduct, criteria, function (products) {
        callback(products);
      }, function () {
        callback([]);
      });
    },

    saveReturnOrderLineForBundleReinvoice: function (receipt, products, idx) {
      var returnLineArr = [],
          line;
      if (!OB.UTIL.isNullOrUndefined(receipt.get('returnLineId'))) {
        returnLineArr = receipt.get('returnLineId');
        receipt.unset('returnLineId');
      }

      if (idx < products.models.length) {
        var prd = products.models[idx];
        for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
          line = OB.MobileApp.model.receipt.get('lines').models[i];
          if (OB.UTIL.isNullOrUndefined(line.get('returnLineId')) || line.get('returnLineId') === '') {
            line.set('returnLineId', returnLineArr[idx]);
            OB.CUSTDIS.Utils.saveReturnOrderLineForBundleReinvoice(receipt, products, idx + 1);
          } else if (line.get('returnLineId') === returnLineArr[idx]) {
            idx++;
          }
        }
      }
    },

    saveInvoiceTypeForEPAYBundlePromotion: function (receipt) {
      var line, invoiceType;
      for (var i = 0; i < receipt.get('lines').models.length; i++) {
        line = receipt.get('lines').models[i];
        if (!OB.UTIL.isNullOrUndefined(receipt.get('lines').models[i].get('custdisOffer')) && OB.UTIL.isNullOrUndefined(receipt.get('lines').models[i].get('custdisOrderline'))) {
          if (!OB.UTIL.isNullOrUndefined(receipt.get('lines').models[i].get('product').get('invoice_type'))) {
            invoiceType = receipt.get('lines').models[i].get('product').get('invoice_type');
            break;
          }
        }
      }
      if (!OB.UTIL.isNullOrUndefined(invoiceType)) {
        for (var i = 0; i < receipt.get('lines').models.length; i++) {
          line = receipt.get('lines').models[i];
          if (!OB.UTIL.isNullOrUndefined(line.get('product'))) {
            if (!OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type'))) {
              continue;
            } else {
              line.get('product').set('invoice_type', invoiceType);
              line.set('invoice_type', invoiceType);
            }
          }
        }
        receipt.save();
      }
    },
    addAvailabilityRule: function(moment) {
      var weekday = [
          'sunday',
          'monday',
          'tuesday',
          'wednesday',
          'thursday',
          'friday',
          'saturday'
        ],
        currenttime = moment.format('YYYY-MM-DDTHH:mm:ss'),
        day = weekday[moment.day()],
        startingtimeday = 'startingtime'.concat(day),
        endingtimeday = 'endingtime'.concat(day);
      return (
        '\n' + //
        "AND ((O.allweekdays = 'true' AND ((O.startingtime < '" +
        currenttime +
        "' AND O.endingtime > '" +
        currenttime +
        "') " + //
        "OR (O.endingtime is null AND O.startingtime < '" +
        currenttime +
        "') " + //
        "OR (O.startingtime is null and '" +
        currenttime +
        "' < O.endingtime) " + //
        'OR (O.startingtime is null AND O.endingtime is null))) ' + //
        'OR (' +
        day +
        " = 'true' " + //
        'AND ((' +
        startingtimeday +
        " < '" +
        currenttime +
        "' AND " +
        endingtimeday +
        " > '" +
        currenttime +
        "') " + //
        'OR  (' +
        endingtimeday +
        ' is null AND ' +
        startingtimeday +
        " < '" +
        currenttime +
        "') " + //
        'OR (' +
        startingtimeday +
        " is null and '" +
        currenttime +
        "' < " +
        endingtimeday +
        ') ' + //
        'OR (' +
        startingtimeday +
        ' is null AND ' +
        endingtimeday +
        ' is null)))) '
      );
    },
    addPromotionProducts: function (promotion, receipt, line) {
      enyo.$.scrim.show();
      this.getPromotionsProducts(promotion.get('id'), function (products) {
        var createIRPayments = function (products) {
            receipt.calculateReceipt(function () {
              receipt.save(function () {
                OB.MobileApp.model.orderList.saveCurrent();
                _.each(products.models, function (prd) {
                  if (!OB.UTIL.isNullOrUndefined(prd.get('custdisIrType'))) {
                    if (!receipt.get('isQuotation')) {
                      var searhKey = prd.get('custdisIrType') === 'IRA' ? 'CUSTSPM_payment.instantA' : 'CUSTSPM_payment.instantB',
                          paymentMethod = OB.CUSTDIS.Utils.getPaymentMethod(searhKey, 'WebPOS Instant Redemption ' + (prd.get('custdisIrType') === 'IRA' ? 'A' : 'B'));
                      if (paymentMethod) {
                        // Add Instant Redemption payment
                        for (var i = 0; i < prd.get('custdisGiftqty'); i++) {
                          OB.CUSTDIS.Utils.addReceiptPayment(searhKey, paymentMethod, prd.get('custdisAmount'), receipt, line);
                        }
                      }
                    }
                  }
                });
                enyo.$.scrim.hide();
              });
            });
            },
            
            
            
            
            
            
            
            checkProdAndProdCatForDocumentType = function (products, idx) {
            var documentSK = receipt.get('custsdtDocumenttypeSearchKey'),
                documentTypeId = receipt.get('custsdtDocumenttype'),
                prodArr = [],
                prodCatArr = [],
                isError = false;
            if (idx < products.models.length) {
              var prd = products.models[idx];
              if (prd.get('custdisIsGift')) {
                OB.Dal.get(OB.Model.Product, prd.get('product'), function (prod) {
                  OB.CUSTSDT.Utils.fetchProdAndProdCatForDocumentType(documentSK, documentTypeId, prodCatArr, prodArr, prod, function () {
                    OB.CUSTSDT.Utils.checkProdAndProdCatForDocumentType(documentSK, prodCatArr, prodArr, prod, isError, function () {
                      if (prod.get('isError')) {
                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), 'Secondary article(s) ' + prod.get('_identifier') + ' are not allowed in ' + documentSK + ' document type. Please delete the primary article or proceed to complete the bill without Promo', [{
                          label: OB.I18N.getLabel('OBMOBC_LblOk'),
                          isConfirmButton: true,
                          action: function () {
                            return;
                          }
                        }]);
                      } else {
                        checkProdAndProdCatForDocumentType(products, idx + 1);
                      }
                    });
                  });
                });
              } else {
                checkProdAndProdCatForDocumentType(products, idx + 1);
              }
            } else {
              OB.Model.Discounts.addManualPromotion(receipt, [line], {
                rule: promotion,
                definition: {}
              });
              createProductGiftLine(products, 0);
            }
            },
            checkProdAndProdCatForDocumentType, createProductGiftLine;
        createProductGiftLine = function (products, idx) {
          if (idx < products.models.length) {
            var prd = products.models[idx];

            if (promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
              prd.set('custdisIsGift', true);
              prd.set('discType', '1424B47A341E4D38B0F61CCF26450AF3');
              prd.set('custdisGiftqty', promotion.get('custdisY'));
              prd.set('custdisPrimaryNewPrice', promotion.get('custdisPrimaryNewprice'));
              prd.set('custdisSecondaryNewPrice', promotion.get('custdisSecondaryNewprice'));
            }
            if (prd.get('custdisIsGift')) {
              OB.Dal.get(OB.Model.Product, prd.get('product'), function (prod) {
                var prodGift;
                var prodGiftQty = 0;
                var updateGiftQty = false;
                var giftProdDelivery = null;
                var options = new Object();
                var opt = [];
                var line1 = {};
                var relatedLines = null;
                var recLength = receipt.attributes.lines.models.length;
                var prodGiftMap = new Map();
                var digitalBundlePromo = false,
                    isReInvoice = false,
                    digiPrdCount = 0;

                var checkForDigitalProduct = function (receipt, products, prod, callback) {
                  var prdValidated = 0;
                  products.models.forEach(function (p) {
                    prodGift = p.attributes.product;
                    var query = "select p.* from m_product p where p.m_product_id='" + prodGift + "'";
                    OB.Dal.queryUsingCache(OB.Model.Product, query, [], function (product) {
                      if (product.models.length > 0) {
                        var prod = product.models[0];
                        prdValidated++;
                        if (prod.get('digiProductType') === 'P' || prod.get('digiProductType') === 'E' || prod.get('digiProductType') === 'A' || prod.get('digiProductType') === 'SP' || prod.get('digiProductType') === 'SA') {
                          digiPrdCount++;
                        }
                        if (products.models.length === prdValidated) {
                          callback();
                        }
                      } else {
                        callback();
                      }
                    }, null);
                  });
                };

                products.models.forEach(function (p) {
                  if (p.attributes.custdisIsGift && prd.get('product') === p.attributes.product) {
                    prodGift = p.attributes.product;
                    receipt.attributes.lines.models.forEach(function (rec) {
                      if (rec.attributes.product.id === prodGift && !OB.UTIL.isNullOrUndefined(p.attributes.custdisGiftqty) && p.attributes.custdisGiftqty <= 1 && recLength >= products.models.length) {
                        if (!prodGiftMap.has(rec.attributes.product.id)) {
                          prodGiftMap.set(rec.attributes.product.id, prd.attributes.custdisGiftqty);
                          updateGiftQty = true;
                          if (line.attributes.qty > rec.attributes.qty) {
                            prodGiftQty = line.attributes.qty - rec.attributes.qty;
                          } else if (rec.attributes.qty > line.attributes.qty) {
                            prodGiftQty = line.attributes.qty - rec.attributes.qty;
                            options.line = rec;
                            line1 = {
                              line: rec
                            };
                            opt.push(line1);
                          }
                        }
                      } else if (rec.attributes.product.id === prodGift && !OB.UTIL.isNullOrUndefined(p.attributes.custdisGiftqty) && p.attributes.custdisGiftqty > 1 && recLength >= products.models.length && receipt.get('custsdtDocumenttypeSearchKey') === 'BS') {
                        if (!prodGiftMap.has(rec.attributes.product.id)) {
                          prodGiftMap.set(rec.attributes.product.id, prd.attributes.custdisGiftqty);
                          prodGiftQty = line.attributes.qty * prd.attributes.custdisGiftqty;
                          updateGiftQty = true;
                          if (prodGiftQty > rec.attributes.qty) {
                            prodGiftQty = prodGiftQty - rec.attributes.qty;
                          } else {
                            prodGiftQty = prodGiftQty - rec.attributes.qty;
                            if (prodGiftQty === 0) {
                              prodGiftQty = prd.attributes.custdisGiftqty;
                            }
                            options.line = rec;
                            line1 = {
                              line: rec
                            };
                            opt.push(line1);
                          }
                        }
                      } else if (prodGiftMap.size === 0) {
                        prodGiftQty = p.attributes.custdisGiftqty;

                      }
                    });
                  }
                });

                if (prod.get('productType') === 'S') {
                  relatedLines = [{
                    orderlineId: line.id,
                    productCategory: line.get('product').get('productCategory'),
                    productName: line.get('product').get('description')
                  }];
                }

                if (updateGiftQty) {
                  var giftQty = prodGiftQty;
                } else {
                  var giftQty = prd.attributes.custdisGiftqty;
                }

                if (promotion.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3') {
                  var giftQty = promotion.get('custdisY');
                  prodGiftQty = promotion.get('custdisY');
                }

                if (!OB.UTIL.isNullOrUndefined(receipt.get('custsdtDocumenttypeSearchKey')) && receipt.get('custsdtDocumenttypeSearchKey') === 'BS') {
                  prod.set('groupProduct', true);
                }

                var delTypeArr = JSON.parse(localStorage.getItem('allowedDelivery'));
                if (delTypeArr.includes(prod.get('cUSTDELDeliveryCondition'))) {
                  giftProdDelivery = prod.get('cUSTDELDeliveryCondition');
                } else {
                  giftProdDelivery = delTypeArr[0];
                }

                checkForDigitalProduct(receipt, products, prod, function () {
                  if (products.models.length === digiPrdCount) {
                    digitalBundlePromo = true;
                  }
                  if (!receipt.get('isQuotation')) {
                    if (!OB.UTIL.isNullOrUndefined(prod.get('digiProductType')) && OB.UTIL.isNullOrUndefined(receipt.get('reInvoiceLink'))) {
                      receipt.set('digiProductPresent', true);
                      if (receipt.get('custsdtDocumenttypeSearchKey') !== 'BS') {
                        enyo.$.scrim.show();
                        var pinPrintProduct, activationProduct, serialNoArr = [];
                        if (prod.get('digiProductType') === 'P' || prod.get('digiProductType') === 'E' || prod.get('digiProductType') === 'SP') {
                          pinPrintProduct = true;
                        } else if (prod.get('digiProductType') === 'A' || prod.get('digiProductType') === 'SA') {
                          activationProduct = true;
                        }
                        //check whether secondary digital product is having invoice type 
                        var invoiceTypeNotSelected = false;
                        if (receipt.get('digiProductPresent')) {
                          if (OB.UTIL.isNullOrUndefined(prod.get('invoice_type'))) {
                            if (!OB.UTIL.isNullOrUndefined(line.get('product').get('invoice_type'))) {
                              prod.set('invoice_type', line.get('product').get('invoice_type'));
                              if (prod.get('invoice_type') === 'Re Invoice' || prod.get('invoice_type') === 're_invoice') {
                                isReInvoice = true;
                              }
                            } else {
                              invoiceTypeNotSelected = true;
                            }
                          }
                        }

                        if ((pinPrintProduct || activationProduct) && !(digitalBundlePromo && isReInvoice)) {
                          if (receipt.get('digiProductPresent') && invoiceTypeNotSelected) {
                            OB.MobileApp.view.waterfall('onShowPopup', {
                              popup: 'CPSE.UI.epayInvoiceType',
                              args: {
                                data: receipt,
                                callback: function () {
                                  receipt.save();
                                  prod.set('invoice_type', receipt.get('lines').models[0].get('product').get('invoice_type'));
                                  if (prod.get('invoice_type') === 'New Invoice' || prod.get('invoice_type') === 'new_invoice') {
                                    if (pinPrintProduct) {
                                      OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                                    } else if (activationProduct) {
                                      OB.UTIL.showLoading(true);
                                      OB.CUSTDIS.Utils.showEPAYSerialNoPopupForGiftArticle(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, 0, serialNoArr, createProductGiftLine);
                                      OB.UTIL.showLoading(false);
                                    }
                                  } else if (prod.get('invoice_type') === 'Re Invoice' || prod.get('invoice_type') === 're_invoice') {
                                    this.params = [];
                                    this.params.isQuotation = false;
                                    this.params.isReturn = false;
                                    this.params.isReinvoice = true;

                                    CPSE.UI.ModalPaidReceiptsModified.prototype.setParams(this.params);
                                    OB.EPAY_R.Reinvoice.prodToAddId = prod.get('id');
                                    OB.EPAY_R.Reinvoice.prodToAddDescription = prod.get('description');
                                    OB.EPAY_R.Reinvoice.promotion = promotion;
                                    OB.MobileApp.view.waterfallDown('onShowPopup', {
                                      popup: 'CPSE.UI.ModalPaidReceiptsModified',
                                      args: {
                                        data: prod,
                                        callback: function () {
                                          receipt.set('returnLineId', prod.get('returnLineId'));
                                          receipt.set('receiptLines', prod.get('receiptLines'));
                                          receipt.set('selectedLines', prod.get('selectedLines'));
                                          receipt.set('retDocumentNo', prod.get('retDocumentNo'));
                                          receipt.set('returnOrder', prod.get('returnOrder'));
                                          receipt.set('reInvoiceLink', true);
                                          receipt.save();
                                          OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                                        }
                                      }
                                    });
                                  } else {
                                    enyo.$.scrim.hide();
                                    OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                                  }
                                }
                              }
                            });
                          } else {
                            if (prod.get('invoice_type') === 'New Invoice' || prod.get('invoice_type') === 'new_invoice') {
                              if (pinPrintProduct) {
                                OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                              } else if (activationProduct) {
                                OB.UTIL.showLoading(true);
                                OB.CUSTDIS.Utils.showEPAYSerialNoPopupForGiftArticle(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, 0, serialNoArr, createProductGiftLine);
                                OB.UTIL.showLoading(false);
                              }
                            } else if (prod.get('invoice_type') === 'Re Invoice' || prod.get('invoice_type') === 're_invoice') {
                              this.params = [];
                              this.params.isQuotation = false;
                              this.params.isReturn = false;
                              this.params.isReinvoice = true;

                              CPSE.UI.ModalPaidReceiptsModified.prototype.setParams(this.params);
                              OB.EPAY_R.Reinvoice.prodToAddId = prod.get('id');
                              OB.EPAY_R.Reinvoice.prodToAddDescription = prod.get('description');
                              OB.EPAY_R.Reinvoice.promotion = promotion;
                              OB.MobileApp.view.waterfallDown('onShowPopup', {
                                popup: 'CPSE.UI.ModalPaidReceiptsModified',
                                args: {
                                  data: prod,
                                  callback: function () {
                                    receipt.set('returnLineId', prod.get('returnLineId'));
                                    receipt.set('reInvoiceLink', true);
                                    receipt.save();
                                    OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                                  }
                                }
                              });
                            } else {
                              enyo.$.scrim.hide();
                              OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                            }
                          }
                        } else {
                          OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                        }
                      } else {
                        OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                      }
                    } else {
                      OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                    }
                  } else {
                    OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
                  }
                });
              });
            } else {
              createProductGiftLine(products, idx + 1);
            }
          } else {
            OB.CUSTDIS.Utils.saveInvoiceTypeForEPAYBundlePromotion(receipt);
            createIRPayments(products);
            OB.CUSTDIS.Utils.saveReturnOrderLineForBundleReinvoice(receipt, products, 0);
          }
        };
        checkProdAndProdCatForDocumentType(products, 0);
      });
    },

    showEPAYSerialNoPopupForGiftArticle: function (prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, index, serialNoArr, createProductGiftLine) {
      OB.UTIL.showLoading(false);
      receipt.set('_identifier', prod.get('_identifier'));
      if (index < prodGiftQty) {
        OB.MobileApp.view.waterfallDown('onShowPopup', {
          popup: 'CPSE.UI.epayActivationSerialNo',
          args: {
            epaydata: receipt,
            callback: function () {
              serialNoArr.push(receipt.get('digiSerialNo'));
              OB.CUSTDIS.Utils.showEPAYSerialNoPopupForGiftArticle(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, index + 1, serialNoArr, createProductGiftLine);
            }
          }
        });
      } else {
        receipt.unset('digiSerialNo');
        OB.CUSTDIS.Utils.addPromotionProductToTicket(prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine);
        OB.CUSTDIS.Utils.saveEPAYSerialNoForGiftArticle(receipt, products, idx, serialNoArr, createProductGiftLine);
      }
    },

    saveEPAYSerialNoForGiftArticle: function (receipt, products, idx, serialNoArr, createProductGiftLine) {
      var count = 0;
      if (count < serialNoArr.length) {
        for (var i = 0; i < receipt.attributes.lines.models.length; i++) {
          if ((receipt.get('lines').models[i].get('product').get('digiProductType') === 'A' || receipt.get('lines').models[i].get('product').get('digiProductType') === 'SA') && (OB.UTIL.isNullOrUndefined(receipt.get('lines').models[i].get('product').get('digiSerialNo')) || receipt.get('lines').models[i].get('product').get('digiSerialNo') === '')) {
            receipt.get('lines').models[i].get('product').set('digiSerialNo', serialNoArr[count]);
            count++;
          }
        }
        receipt.save();
      }
    },

    addPromotionProductToTicket: function (prod, promotion, prodGiftQty, giftQty, prd, giftProdDelivery, receipt, line, products, relatedLines, idx, createProductGiftLine) {
      if (prodGiftQty > 0 || giftQty > 0) {
        receipt.addProductToOrder(prod, giftQty ? giftQty : 1, undefined, {
          originalLine: line.id,
          custdisAddByPromo: true,
          custdisPromotion: promotion,
          custdisOrderline: line.id,
          custdisIrType: prd.get('custdisIrType'),
          custdisRedemptionAmount: prd.get('custdisIrType') ? prd.get('custdisAmount') : 0,
          custdisAmount: OB.CUSTDIS.Utils.calculateLineDiscount(prod.get('standardPrice'), prd),
          cUSTDELDeliveryCondition: giftProdDelivery,
          cUSTDELDeliveryTime: line.get('cUSTDELDeliveryTime'),
          relatedLines: relatedLines
        }, function () {
          createProductGiftLine(products, idx + 1);
        });
      } else {
        for (let i = 0; i < opt.length; i++) {
          if (!OB.UTIL.isNullOrUndefined(opt[i + 1])) {
            if (opt[i].line.get('qty') < opt[i + 1].line.get('qty')) {
              opt[i + 1].line.set('isReturnsNegative', true);
              opt[i].line.set('isReturnsNegative', false);
            } else {
              opt[i].line.set('isReturnsNegative', true);
              opt[i + 1].line.set('isReturnsNegative', false);
            }
            receipt.addProductToOrder(prod, giftQty ? giftQty : 1, opt[i + 1].line.get('isReturnsNegative') ? opt[i + 1] : opt[i], {
              originalLine: line.id,
              custdisAddByPromo: true,
              custdisPromotion: promotion,
              custdisOrderline: line.id,
              custdisIrType: prd.get('custdisIrType'),
              custdisRedemptionAmount: prd.get('custdisIrType') ? prd.get('custdisAmount') : 0,
              custdisAmount: OB.CUSTDIS.Utils.calculateLineDiscount(prod.get('standardPrice'), prd),
              cUSTDELDeliveryCondition: giftProdDelivery,
              cUSTDELDeliveryTime: line.get('cUSTDELDeliveryTime'),
              relatedLines: relatedLines
            }, function () {
              createProductGiftLine(products, idx + 1);
            });
          } else {
            opt[i].line.set('isReturnsNegative', true);
            receipt.addProductToOrder(prod, giftQty ? giftQty : 1, opt[i], {
              originalLine: line.id,
              custdisAddByPromo: true,
              custdisPromotion: promotion,
              custdisOrderline: line.id,
              custdisIrType: prd.get('custdisIrType'),
              custdisRedemptionAmount: prd.get('custdisIrType') ? prd.get('custdisAmount') : 0,
              custdisAmount: OB.CUSTDIS.Utils.calculateLineDiscount(prod.get('standardPrice'), prd),
              cUSTDELDeliveryCondition: giftProdDelivery,
              cUSTDELDeliveryTime: line.get('cUSTDELDeliveryTime'),
              relatedLines: relatedLines
            }, function () {
              createProductGiftLine(products, idx + 1);
            });
          }
        }
      }
    },

    addReceiptPayment: function (searchKey, paymentMethod, amount, receipt, line) {

      var newPayment = new OB.Model.PaymentLine({
        kind: searchKey,
        name: paymentMethod.paymentMethod.name,
        amount: amount,
        mulrate: paymentMethod.mulrate,
        isocode: paymentMethod.isocode,
        paymentData: {
          lineId: line.id
        }
      });
      receipt.addPayment(newPayment);
    },

    addLinePromotion: function (receipt, line, promotion, amt) {
      line.set('custdisOffer', promotion.rule.id);
      var definition = promotion.definition;
      definition.manual = true;
      definition._idx = -1;
      definition.lastApplied = true;
      definition.amt = amt;
      definition.userAmt = amt;
      receipt.addPromotion(line, promotion.rule, definition);
      receipt.set('custdisTotalMinAmount', promotion.rule.get('custdisTotalMinAmount'));
    },

    addSharafPromotion: function (receipt, line, promotion) {
      var count = 0;
      if (!OB.UTIL.isNullOrUndefined(line.get('promotions'))) {
        for (var j = 0; j < line.get('promotions').length; j++) {
          if (line.get('promotions').length > 0) {
            if ((line.get('promotions')[j].discountType === "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (line.get('promotions')[j].discountType === "D1D193305A6443B09B299259493B272A") || (line.get('promotions')[j].discountType === "7B49D8CC4E084A75B7CB4D85A6A3A578") || (line.get('promotions')[j].discountType === "8338556C0FBF45249512DB343FEFD280") || (line.get('promotions')[j].discountType === "20E4EC27397344309A2185097392D964")) {
              count++;
            }
          }
        }
      }
      if (!OB.UTIL.isNullOrUndefined(line.get('custdisAmount'))) {
        if (line.get('qty') >= 1 && OB.UTIL.isNullOrUndefined(line.get('custdisOffer'))) {
          this.addLinePromotion(receipt, line, promotion, line.get('custdisIrType') === 'IRA' || line.get('custdisIrType') === 'IRB' ? 0 : line.get('custdisAmount'));
        } else if (line.get('qty') >= 1 && !OB.UTIL.isNullOrUndefined(line.get('custdisOffer'))) {
          if (line.get('qty') > line.attributes.promotions.length) {
            var loop = line.get('qty') - line.attributes.promotions.length + count;
            for (i = loop; i > 0; i--) {
              this.addLinePromotion(receipt, line, promotion, line.get('custdisIrType') === 'IRA' || line.get('custdisIrType') === 'IRB' ? 0 : line.get('custdisAmount'));
            }
          } else if (line.get('qty') < line.attributes.promotions.length) {
            var loopQty = line.get('qty') + count;
            OB.CUSTDIS.Utils.removePromotionNew(line, promotion.rule, loopQty, receipt);
          } else if (!OB.UTIL.isNullOrUndefined(line.get('custdisOffer')) && (line.get('custdisOffer') !== promotion.rule.get('id'))) {
            this.addLinePromotion(receipt, line, promotion, line.get('custdisIrType') === 'IRA' || line.get('custdisIrType') === 'IRB' ? 0 : line.get('custdisAmount'));
          }
        }
      }
    },

    removePromotionNew: function (line, rule, loopQty, receipt) {
      var promotions = line.get('promotions'),
          ruleId = rule.id,
          discountinstance = rule.discountinstance,
          removed = false,
          res = [],
          i;

      if (!promotions) {
        return;
      }
      for (i = 0; i < loopQty; i++) {
        if (promotions[i].ruleId === rule.id && promotions[i].discountinstance === discountinstance || ((promotions[i].discountType === "5B59AAF5B6BF477C912A7DFD8A7EE7C7") || (promotions[i].discountType === "D1D193305A6443B09B299259493B272A") || (promotions[i].discountType === "7B49D8CC4E084A75B7CB4D85A6A3A578") || (promotions[i].discountType === "8338556C0FBF45249512DB343FEFD280") || (promotions[i].discountType === "20E4EC27397344309A2185097392D964"))) {
          res.push(promotions[i]);
        }
      }
      line.set('promotions', res);
      OB.CUSTDIS.Utils.calculateDiscountedLinePriceNew(line);
      line.trigger('change');
      OB.CUSTDIS.Utils.saveNew(receipt);
    },

    calculateDiscountedLinePriceNew: function (line) {
      var i;
      var allDiscountedAmt = 0;
      for (i = 0; i < line.get('promotions').length; i++) {
        if (!line.get('promotions')[i].hidden) {
          allDiscountedAmt += line.get('promotions')[i].amt;
        }
      }

      line.set('discountedLinePrice', OB.DEC.toNumber(new BigDecimal(String(line.get('price'))).subtract(new BigDecimal(String(allDiscountedAmt)).divide(new BigDecimal(String(line.get('qty'))), 20, OB.DEC.getRoundingMode()))));

    },

    saveNew: function (receipt, callback) {
      var receiptObj = receipt;
      if (!OB.MobileApp.model.get('preventOrderSave') && !receiptObj.pendingCalculatereceiptObjeipt) {
        var undoCopy = receiptObj.get('undo'),
            me = receiptObj;
        forceInsert = false;

        if (receiptObj.get('isBeingClosed')) {
          var diffreceiptObjeipt = OB.UTIL.diffJson(receiptObj.serializeToJSON(), receiptObj.get('json'));
          var error = new Error();
          OB.error('The receiptObjeipt is being save during the closing: ' + diffreceiptObjeipt);
          OB.error('The stack trace is: ' + error.stack);
        }

        var now = new Date();
        receiptObj.set('timezoneOffset', now.getTimezoneOffset());

        if (!receiptObj.get('id') || !receiptObj.id) {
          var uuid = OB.UTIL.get_UUID();
          receiptObj.set('id', uuid);
          OB.info('[orderid][' + new Date().getTime() + '] UUID -' + uuid + '- generated for order (' + receiptObj.get('documentNo') + '). Stack Trace: ' + OB.UTIL.getStackTrace('', false));
          receiptObj.id = uuid;
          forceInsert = true;
        }

        receiptObj.set('json', JSON.stringify(receiptObj.serializeToJSON()));
        if (callback === undefined || !callback instanceof Function) {
          callback = function () {};
        }
        if ((receiptObj.get('isQuotation') && !receiptObj.get('isEditable')) || receiptObj.get('isCancelling')) {
          if (callback) {
            callback();
          }
        } else {
          OB.Dal.save(receiptObj, function () {
            if (callback) {
              callback();
            }
          }, function () {
            OB.error(arguments);
          }, forceInsert);
        }


        receiptObj.setUndo('SaveOrder', undoCopy);
      } else {
        if (callback) {
          callback();
        }
      }
    },

    setLineDiscount: function (line, offerProduct) {
      //line.set('custdisPaymentAmount', line.get('custdisAmount'));
      line.set('custdisAmount', this.calculateLineDiscount(line.get('product').get('standardPrice'), offerProduct));
    },

    calculateLineDiscount: function (standardPrice, offerProduct) {
    	var discount;
    	if(!OB.UTIL.isNullOrUndefined(standardPrice) && !OB.UTIL.isNullOrUndefined(offerProduct)){
   		  var newPrice = offerProduct.get('custdisAmount'),
             discountAmount = offerProduct.get('custdisDiscountamount'),
             discountPercentage = offerProduct.get('custdisPercentage');

         if (newPrice === 0 && (!OB.UTIL.isNullOrUndefined(offerProduct.get('custdisPrimaryNewPrice')))) {
           newPrice = offerProduct.get('custdisPrimaryNewPrice');
         }

         if (!OB.UTIL.isNullOrUndefined(offerProduct.get('discType')) && offerProduct.get('discType') === '1424B47A341E4D38B0F61CCF26450AF3') {
           if (!OB.UTIL.isNullOrUndefined(offerProduct.get('custdisSecondaryNewPrice'))) {
             newPrice = offerProduct.get('custdisSecondaryNewPrice');
           } else {
             newPrice = 0;
           }
         }
         if (!OB.UTIL.isNullOrUndefined(newPrice)) {
           discount = standardPrice - newPrice;
         } else if (!OB.UTIL.isNullOrUndefined(discountAmount)) {
           discount = discountAmount;
         } else if (!OB.UTIL.isNullOrUndefined(discountPercentage)) {
           discount = OB.DEC.toNumber(OB.DEC.toBigDecimal(standardPrice).multiply(OB.DEC.toBigDecimal(offerProduct.get('custdisPercentage')).divide(new BigDecimal('100'), 20, OB.DEC.getRoundingMode())));
           // TODO: Multiply by quantity of the line. Right now quantity is only 1.
           //discPrice = OB.DEC.mul(OB.DEC.abs(line.get('qty')), OB.DEC.toNumber(price.subtract(discPrice)));
         } else {
           discount = 0;
         }
         return discount;
    		
    	}else{
    		discount = 0;
    		return discount;
    	}
    },

    validateBPCategoryPromotions: function (bp, errorMessage, callback) {
      var i = 0;
      var offerIds = [];
      _.each(OB.POS.terminal.terminal.receipt.attributes.lines.models, function (line) {
        if (line.attributes.custdisOffer) {
          offerIds[i] = "'" + line.attributes.custdisOffer + "'";
          i = i + 1;
        }
      });
      var info = [];
      if (offerIds.length !== 0) {
        var query = "select distinct OBPG.* from m_offer_bp_group OBPG join m_offer O on O.m_offer_id = OBPG.m_offer_id" //
        + " where OBPG.m_offer_id in (" + offerIds.toString() + ")" //
        + " and O.bp_group_selection = 'N' and OBPG.c_bp_group_id != '" + bp.get('businessPartnerCategory') + "'";
        OB.Dal.query(OB.Model.DiscountFilterBusinessPartnerGroup, query, [], function (offers) {
          if (offers.length === 0) {
            callback(info);
          } else {
            info.push(OB.I18N.getLabel(errorMessage).replace("%C_BPC", bp.get('businessPartnerCategory_name')));
            _.each(offers.models, function (offer) {
              _.each(OB.POS.terminal.terminal.receipt.attributes.lines.models, function (line) {
                if ((offer.get('priceAdjustment') == line.attributes.custdisOffer)) {
                  info.push('- ' + line.get('product').get('_identifier') + '(' + offer.get('_identifier') + ')');
                }
              });
            });
            callback(info);
          }
        }, function () {
          callback(info);
        });
      } else {
        callback(info);
      }
    }

  };

  OB.Model.DiscountProductQuery = OB.Data.ExtensibleModel.extend({});
  OB.Model.DiscountProductQuery.addProperties([{
    name: 'productId',
    column: 'm_product_id'
  }, {
    name: 'name',
    column: '_identifier'
  }, {
    name: 'standardPrice',
    column: 'standardPrice'
  }, {
    name: 'irType',
    column: 'em_custdis_ir_type'
  }, {
    name: 'custdisAmount',
    column: 'em_custdis_amount'
  }, {
    name: 'isGift',
    column: 'em_custdis_is_gift'
  }, {
    name: 'custdisPercentage',
    column: 'em_custdis_percentage'
  }, {
    name: 'custdisDiscountamount',
    column: 'em_custdis_discountamount'
  }, {
    name: 'custdisGiftQty',
    column: 'em_custdis_giftqty'
  }, {
    name: 'custdisPrimaryNewPrice',
    column: 'em_custdis_primary_newprice'
  }, {
    name: 'custdisSecondaryNewPrice',
    column: 'em_custdis_secondary_newprice'
  }]);

}());

OB.UI.DiscountList.extend({
    renderLine: 'OB.UI.DiscountList.OptionsNew'
});

enyo.kind({
    kind: 'enyo.Option',
    name: 'OB.UI.DiscountList.OptionsNew',
    initComponents: function() {
        if (this.model.get('discountType') === '61D65EECCF97487A82D169BB7E96832C' || this.model.get('discountType') === '1424B47A341E4D38B0F61CCF26450AF3'
            || this.model.get('discountType') === 'B277B63BDA6E4B67B3D8F491FC3C4E3B' || this.model.get('discountType') === '399EF97C508B40E787A444CE3855A7E4'
            || this.model.get('discountType') === '25ECBE384EAB4C1EB69948FF99FC8456') {
            this.hide();
        } else {
            var rule = OB.Model.Discounts.discountRules[this.model.get('discountType')],
                propertyToShow = '';
            if (rule.getAmountProperty && rule.getAmountProperty instanceof Function) {
                propertyToShow = OB.Model.Discounts.discountRules[this.model.get('discountType')].getAmountProperty();
            }
            this.setValue(this.model.get('id'));
            this.originalText = this.model.get('_identifier');
            // TODO: this shouldn't be hardcoded but defined in each promotion
            if (!OB.Model.Discounts.discountRules[this.model.get('discountType')].isFixed) {
                //variable
                this.requiresQty = true;
                if (!OB.Model.Discounts.discountRules[this.model.get('discountType')].isAmount) {
                    //variable porcentaje
                    this.units = '%';
                    if (!_.isUndefined(this.model.get(propertyToShow)) && !_.isNull(this.model.get(propertyToShow))) {
                        this.amt = this.model.get(propertyToShow);
                    }
                } else {
                    //variable qty
                    this.units = OB.MobileApp.model.get('terminal').currency$_identifier;
                    if (this.model.get(propertyToShow)) {
                        this.amt = this.model.get(propertyToShow);
                    }
                }
            } else {
                //fixed
                this.requiresQty = false;
                if (!OB.Model.Discounts.discountRules[this.model.get('discountType')].isAmount) {
                    //fixed percentage
                    this.units = '%';
                    if (!_.isUndefined(this.model.get(propertyToShow)) && !_.isNull(this.model.get(propertyToShow))) {
                        this.amt = this.model.get(propertyToShow);
                    }
                } else {
                    //fixed amount
                    this.units = OB.MobileApp.model.get('terminal').currency$_identifier;
                    if (!_.isUndefined(this.model.get(propertyToShow)) && !_.isNull(this.model.get(propertyToShow))) {
                        this.amt = this.model.get(propertyToShow);
                    }
                }
            }
            if (this.amt) {
                this.setContent(this.originalText + ' - ' + this.amt + ' ' + this.units);
            } else {
                this.setContent(this.originalText);
            }
        }
    }
});
/**
 * 
 */

//extending applyChange function of OB.UI.EditOrderLine of modalreturnline.js file
OB.UI.EditOrderLine.extend ({
  applyChange: function (inSender, inEvent) {
    var me = this;
    var model = inEvent.model;
    var index = inEvent.lines.indexOf(this.newAttribute);
    var line, promotionsfactor;
    if (index !== -1) {
      if (this.$.checkboxButtonReturn.checked) {
        var initialQty = inEvent.lines[index].quantity;
        var qty = this.$.quantity.getValue();
        var orderList = OB.MobileApp.model.orderList;
        enyo.forEach(orderList.models, function (order) {
          enyo.forEach(order.get('lines').models, function (l) {
            if (l.get('originalOrderLineId')) {
              if (l.get('product').id === me.newAttribute.id && l.get('originalOrderLineId') === me.newAttribute.lineId) {
                qty = qty - l.get('qty');
              }
            }
          });
        });
        if (qty > inEvent.lines[index].remainingQuantity) {
          OB.UTIL.showWarning(OB.I18N.getLabel('OBRETUR_ExceedsQuantity') + ' ' + me.newAttribute.name);
          inEvent.lines[index].exceedsQuantity = true;
        }
        inEvent.lines[index].remainingQuantity = inEvent.lines[index].remainingQuantity - this.$.quantity.getValue();
        inEvent.lines[index].selectedQuantity = this.$.quantity.getValue();
        // update promotions amount to the quantity returned
        /*enyo.forEach(this.newAttribute.promotions, function (p) {
          if (!OB.UTIL.isNullOrUndefined(p)) {
            p.amt = OB.DEC.mul(p.amt, (me.$.quantity.getValue() / initialQty));
           	p.actualAmt = OB.DEC.mul(p.actualAmt, (me.$.quantity.getValue() / initialQty));
            p.displayedTotalAmount = OB.DEC.mul(p.displayedTotalAmount, (me.$.quantity.getValue() / initialQty));
          }
        });*/
      } else {
        inEvent.lines.splice(index, 1);
      }
    }
  }
});